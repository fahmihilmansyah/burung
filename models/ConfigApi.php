<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "config_api".
 *
 * @property int $id
 * @property string|null $name_api
 * @property string|null $url_host
 * @property string|null $token_key
 * @property string|null $secret_key
 * @property string|null $kode_api
 */
class ConfigApi extends \yii\db\ActiveRecord
{
    /**
     * change connection
     */
    public static function getDb() {
        return Yii::$app->get('db_masjed'); 
    }
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'config_api';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name_api', 'url_host', 'token_key', 'secret_key', 'kode_api'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name_api' => 'Name Api',
            'url_host' => 'Url Host',
            'token_key' => 'Token Key',
            'secret_key' => 'Secret Key',
            'kode_api' => 'Kode Api',
        ];
    }
}
