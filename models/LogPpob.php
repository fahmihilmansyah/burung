<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "log_ppob".
 *
 * @property string $id
 * @property string|null $req_json
 * @property string|null $res_json
 * @property string|null $created_ad
 * @property string|null $no_transaksi
 * @property string|null $type
 */
class LogPpob extends \yii\db\ActiveRecord
{
    /**
     * change connection
     */
    public static function getDb() {
        return Yii::$app->get('db_masjed'); 
    }
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'log_ppob';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['req_json', 'res_json'], 'string'],
            [['created_ad'], 'safe'],
            [['id'], 'string', 'max' => 60],
            [['no_transaksi', 'type'], 'string', 'max' => 255],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'req_json' => 'Req Json',
            'res_json' => 'Res Json',
            'created_ad' => 'Created Ad',
            'no_transaksi' => 'No Transaksi',
            'type' => 'Type',
        ];
    }
}
