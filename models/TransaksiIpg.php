<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "transaksi_ipg".
 *
 * @property string $id
 * @property string|null $trxid
 * @property string|null $status
 * @property string|null $created_ad
 * @property string|null $updated_ad
 * @property string|null $response_get
 * @property string|null $response_post
 * @property string|null $target_table
 * @property string|null $request_json
 * @property string|null $response_json
 * @property string|null $rescallback_json
 */
class TransaksiIpg extends \yii\db\ActiveRecord
{
    /**
     * change connection
     */
    public static function getDb() {
        return Yii::$app->get('db_masjed'); 
    }
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'transaksi_ipg';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['created_ad', 'updated_ad'], 'safe'],
            [['response_get', 'response_post', 'request_json', 'response_json', 'rescallback_json'], 'string'],
            [['id'], 'string', 'max' => 60],
            [['trxid', 'status', 'target_table'], 'string', 'max' => 255],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'trxid' => 'Trxid',
            'status' => 'Status',
            'created_ad' => 'Created Ad',
            'updated_ad' => 'Updated Ad',
            'response_get' => 'Response Get',
            'response_post' => 'Response Post',
            'target_table' => 'Target Table',
            'request_json' => 'Request Json',
            'response_json' => 'Response Json',
            'rescallback_json' => 'Rescallback Json',
        ];
    }
}
