<div style="box-sizing: border-box; display:block; position:relative; margin:0 auto; background:#fff; max-width: 480px; width:480px; padding:20px; top:50px;">
    <h5 style="margin:0px; padding:0px; font-weight:bold; font-size:20px; text-align:center">Assalamu'alaikum Wrahmatullahi Wabarakatuh,</h5>
    <p style="font-size:13px; text-align:center;">Terima kasih, Fahmi Hilmansyah. </p>
    <div style="background:#ccc; overflow:hidden; padding: 5px; border-radius:5px; background: #ddd">
        <p style="font-size:13px; text-align:center;">Kode OTP kamu.</p>

        <div style="text-align:center">
        <?php 
            $otp_codes = isset($otp_code) ? str_split($otp_code) : str_split("123456") ;

            foreach ($otp_codes as $key => $oc) {
        ?>
            <div style="display:inline-block; background:#999; border-radius:5px; margin:1px; width:50px">
                <span style="display:block; box-sizing:border-box; position:relative; font-size:25px; text-align:center; padding:5px; color:#fff; font-weight:bold;">
                    <?= $oc; ?>
                </span>
            </div>
        <?php } ?>
        </div>
    </div>
    <p style="font-size:13px; text-align:center;">
        Aktifkan akunmu dengan memasukan kode tersebut pada halaman aktifasi.
    </p>
</div>