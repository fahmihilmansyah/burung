<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class BoAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'admintydashboard/libraries/bower_components/bootstrap/css/bootstrap.min.css',
        'admintydashboard/libraries/assets/css/jquery.mCustomScrollbar.css',
        'admintydashboard/libraries/bower_components/sweetalert/css/sweetalert.css',
        'admintydashboard/libraries/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css',
        'admintydashboard/libraries/assets/pages/data-table/css/buttons.dataTables.min.css',
        'admintydashboard/libraries/bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css',
        'js/dtablefixedcolumn/css/fixedColumns.bootstrap4.min.css',
        'admintydashboard/libraries/bower_components/datedropper/css/datedropper.min.css',
        'admintydashboard/libraries/bower_components/jquery-minicolors/css/jquery.minicolors.css',
        'admintydashboard/libraries/bower_components/pnotify/css/pnotify.css',
        'admintydashboard/libraries/bower_components/pnotify/css/pnotify.brighttheme.css',
        'admintydashboard/libraries/bower_components/pnotify/css/pnotify.buttons.css',
        'admintydashboard/libraries/bower_components/pnotify/css/pnotify.history.css',
        'admintydashboard/libraries/bower_components/pnotify/css/pnotify.mobile.css',
        'admintydashboard/libraries/assets/icon/icofont/css/icofont.css',
        'admintydashboard/libraries/assets/icon/feather/css/feather.css',
        'admintydashboard/libraries/bower_components/select2/css/select2.min.css',
        'admintydashboard/libraries/bower_components/bootstrap-multiselect/css/bootstrap-multiselect.css',
        'admintydashboard/libraries/bower_components/multiselect/css/multi-select.css',
        'admintydashboard/libraries/assets/css/style.css',
        'css/site.css',
        'css/bo.css',   
    ];
    public $js = [
        'admintydashboard/libraries/bower_components/jquery/js/jquery.min.js',
        'admintydashboard/libraries/bower_components/jquery-ui/js/jquery-ui.min.js',
        'admintydashboard/libraries/bower_components/popper.js/js/popper.min.js',
        'admintydashboard/libraries/bower_components/bootstrap/js/bootstrap.min.js',
        'admintydashboard/libraries/bower_components/jquery-slimscroll/js/jquery.slimscroll.js',
        'admintydashboard/libraries/bower_components/modernizr/js/modernizr.js',
        'admintydashboard/libraries/bower_components/sweetalert/js/sweetalert.min.js',
        'admintydashboard/libraries/assets/js/jquery.mCustomScrollbar.concat.min.js',
        'admintydashboard/libraries/assets/js/pcoded.min.js',
        'admintydashboard/libraries/assets/js/vartical-layout.min.js',
        'admintydashboard/libraries/bower_components/datatables.net/js/jquery.dataTables.min.js',
        'admintydashboard/libraries/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js',
        'admintydashboard/libraries/bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js',
        'admintydashboard/libraries/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js',
        'admintydashboard/libraries/bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js',
        'js/dtablefixedcolumn/js/dataTables.fixedColumns.min.js',
        'js/dtablefixedcolumn/js/fixedColumns.bootstrap4.min.js',
        'admintydashboard/libraries/bower_components/pnotify/js/pnotify.js',
        'admintydashboard/libraries/bower_components/pnotify/js/pnotify.desktop.js',
        'admintydashboard/libraries/bower_components/pnotify/js/pnotify.buttons.js',
        'admintydashboard/libraries/bower_components/pnotify/js/pnotify.confirm.js',
        'admintydashboard/libraries/bower_components/pnotify/js/pnotify.callbacks.js',
        'admintydashboard/libraries/bower_components/pnotify/js/pnotify.animate.js',
        'admintydashboard/libraries/bower_components/pnotify/js/pnotify.history.js',
        'admintydashboard/libraries/bower_components/pnotify/js/pnotify.mobile.js',
        'admintydashboard/libraries/bower_components/pnotify/js/pnotify.nonblock.js',
        'admintydashboard/libraries/bower_components/select2/js/select2.full.min.js',
        'admintydashboard/libraries/bower_components/bootstrap-multiselect/js/bootstrap-multiselect.js',
        'admintydashboard/libraries/bower_components/multiselect/js/jquery.multi-select.js',
        'admintydashboard/libraries/bower_components/datedropper/js/datedropper.min.js',
        'admintydashboard/libraries/assets/js/script.min.js',
        'js/jqvalidate/jquery.validate.min.js',
        'js/jqvalidate/additional-methods.min.js',
        'js/global.js',
        'js/jos.js'
    ];
    public $jsOptions = ['position' => \yii\web\View::POS_HEAD];
    public $depends = [];
}
