<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/app.css',
        'js/plugins/snackbar/snackbar.min.css',
        'js/plugins/owl/assets/owl.carousel.min.css',
        'js/plugins/owl/assets/owl.theme.default.min.css',
        'js/plugins/jqui/jquery-ui.min.css',
    ];
    public $js = [
        'js/plugins/jqvalidate/jquery.validate.min.js',
        'js/plugins/jqvalidate/additional-methods.min.js',
        'js/plugins/snackbar/snackbar.min.js',
        'js/plugins/owl/owl.carousel.min.js',
        'js/plugins/jqui/jquery-ui.min.js',
        'js/global.js',
        'js/accordion.js',
        'js/basicsubmit.js',
        'js/event.js',
        'js/penilaian.js',
        'js/addjenis.js',
        'js/addjuri.js',
        'js/register.js',
        'js/login.js',
        'js/otpinput.js',
        'js/juri.js',
        'js/hari.js',
        'js/assignspesific.js',    
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
