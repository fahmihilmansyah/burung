-- MySQL dump 10.13  Distrib 8.0.23, for Linux (x86_64)
--
-- Host: localhost    Database: gacorapp
-- ------------------------------------------------------
-- Server version	8.0.23-0ubuntu0.20.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `type_penilaian`
--

DROP TABLE IF EXISTS `type_penilaian`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `type_penilaian` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nama_type` varchar(255) DEFAULT NULL,
  `nilai` decimal(25,2) DEFAULT NULL,
  `color` varchar(255) DEFAULT NULL,
  `kat` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `type_penilaian`
--

LOCK TABLES `type_penilaian` WRITE;
/*!40000 ALTER TABLE `type_penilaian` DISABLE KEYS */;
INSERT INTO `type_penilaian` VALUES (1,'Merah',100.00,'red','LB'),(2,'Kuning',15.00,'yellow','LB'),(3,'Biru',40.00,'blue','LB'),(4,'Bunyi',37.00,NULL,'OT'),(5,'Bunyi Stabil',37.50,NULL,'OT'),(6,'Bendera Favorit',0.00,NULL,'OT');
/*!40000 ALTER TABLE `type_penilaian` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event`
--

DROP TABLE IF EXISTS `event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `event` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nama_event` varchar(255) DEFAULT NULL,
  `tgl_event` varchar(255) DEFAULT NULL,
  `nominal` decimal(25,2) DEFAULT NULL,
  `waktu` varchar(255) DEFAULT NULL,
  `created_ad` datetime DEFAULT NULL,
  `updated_ad` datetime DEFAULT NULL,
  `event_group_id` int DEFAULT NULL,
  `jenis_burung` text,
  `type_gantungan_id` int DEFAULT NULL,
  `is_khusus` int DEFAULT '0',
  `user_id` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event`
--

LOCK TABLES `event` WRITE;
/*!40000 ALTER TABLE `event` DISABLE KEYS */;
INSERT INTO `event` VALUES (20,'Tes Event 5 Tidak Khusus','2021-04-20 00:00:00',433330.00,'14 s/d selsai','2021-04-15 23:23:57','2021-05-03 07:48:24',3,'4',2,0,1),(21,'Tes Event 6','2021-04-13 00:00:00',100000.00,'14 s/d selsai','2021-04-16 02:41:46','2021-04-16 02:41:54',3,'4',2,1,1),(22,'Event Baru','2021-04-05 00:00:00',300000.00,'14 s/d selsai','2021-04-20 11:48:31',NULL,3,'4',2,1,1),(23,'Tes Event 5','2021-05-25 00:00:00',400000.00,'14 s/d selsai','2021-05-03 00:13:50',NULL,3,'3',2,1,1);
/*!40000 ALTER TABLE `event` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `type_gantungan`
--

DROP TABLE IF EXISTS `type_gantungan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `type_gantungan` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `jumlah_row` int DEFAULT NULL,
  `jumlah_col` int DEFAULT NULL,
  `type_gnt` varchar(100) DEFAULT NULL,
  `created_ad` datetime DEFAULT NULL,
  `updated_ad` datetime DEFAULT NULL,
  `user_id` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `type_gantungan`
--

LOCK TABLES `type_gantungan` WRITE;
/*!40000 ALTER TABLE `type_gantungan` DISABLE KEYS */;
INSERT INTO `type_gantungan` VALUES (2,'Gantungan 10x11 DESC SLINE',10,11,'DESCSLINE','2021-04-11 08:55:56','2021-04-11 09:05:05',NULL);
/*!40000 ALTER TABLE `type_gantungan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menus`
--

DROP TABLE IF EXISTS `menus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `menus` (
  `id` int NOT NULL AUTO_INCREMENT,
  `sort_number` int DEFAULT '0',
  `name` varchar(50) DEFAULT NULL,
  `path` varchar(200) DEFAULT NULL,
  `icon` varchar(50) DEFAULT NULL,
  `parent` varchar(60) DEFAULT '*',
  `is_active` int DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `is_deleted` int DEFAULT '0',
  `last_access` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menus`
--

LOCK TABLES `menus` WRITE;
/*!40000 ALTER TABLE `menus` DISABLE KEYS */;
INSERT INTO `menus` VALUES (1,1,'Dashboard','#','icon-bar-chart','0',1,'2021-05-02 09:58:59','2021-05-02 09:58:59',0,NULL),(2,2,'Setting Application','#','icon-settings','0',1,'2021-05-02 09:58:59','2021-05-02 09:58:59',0,NULL),(3,3,'Manajemen Menus','settings/menu','','2',1,'2021-05-02 09:58:59','2021-05-02 09:58:59',0,NULL),(4,4,'Manajemen Roles','settings/role','','2',1,'2021-05-02 09:58:59','2021-05-02 09:58:59',0,NULL),(5,5,'Manajemen Users','settings/user','','2',1,'2021-05-02 09:58:59','2021-05-02 09:58:59',0,NULL);
/*!40000 ALTER TABLE `menus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migration`
--

DROP TABLE IF EXISTS `migration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migration`
--

LOCK TABLES `migration` WRITE;
/*!40000 ALTER TABLE `migration` DISABLE KEYS */;
INSERT INTO `migration` VALUES ('m000000_000000_base',1619920841),('m210413_033942_create_users_table',1619924266),('m210413_034145_create_roles_table',1619924266),('m210413_035134_create_roles_access_table',1619924266),('m210413_035503_create_menus_table',1619924266),('m210415_050359_create_icons_table',1619924266),('m210423_054837_create_logs_table',1619924266);
/*!40000 ALTER TABLE `migration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event_group`
--

DROP TABLE IF EXISTS `event_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `event_group` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `desc` text NOT NULL,
  `created_ad` datetime DEFAULT NULL,
  `updated_ad` datetime DEFAULT NULL,
  `user_id` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event_group`
--

LOCK TABLES `event_group` WRITE;
/*!40000 ALTER TABLE `event_group` DISABLE KEYS */;
INSERT INTO `event_group` VALUES (3,'Tes Aja','Tes','2021-04-15 09:58:23',NULL,1);
/*!40000 ALTER TABLE `event_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admins`
--

DROP TABLE IF EXISTS `admins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admins` (
  `id` varchar(60) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `password` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `is_deleted` int DEFAULT '0',
  `last_signin_at` datetime DEFAULT NULL,
  `last_signout_at` datetime DEFAULT NULL,
  `last_access` varchar(60) DEFAULT NULL,
  `role_id` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admins`
--

LOCK TABLES `admins` WRITE;
/*!40000 ALTER TABLE `admins` DISABLE KEYS */;
INSERT INTO `admins` VALUES ('75177627-0980-4bf4-b7d6-a9c2b69a73e0','Admin Gacor','admin@gacorapp.id','admin@gacorapp.id','$2y$13$yL1RRyklIl3e9V/9a1ZDrueu9tN7dA4accLIk2kIMVutFDCJ2i/Sa','2021-05-02 09:59:00','2021-05-02 09:59:00',0,NULL,NULL,NULL,'5dc266a2-09d8-402e-9fcf-9b64a558f092');
/*!40000 ALTER TABLE `admins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `peserta`
--

DROP TABLE IF EXISTS `peserta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `peserta` (
  `id` varchar(60) NOT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `telp` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `created_ad` datetime DEFAULT NULL,
  `updated_ad` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `peserta`
--

LOCK TABLES `peserta` WRITE;
/*!40000 ALTER TABLE `peserta` DISABLE KEYS */;
/*!40000 ALTER TABLE `peserta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles_access`
--

DROP TABLE IF EXISTS `roles_access`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `roles_access` (
  `id` varchar(60) NOT NULL,
  `menu_id` int DEFAULT NULL,
  `role_id` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles_access`
--

LOCK TABLES `roles_access` WRITE;
/*!40000 ALTER TABLE `roles_access` DISABLE KEYS */;
INSERT INTO `roles_access` VALUES ('090ca888-fdcc-4d95-bcd7-b69a0446db25',3,'5dc266a2-09d8-402e-9fcf-9b64a558f092'),('2e3f2bc3-b9dc-48bf-9bef-aa78d2bd8780',5,'5dc266a2-09d8-402e-9fcf-9b64a558f092'),('3d05035f-8bd7-4583-8fe3-111a995e9c44',4,'5dc266a2-09d8-402e-9fcf-9b64a558f092'),('839f0689-cd9d-4302-ae43-453c3f12663a',2,'5dc266a2-09d8-402e-9fcf-9b64a558f092'),('c0077885-12ee-4c72-b88f-41b49d29e6c6',1,'5dc266a2-09d8-402e-9fcf-9b64a558f092');
/*!40000 ALTER TABLE `roles_access` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `icons`
--

DROP TABLE IF EXISTS `icons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `icons` (
  `id` int NOT NULL AUTO_INCREMENT,
  `icon` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=243 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `icons`
--

LOCK TABLES `icons` WRITE;
/*!40000 ALTER TABLE `icons` DISABLE KEYS */;
INSERT INTO `icons` VALUES (1,'icon-alert-octagon'),(2,'icon-alert-circle'),(3,'icon-activity'),(4,'icon-alert-triangle'),(5,'icon-align-center'),(6,'icon-airplay'),(7,'icon-align-justify'),(8,'icon-align-left'),(9,'icon-align-right'),(10,'icon-arrow-down-left'),(11,'icon-arrow-down-right'),(12,'icon-anchor'),(13,'icon-aperture'),(14,'icon-arrow-left'),(15,'icon-arrow-right'),(16,'icon-arrow-down'),(17,'icon-arrow-up-left'),(18,'icon-arrow-up-right'),(19,'icon-arrow-up'),(20,'icon-award'),(21,'icon-bar-chart'),(22,'icon-at-sign'),(23,'icon-bar-chart-2'),(24,'icon-battery-charging'),(25,'icon-bell-off'),(26,'icon-battery'),(27,'icon-bluetooth'),(28,'icon-bell'),(29,'icon-book'),(30,'icon-briefcase'),(31,'icon-camera-off'),(32,'icon-calendar'),(33,'icon-bookmark'),(34,'icon-box'),(35,'icon-camera'),(36,'icon-check-circle'),(37,'icon-check'),(38,'icon-check-square'),(39,'icon-cast'),(40,'icon-chevron-down'),(41,'icon-chevron-left'),(42,'icon-chevron-right'),(43,'icon-chevron-up'),(44,'icon-chevrons-down'),(45,'icon-chevrons-right'),(46,'icon-chevrons-up'),(47,'icon-chevrons-left'),(48,'icon-circle'),(49,'icon-clipboard'),(50,'icon-chrome'),(51,'icon-clock'),(52,'icon-cloud-lightning'),(53,'icon-cloud-drizzle'),(54,'icon-cloud-rain'),(55,'icon-cloud-off'),(56,'icon-codepen'),(57,'icon-cloud-snow'),(58,'icon-compass'),(59,'icon-copy'),(60,'icon-corner-down-right'),(61,'icon-corner-down-left'),(62,'icon-corner-left-down'),(63,'icon-corner-left-up'),(64,'icon-corner-up-left'),(65,'icon-corner-up-right'),(66,'icon-corner-right-down'),(67,'icon-corner-right-up'),(68,'icon-cpu'),(69,'icon-credit-card'),(70,'icon-crosshair'),(71,'icon-disc'),(72,'icon-delete'),(73,'icon-download-cloud'),(74,'icon-download'),(75,'icon-droplet'),(76,'icon-edit-2'),(77,'icon-edit'),(78,'icon-edit-1'),(79,'icon-external-link'),(80,'icon-eye'),(81,'icon-feather'),(82,'icon-facebook'),(83,'icon-file-minus'),(84,'icon-eye-off'),(85,'icon-fast-forward'),(86,'icon-file-text'),(87,'icon-film'),(88,'icon-file'),(89,'icon-file-plus'),(90,'icon-folder'),(91,'icon-filter'),(92,'icon-flag'),(93,'icon-globe'),(94,'icon-grid'),(95,'icon-heart'),(96,'icon-home'),(97,'icon-github'),(98,'icon-image'),(99,'icon-inbox'),(100,'icon-layers'),(101,'icon-info'),(102,'icon-instagram'),(103,'icon-layout'),(104,'icon-link-2'),(105,'icon-life-buoy'),(106,'icon-link'),(107,'icon-log-in'),(108,'icon-list'),(109,'icon-lock'),(110,'icon-log-out'),(111,'icon-loader'),(112,'icon-mail'),(113,'icon-maximize-2'),(114,'icon-map'),(115,'icon-map-pin'),(116,'icon-menu'),(117,'icon-message-circle'),(118,'icon-message-square'),(119,'icon-minimize-2'),(120,'icon-mic-off'),(121,'icon-minus-circle'),(122,'icon-mic'),(123,'icon-minus-square'),(124,'icon-minus'),(125,'icon-moon'),(126,'icon-monitor'),(127,'icon-more-vertical'),(128,'icon-more-horizontal'),(129,'icon-move'),(130,'icon-music'),(131,'icon-navigation-2'),(132,'icon-navigation'),(133,'icon-octagon'),(134,'icon-package'),(135,'icon-pause-circle'),(136,'icon-pause'),(137,'icon-percent'),(138,'icon-phone-call'),(139,'icon-phone-forwarded'),(140,'icon-phone-missed'),(141,'icon-phone-off'),(142,'icon-phone-incoming'),(143,'icon-phone'),(144,'icon-phone-outgoing'),(145,'icon-pie-chart'),(146,'icon-play-circle'),(147,'icon-play'),(148,'icon-plus-square'),(149,'icon-plus-circle'),(150,'icon-plus'),(151,'icon-pocket'),(152,'icon-printer'),(153,'icon-power'),(154,'icon-radio'),(155,'icon-repeat'),(156,'icon-refresh-ccw'),(157,'icon-rewind'),(158,'icon-rotate-ccw'),(159,'icon-refresh-cw'),(160,'icon-rotate-cw'),(161,'icon-save'),(162,'icon-search'),(163,'icon-server'),(164,'icon-scissors'),(165,'icon-share-2'),(166,'icon-share'),(167,'icon-shield'),(168,'icon-settings'),(169,'icon-skip-back'),(170,'icon-shuffle'),(171,'icon-sidebar'),(172,'icon-skip-forward'),(173,'icon-slack'),(174,'icon-slash'),(175,'icon-smartphone'),(176,'icon-square'),(177,'icon-speaker'),(178,'icon-star'),(179,'icon-stop-circle'),(180,'icon-sun'),(181,'icon-sunrise'),(182,'icon-tablet'),(183,'icon-tag'),(184,'icon-sunset'),(185,'icon-target'),(186,'icon-thermometer'),(187,'icon-thumbs-up'),(188,'icon-thumbs-down'),(189,'icon-toggle-left'),(190,'icon-toggle-right'),(191,'icon-trash-2'),(192,'icon-trash'),(193,'icon-trending-up'),(194,'icon-trending-down'),(195,'icon-triangle'),(196,'icon-type'),(197,'icon-twitter'),(198,'icon-upload'),(199,'icon-umbrella'),(200,'icon-upload-cloud'),(201,'icon-unlock'),(202,'icon-user-check'),(203,'icon-user-minus'),(204,'icon-user-plus'),(205,'icon-user-x'),(206,'icon-user'),(207,'icon-users'),(208,'icon-video-off'),(209,'icon-video'),(210,'icon-voicemail'),(211,'icon-volume-x'),(212,'icon-volume-2'),(213,'icon-volume-1'),(214,'icon-volume'),(215,'icon-watch'),(216,'icon-wifi'),(217,'icon-x-square'),(218,'icon-wind'),(219,'icon-x'),(220,'icon-x-circle'),(221,'icon-zap'),(222,'icon-zoom-in'),(223,'icon-zoom-out'),(224,'icon-command'),(225,'icon-cloud'),(226,'icon-hash'),(227,'icon-headphones'),(228,'icon-underline'),(229,'icon-italic'),(230,'icon-bold'),(231,'icon-crop'),(232,'icon-help-circle'),(233,'icon-paperclip'),(234,'icon-shopping-cart'),(235,'icon-tv'),(236,'icon-wifi-off'),(237,'icon-minimize'),(238,'icon-maximize'),(239,'icon-gitlab'),(240,'icon-sliders'),(241,'icon-star-on'),(242,'icon-heart-on');
/*!40000 ALTER TABLE `icons` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `peserta_booking`
--

DROP TABLE IF EXISTS `peserta_booking`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `peserta_booking` (
  `id` varchar(60) NOT NULL,
  `peserta_id` varchar(60) DEFAULT NULL,
  `gantunga_id` varchar(60) DEFAULT NULL,
  `kode_booking` varchar(255) DEFAULT NULL,
  `created_ad` datetime DEFAULT NULL,
  `updated_ad` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `peserta_booking`
--

LOCK TABLES `peserta_booking` WRITE;
/*!40000 ALTER TABLE `peserta_booking` DISABLE KEYS */;
/*!40000 ALTER TABLE `peserta_booking` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event_group_jenis_burung`
--

DROP TABLE IF EXISTS `event_group_jenis_burung`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `event_group_jenis_burung` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `jenis_burung_id` int DEFAULT NULL,
  `event_group_id` int DEFAULT NULL,
  `created_ad` datetime DEFAULT NULL,
  `updated_ad` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event_group_jenis_burung`
--

LOCK TABLES `event_group_jenis_burung` WRITE;
/*!40000 ALTER TABLE `event_group_jenis_burung` DISABLE KEYS */;
INSERT INTO `event_group_jenis_burung` VALUES (13,5,3,'2021-05-02 16:14:55',NULL),(14,4,3,'2021-05-02 16:14:55',NULL),(15,3,3,'2021-05-02 16:14:55',NULL);
/*!40000 ALTER TABLE `event_group_jenis_burung` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `juri_assignment`
--

DROP TABLE IF EXISTS `juri_assignment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `juri_assignment` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `juri_id` int DEFAULT NULL,
  `gnt_id` varchar(60) DEFAULT NULL,
  `event_id` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16860253 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `juri_assignment`
--

LOCK TABLES `juri_assignment` WRITE;
/*!40000 ALTER TABLE `juri_assignment` DISABLE KEYS */;
INSERT INTO `juri_assignment` VALUES (16860132,5,'03eddba5-5a7f-444d-b0d0-653fca3fe432',20),(16860133,5,'06cb238e-3a62-4e9d-aa8b-6acfce7cf0e9',20),(16860134,5,'15494465-4b4e-428f-bf5e-38a19cbf71b1',20),(16860135,5,'16f12246-2912-4f44-9a70-a99cefda80d8',20),(16860136,5,'176dedbf-8ee9-41a5-878d-143c2bd74e95',20),(16860137,5,'1c9ca94f-99b6-4a18-a7db-fb892d63c08b',20),(16860138,5,'1e5de1f4-d90d-4d68-8690-53199d7f09eb',20),(16860139,5,'206cfadc-1255-49e3-a587-22dd761240dc',20),(16860140,5,'2297f60b-fa07-4082-ac88-0bf97a92799d',20),(16860141,5,'253d2096-1e12-4842-9ecb-ec1a94e7880d',20),(16860142,5,'26291c84-abc6-4ed6-832f-8e1978049d34',20),(16860143,5,'2a3e5ef3-feb6-45ff-ac4d-55ff0508efb2',20),(16860144,5,'2b7e080d-0120-470f-aaf8-ddb17e093a5e',20),(16860145,5,'2b9544cd-b9f4-46e2-b9db-1b887a8fe4c8',20),(16860146,5,'30344a03-aadb-46d5-904f-318e40123d60',20),(16860147,5,'30374e2d-4a7e-427e-83c0-708ac1a79cbe',20),(16860148,5,'30fb85e3-ffbd-4e52-9cde-8e20656377c3',20),(16860149,5,'3638b9f1-5007-40cc-b13c-6c8ef8992270',20),(16860150,5,'364b8573-fe58-41b6-bf6a-85d052f058f6',20),(16860151,5,'37f2b0bd-2368-4bcc-be25-12ac454d9a9a',20),(16860152,5,'38560142-6382-4c80-8358-ef6908eddb66',20),(16860153,5,'392c8d3e-82cd-4b72-a8ac-af929f1f8006',20),(16860154,5,'3a661727-67bd-4081-a628-1f51db09ed53',20),(16860155,5,'3b394a44-1dbb-4b9e-ac97-c07e706a5b19',20),(16860156,5,'3de73a04-3d0b-4be9-a736-31ca712a8954',20),(16860157,5,'42a46cd1-a1c3-4c60-a84a-950af93d71b3',20),(16860158,5,'46221809-13c6-4c2b-9ccd-6a2fcef64150',20),(16860159,5,'47067c4d-3c86-435b-aa99-50b45dfcc9fb',20),(16860160,5,'4d189b0d-86af-4451-8e34-ac02c1ef59d2',20),(16860161,5,'4e26bf4f-f30a-4a42-884c-218dfce8cbcd',20),(16860162,5,'55b25d9e-82bb-4b32-94ae-d693774db816',20),(16860163,5,'595ad0e3-8c98-48c3-b772-70bf4d4ef2cc',20),(16860164,5,'5ae4bd53-a368-48cb-be5e-ddee00c0068e',20),(16860165,5,'5bc09195-6bf2-48bc-8e57-158f7d129fc3',20),(16860166,5,'5e58547f-1a83-4bf8-8627-55a2cc80c21b',20),(16860167,5,'61f977ff-0683-4086-aa3f-a2e50717f01f',20),(16860168,5,'678b72bc-49e5-4eec-89f7-d7cacac6da37',20),(16860169,5,'76179b7d-da33-4aa1-b746-3b2c5c0beb7b',20),(16860170,5,'790b3951-dea6-43da-aa46-ddeeb9ff6318',20),(16860171,5,'7a7712da-b4f8-4b2a-a125-deb2575d5e6b',20),(16860172,5,'7dd9b13e-ad56-4b64-9414-d3d0bcd768ee',20),(16860173,5,'812eb18e-f17c-43a5-9e59-b8a74c8bbef8',20),(16860174,5,'8273329a-dea9-4681-8ad1-bf68f82feaaa',20),(16860175,5,'82dbca27-8509-4281-acca-5594e5acfde4',20),(16860176,5,'8968e395-5bce-4b40-a208-ecf8e852cd6b',20),(16860177,5,'8a49c236-9fec-4ae0-be34-0b0a6e7e2fcb',20),(16860178,5,'8d693673-586c-4c02-8acb-28dcd37e4ab8',20),(16860179,5,'8dfe4a40-dd51-48a6-ab66-20a32149bd1f',20),(16860180,5,'8ea72d76-68af-42b8-97bd-ab5386b7a743',20),(16860181,5,'8f21f25d-e98e-4d98-a420-e92ed3e8cd21',20),(16860182,5,'908fdcb7-9d5f-46f7-924b-b50ca338c010',20),(16860183,5,'9349af79-0b7d-4711-a58c-775755b6f92b',20),(16860184,5,'949b6b8d-2b63-4235-9fe4-8cff4babd126',20),(16860185,5,'96336024-d892-45c9-856d-44680bf078d3',20),(16860186,5,'979ffad8-e3d9-4331-b588-4d06e0b8f716',20),(16860187,5,'99828795-5876-42e4-a3cb-35bc1e7207f2',20),(16860188,5,'9c9cb06c-8e23-4280-a804-04ddab4ce70f',20),(16860189,5,'9f2c8547-8288-473e-a1fd-9748b6012d30',20),(16860190,5,'a2f0ddcd-b2fd-4dd8-b9a1-b95d20dafe92',20),(16860191,5,'a51f99c0-a37a-47d6-a2ab-1c05533ccf4f',20),(16860192,5,'a7831ca3-6ca5-44db-8b0f-5085c26fa1c9',20),(16860193,5,'a91d4240-182c-4d20-8c47-cdf1821dd880',20),(16860194,5,'a98048e5-7be4-4d90-ab45-60d180075fe7',20),(16860195,5,'ab17c77a-5340-4d9e-9025-c872c490905c',20),(16860196,5,'ac80b9e1-33db-4d6f-a2db-320aec7a4015',20),(16860197,5,'ad89c8e4-3f4a-4763-bd98-cf26bfd684fc',20),(16860198,5,'b15afef5-9486-409b-8fba-526a8d383293',20),(16860199,5,'b16d20b5-636d-4a38-8f70-d074146f9447',20),(16860200,5,'b6c22f95-1920-45a1-9d43-a8a7b7140ccb',20),(16860201,5,'b6f87c01-f202-44fe-8fa2-52e794978495',20),(16860202,5,'b96dd58c-a2e6-4c5c-84e2-a22b8a675713',20),(16860203,5,'bbfaab92-654f-42c4-b4e1-e00684473574',20),(16860204,5,'bd655c90-814d-4c75-8cc7-1bc1592d9005',20),(16860205,5,'be41d818-aeab-4f30-a73b-366f8cc996c1',20),(16860206,5,'bf5ae7e2-44bd-4fdf-9229-95fec6b6eabf',20),(16860207,5,'c0247674-cca4-4458-be8c-f06f884a53de',20),(16860208,5,'c42977a6-8ecc-4ae9-8192-6cadf5003b9d',20),(16860209,5,'c46f1170-08b7-4a88-8612-54db9bc2c550',20),(16860210,5,'c8b83c64-369f-404c-a9ba-cf8c97cb8ca3',20),(16860211,5,'ca08d3de-d888-4266-adfa-a76d53d2fea7',20),(16860212,5,'ca5dc2ee-c233-422b-9719-a943f559fd9d',20),(16860213,5,'cedf613c-94e0-4688-8d77-175716033f42',20),(16860214,5,'d2461ce8-578d-4d8c-b64c-b8c4f5689ba1',20),(16860215,5,'d263557f-f9e4-473d-b304-fa7251ff6124',20),(16860216,5,'d3c37b18-a2e0-416d-99f4-5d6d1a4b737c',20),(16860217,5,'d528ea44-e474-4ebb-9897-4cefe39cad2b',20),(16860218,5,'d5e0756b-f058-4f1f-9256-21288cc89afb',20),(16860219,5,'d9beb6ce-44a0-4654-b1d2-d260b02eb890',20),(16860220,5,'dcf830cb-395d-4529-a806-933836720494',20),(16860221,5,'de7e9140-7e1e-4994-b70c-dd3fbfce212a',20),(16860222,5,'df5c179d-e966-4698-afe4-2618f478deb8',20),(16860223,5,'dfc6ed17-8360-40eb-911a-0ab0e180b4a0',20),(16860224,5,'e0291373-dbc7-4540-b47b-335e68f60c3b',20),(16860225,5,'e0fe4a7e-d4e0-4d01-b384-36b79883092c',20),(16860226,5,'e2da7815-3228-4daf-a705-4abc79d34dbb',20),(16860227,5,'e3ae09a1-1ff9-4a33-a417-1c1714f89bd0',20),(16860228,5,'e3f7a7ca-6d2a-4fc1-be1f-c140bfe6babd',20),(16860229,5,'e443909d-2b52-4e53-a37d-12d780379a79',20),(16860230,5,'e749dd29-36db-476e-96e6-e07d736f98af',20),(16860231,5,'e77dad5b-81c3-4abb-86b0-a6d7ad44a79b',20),(16860232,5,'e83b3f8c-8545-4e99-b6c1-8bb6635827e2',20),(16860233,5,'ed8f049e-897d-4ba3-9fc3-e9cf75ecf0a8',20),(16860234,5,'edc9a73e-66fb-4679-a360-b944e0ab1bc7',20),(16860235,5,'f123d40b-2889-4f4b-b15b-7c30d9edd6d8',20),(16860236,5,'f15b7fd5-f1ca-4996-9191-dbc43ce6a109',20),(16860237,5,'f1b3dc3f-c2d9-4f50-92ef-8279ebcec7c4',20),(16860238,5,'f6775b3f-6cad-4468-8bc5-61d216857ae7',20),(16860239,5,'f7172975-acfd-48f4-ab6b-79a7b6869e01',20),(16860240,5,'f7355c22-0333-4a4d-b3d6-d3e0b71f219a',20),(16860241,5,'ffc8afbb-7837-4a24-aa57-4fdbeabfa1ca',20),(16860242,5,'827fbb79-5d16-4a69-950a-c8be87fbcb12',23),(16860243,5,'5ace2e28-8cb8-4154-a77c-586906f12f96',23),(16860244,5,'0fdf5184-e1d7-4752-aa0d-313c5edbabc9',23),(16860245,5,'f94775f2-a895-49f5-8016-88418d851dd7',23),(16860246,5,'1d067f6e-b0e5-4cac-8f43-8d3297374f24',23),(16860247,5,'41a0c98f-7a90-4d48-9139-f0ca87a07745',23),(16860248,5,'08ec8b5c-4d5d-45b8-94e0-3c27ebb5a8bf',23),(16860249,5,'2352f947-11f2-4e58-9bc5-21b86e3b2b49',23),(16860250,5,'c173ac7c-beaf-418c-b12d-c90ce477b3a0',23),(16860251,5,'18e7241c-8e69-4c14-96d7-2d6965d4ff07',23),(16860252,5,'e079cd80-b562-410e-8cde-ec58bd515eb4',23);
/*!40000 ALTER TABLE `juri_assignment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `roles` (
  `id` varchar(60) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `description` varchar(50) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `is_deleted` int DEFAULT '0',
  `last_access` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES ('5dc266a2-09d8-402e-9fcf-9b64a558f092','Super Administrator','Role untuk admin tertinggi','2021-05-02 09:58:59','2021-05-02 09:58:59',0,NULL);
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `settings` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nama_pemilik` varchar(255) DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `notlp` varchar(255) DEFAULT NULL,
  `created_ad` datetime DEFAULT NULL,
  `updated_ad` datetime DEFAULT NULL,
  `user_id` int DEFAULT NULL,
  `jadwal_pelaksanaan` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings`
--

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` VALUES (2,'Budi','N','123456','2021-04-11 14:33:52','2021-04-15 22:11:42',1,'1,2');
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_otp`
--

DROP TABLE IF EXISTS `users_otp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users_otp` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `otp_code` varchar(10) DEFAULT NULL,
  `otp_lifetime` datetime DEFAULT NULL,
  `user_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_otp`
--

LOCK TABLES `users_otp` WRITE;
/*!40000 ALTER TABLE `users_otp` DISABLE KEYS */;
INSERT INTO `users_otp` VALUES (6,'791737','2021-04-04 16:36:28',6);
/*!40000 ALTER TABLE `users_otp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gantungan`
--

DROP TABLE IF EXISTS `gantungan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `gantungan` (
  `id` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `no_gnt` varchar(255) DEFAULT NULL,
  `event_id` int DEFAULT NULL,
  `created_ad` datetime DEFAULT NULL,
  `updated_ad` datetime DEFAULT NULL,
  `no_urut` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gantungan`
--

LOCK TABLES `gantungan` WRITE;
/*!40000 ALTER TABLE `gantungan` DISABLE KEYS */;
INSERT INTO `gantungan` VALUES ('02069982-8be7-422a-adb1-a67400b0caf3','7',22,'2021-04-20 11:48:31','2021-04-20 11:48:31',5),('024da9af-6fa3-4826-a6b7-96c70109009c','64',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',64),('03eddba5-5a7f-444d-b0d0-653fca3fe432','74',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',70),('05b9badd-52e3-4de4-b7af-f3da84b0fd5a','105',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',105),('06327e64-d795-4294-a3aa-5da9f03584b0','59',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',59),('0649cbf2-c946-4e10-87f7-fd686989a3c8','84',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',84),('06cb238e-3a62-4e9d-aa8b-6acfce7cf0e9','25',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',31),('07f3adea-dc97-4368-8c7a-1bbb1732f6de','29',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',27),('08b8a791-e2e4-4723-a4da-9d42d9544f20','13',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',13),('08ec8b5c-4d5d-45b8-94e0-3c27ebb5a8bf','5',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',7),('09dd6155-996c-4569-bf03-2380884b9ad4','69',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',75),('0a311aee-562a-4380-b557-f40579ca154b','79',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',79),('0adf780d-475f-4917-b47a-d56ed7f0dcc5','20',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',20),('0e32fe93-8abc-4e55-805e-aa9a5b1d31e1','11',22,'2021-04-20 11:48:31','2021-04-20 11:48:31',1),('0e50dd49-be25-40c0-9f02-ff63ae6677c2','65',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',65),('0f4d4f39-4661-4b80-9041-55742af133d7','30',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',26),('0fdf5184-e1d7-4752-aa0d-313c5edbabc9','9',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',3),('11211185-2c48-4ad5-a2ea-9c6198c28bb4','32',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',24),('1260bee3-6c68-4992-846a-a0959dcf4fb0','96',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',92),('136b8cb9-a334-415d-bd65-3916b6d3ef89','53',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',47),('13e37063-fd1b-4380-bbd0-af168c8c7402','90',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',98),('141ab4c7-8a71-4851-863d-a75a7c2ee589','95',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',93),('15494465-4b4e-428f-bf5e-38a19cbf71b1','75',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',69),('156c52fb-0e92-4568-b3bb-314c7f222e61','34',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',34),('16c1ee56-c0da-4db2-b01b-cb94d2a0a402','102',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',102),('16f12246-2912-4f44-9a70-a99cefda80d8','60',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',60),('176dedbf-8ee9-41a5-878d-143c2bd74e95','66',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',66),('18e7241c-8e69-4c14-96d7-2d6965d4ff07','2',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',10),('1a25cf66-c385-49ba-a036-296e69a6f6ba','22',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',22),('1a324df1-aaec-4f1f-ae78-7146ce13bdf0','75',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',69),('1ab97d4d-1ceb-4e35-abdc-2d7babb84fa5','52',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',48),('1c9ca94f-99b6-4a18-a7db-fb892d63c08b','53',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',47),('1d067f6e-b0e5-4cac-8f43-8d3297374f24','7',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',5),('1e5de1f4-d90d-4d68-8690-53199d7f09eb','5',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',7),('206cfadc-1255-49e3-a587-22dd761240dc','49',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',51),('207a02b8-dc64-4c77-af50-9ea713ed1202','94',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',94),('21325da9-1066-481e-b869-75b77d9511ef','72',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',72),('219a7647-01ee-4286-81a2-2bf835d8474a','80',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',80),('2297f60b-fa07-4082-ac88-0bf97a92799d','48',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',52),('2352f947-11f2-4e58-9bc5-21b86e3b2b49','4',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',8),('238f6bbf-aa9f-481e-93e5-6e019af700cb','79',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',79),('2434b70c-a92c-4e10-aad4-0a66b45714f9','85',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',85),('24622bcb-34b6-46c5-920c-1fdf76d0835c','40',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',40),('247866ae-88ab-439e-8d87-e66e155edf9d','108',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',108),('253d2096-1e12-4842-9ecb-ec1a94e7880d','7',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',5),('25fe7605-c56b-45f6-baeb-5f2ac47f5761','36',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',36),('26291c84-abc6-4ed6-832f-8e1978049d34','29',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',27),('265e86bd-c0b9-4474-b8b2-8562c14381b0','62',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',62),('28cf0500-15b2-4334-9cf6-1d9259a96d78','105',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',105),('29543354-2078-402c-882a-428d0fe1c81e','109',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',109),('2a3e5ef3-feb6-45ff-ac4d-55ff0508efb2','6',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',6),('2aea54ff-0cd8-45ac-bc3a-2aead236eae2','56',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',56),('2b7e080d-0120-470f-aaf8-ddb17e093a5e','97',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',91),('2b9544cd-b9f4-46e2-b9db-1b887a8fe4c8','24',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',32),('2d98bff4-a0b6-4128-9bd7-fc0577129f40','98',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',90),('30344a03-aadb-46d5-904f-318e40123d60','36',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',36),('30374e2d-4a7e-427e-83c0-708ac1a79cbe','94',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',94),('3071be39-6e4c-4931-aa91-5ab7da491784','21',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',21),('30fb85e3-ffbd-4e52-9cde-8e20656377c3','55',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',45),('31d3aceb-bac6-4c0e-9aa3-dcb5ded69c24','8',22,'2021-04-20 11:48:31','2021-04-20 11:48:31',4),('32cdd29b-63c2-43da-91e7-3b84ebb0f44b','31',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',25),('33408453-5acd-4518-80c7-a73eaddc5d2a','27',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',29),('3391980a-c41f-4f59-be25-ab58ae0d20c2','10',22,'2021-04-20 11:48:31','2021-04-20 11:48:31',2),('344feb14-ce65-448f-87c6-2777820a0bc2','107',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',107),('3638b9f1-5007-40cc-b13c-6c8ef8992270','42',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',42),('364b8573-fe58-41b6-bf6a-85d052f058f6','52',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',48),('369e9876-be3e-40b4-bff6-818e73355352','16',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',16),('3733c22b-f3e3-4bb0-8db4-49923d03ccfb','83',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',83),('37f2b0bd-2368-4bcc-be25-12ac454d9a9a','40',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',40),('38560142-6382-4c80-8358-ef6908eddb66','3',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',9),('392c8d3e-82cd-4b72-a8ac-af929f1f8006','98',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',90),('3a2c13cf-5c3c-4d97-8081-23f36d3ffcf9','14',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',14),('3a661727-67bd-4081-a628-1f51db09ed53','46',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',54),('3b394a44-1dbb-4b9e-ac97-c07e706a5b19','17',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',17),('3c224954-7a5d-41bc-86da-cc841ccc6257','51',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',49),('3de73a04-3d0b-4be9-a736-31ca712a8954','103',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',103),('4126c4bf-6429-4338-9e14-b61cc24cbb1d','1',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',11),('41783788-14c2-4da3-b064-f21a1f3394e8','82',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',82),('41a0c98f-7a90-4d48-9139-f0ca87a07745','6',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',6),('423cb8f4-2360-4295-9398-79226b04a1cd','106',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',106),('42413ca0-5fad-44a3-9c3d-59bc5e92ff00','41',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',41),('42a46cd1-a1c3-4c60-a84a-950af93d71b3','83',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',83),('42aba59a-54f7-4e63-bade-47830e573df1','13',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',13),('42bfac38-522c-4b69-8fbc-0aad52d6f4bf','28',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',28),('452171c5-aef4-4087-8935-51156469c4c9','66',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',66),('45615e40-e7fd-4a82-bb1c-370a943b07e6','91',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',97),('46221809-13c6-4c2b-9ccd-6a2fcef64150','73',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',71),('46c865bd-ac01-4f15-9958-7ed5da2bff1c','12',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',12),('47067c4d-3c86-435b-aa99-50b45dfcc9fb','35',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',35),('48094fff-dc3a-4c22-91e7-5065ab65c636','14',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',14),('48e76904-f1b8-4fa0-ad20-62551612488d','91',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',97),('4967fdfa-b16a-491e-a937-2786f8363aa2','93',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',95),('496f7c23-4f3b-4854-a808-e24fe5b28d78','53',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',47),('49e3ea3c-bf49-44f4-9ec2-aedd13a3103b','18',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',18),('4a94f796-4e7e-4f55-8302-dfe428b5842c','25',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',31),('4ad9b0b7-1ae3-41cc-b432-08d813dcc3b0','77',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',67),('4c0fe4ca-7746-46db-a654-59a7dff1d51b','16',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',16),('4c9940a9-dd48-45e1-8211-a0a5469adbaa','55',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',45),('4d189b0d-86af-4451-8e34-ac02c1ef59d2','20',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',20),('4e26bf4f-f30a-4a42-884c-218dfce8cbcd','61',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',61),('4f538328-1a7d-4251-87b1-29360ab673d0','42',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',42),('4f774e85-14c7-42b0-a266-758a52c3724d','33',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',23),('50cb32e8-7c0d-4375-ab7e-35b9ed6fef5e','101',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',101),('515946a4-5d33-4b49-8cc7-10d626d1c0bd','9',22,'2021-04-20 11:48:31','2021-04-20 11:48:31',3),('51aceac3-4f90-44a6-812e-7f61951642bf','76',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',68),('52c44aea-615a-411b-a475-4c73515a1943','90',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',98),('53430677-797a-4cc0-be03-2df99045d17b','86',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',86),('558bf443-4191-4338-948c-9e4f6bce327d','24',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',32),('55b25d9e-82bb-4b32-94ae-d693774db816','64',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',64),('564073ac-a70c-4d3c-beef-a606cd429bc6','51',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',49),('5666703c-36c1-4a7b-b3aa-dba6d768fdf2','64',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',64),('5772b238-f6f1-4ae3-860a-4d3a4af96c12','20',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',20),('57bfa874-92e9-4211-9eb2-a1560cbe6599','92',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',96),('58d1cef6-fe35-4939-ad66-ffa4f9041d86','18',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',18),('58fbc7ec-0e53-43cf-990d-d519faf71155','49',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',51),('595ad0e3-8c98-48c3-b772-70bf4d4ef2cc','92',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',96),('595ff31b-db02-4f64-81cb-08f631100209','15',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',15),('59cd91dc-3f00-4fcb-aae6-f758795917e5','57',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',57),('5abcad6f-0a7d-4e1d-ada4-0cb8f0be0ea0','39',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',39),('5ace2e28-8cb8-4154-a77c-586906f12f96','10',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',2),('5ae4bd53-a368-48cb-be5e-ddee00c0068e','65',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',65),('5bc09195-6bf2-48bc-8e57-158f7d129fc3','85',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',85),('5e58547f-1a83-4bf8-8627-55a2cc80c21b','57',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',57),('5f180aa2-ac23-4b24-b07a-88030311fdc5','33',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',23),('60402418-f2ae-432e-bd74-6b9769b3325c','12',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',12),('61f977ff-0683-4086-aa3f-a2e50717f01f','81',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',81),('623d9c18-eb4c-4f2e-a4f0-cb2de2c3850c','48',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',52),('639f0f28-f7dd-48c4-b0e7-f11adaadc7e5','70',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',74),('6595977e-b887-4b6a-a421-eee648804164','23',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',33),('66215907-81e6-4ba4-9819-e8785848e928','62',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',62),('665a977a-4b1c-428a-9a29-5cd6f2f59ca2','30',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',26),('678b72bc-49e5-4eec-89f7-d7cacac6da37','28',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',28),('693da5c8-6a9d-4395-b97e-e122e415959e','65',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',65),('694f8ee8-d605-47bc-a8ea-1a063807827f','60',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',60),('69d83bd2-87e1-4258-8981-d130f48e3e76','26',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',30),('69fe7df8-db10-4b60-b97c-6ced174195d9','27',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',29),('6b015f8e-7b89-49d6-9f96-c862f304a647','48',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',52),('6bcf421d-3cb0-4062-ac5a-4243fd6d7db4','35',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',35),('6da01f46-e34e-41a9-8a36-e2332369c947','19',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',19),('6da2599b-130c-4e3a-8aef-b3c6b3681b36','50',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',50),('6faf83ae-3005-435e-82dc-66aa0bd42536','38',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',38),('70033480-7a5e-47ab-9ab6-4126c57650e1','99',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',89),('739f4a00-bd22-4354-b4d2-811b9ec72272','2',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',10),('744296e3-12bf-400c-b904-b0fb19573caa','37',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',37),('75fc71bd-6a89-483e-85b1-27d697904c80','93',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',95),('76179b7d-da33-4aa1-b746-3b2c5c0beb7b','62',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',62),('762babb7-745c-473c-876a-6aade39c85b1','47',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',53),('790b3951-dea6-43da-aa46-ddeeb9ff6318','18',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',18),('7a7712da-b4f8-4b2a-a125-deb2575d5e6b','78',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',78),('7a99cd9d-43f4-4c69-8a3d-46ef2a1e3f06','5',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',7),('7bac59d7-6930-4e0a-98a8-f229717462c1','61',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',61),('7d1916b7-df74-4eb5-8eb0-938d74e233c9','87',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',87),('7da8e0fd-b9eb-4428-8424-8c06f3b01c31','107',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',107),('7dd9b13e-ad56-4b64-9414-d3d0bcd768ee','90',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',98),('8042834f-391d-4c92-974a-77c7477cdaab','37',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',37),('812eb18e-f17c-43a5-9e59-b8a74c8bbef8','2',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',10),('815280b5-c5f4-4c23-8f50-5c7b76d46a37','46',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',54),('815e8210-cab7-4932-b4d8-316486d7e6b4','49',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',51),('816f2717-1827-446b-a20a-ffc0e2d81ff9','36',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',36),('8273329a-dea9-4681-8ad1-bf68f82feaaa','13',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',13),('827fbb79-5d16-4a69-950a-c8be87fbcb12','11',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',1),('829d17de-7bdf-415f-ba96-e8bc3382841f','70',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',74),('82dbca27-8509-4281-acca-5594e5acfde4','91',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',97),('855dea5f-b61e-45e1-add5-d2ea06666bed','69',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',75),('8678b1d9-1f66-4ecf-9ad8-174cad4f0c32','109',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',109),('87a37ae1-723a-47e3-87b2-e4ee6c9361b4','101',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',101),('8968e395-5bce-4b40-a208-ecf8e852cd6b','43',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',43),('8a49c236-9fec-4ae0-be34-0b0a6e7e2fcb','26',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',30),('8ac6f543-415c-4b3e-8006-ca40fcec3768','102',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',102),('8b81dd1c-92fb-4215-b9d7-37103bfeb89b','46',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',54),('8c28e37c-9765-471d-92b0-f42a43040968','75',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',69),('8d693673-586c-4c02-8acb-28dcd37e4ab8','93',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',95),('8dc06ba1-e4d7-4107-9b32-aa57725f0361','59',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',59),('8dfe4a40-dd51-48a6-ab66-20a32149bd1f','96',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',92),('8ea72d76-68af-42b8-97bd-ab5386b7a743','10',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',2),('8efc6ad4-d968-47c4-aa01-9b4853948a37','85',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',85),('8f21f25d-e98e-4d98-a420-e92ed3e8cd21','109',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',109),('8f90ba6e-f4cd-4f53-ba73-1ab32be5c541','71',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',73),('8fccedb3-0945-4e96-9e3b-ee9dad15fad8','81',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',81),('9066a01c-af63-426f-b161-69c984b41994','28',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',28),('908fdcb7-9d5f-46f7-924b-b50ca338c010','51',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',49),('9349af79-0b7d-4711-a58c-775755b6f92b','4',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',8),('949b6b8d-2b63-4235-9fe4-8cff4babd126','95',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',93),('96336024-d892-45c9-856d-44680bf078d3','102',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',102),('966709e8-bb88-4b64-9ae9-56482a64174d','3',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',9),('9791e4c3-849a-42e0-954b-2f00ee0e86bd','92',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',96),('979ffad8-e3d9-4331-b588-4d06e0b8f716','38',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',38),('9976f387-6db2-413a-85a6-c7da842b8c4b','104',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',104),('99828795-5876-42e4-a3cb-35bc1e7207f2','31',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',25),('9a112e68-e0e7-4e4c-9ce5-ecb78589e448','108',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',108),('9add694e-684a-4469-a606-8598f06b3627','17',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',17),('9c9cb06c-8e23-4280-a804-04ddab4ce70f','77',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',67),('9d538b22-de0e-4de7-83e8-6522f78feaac','26',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',30),('9d8f94b0-54df-4840-ace2-9790fac3fc67','82',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',82),('9e3a0c2c-08c8-4285-9aed-bc940479d5c8','104',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',104),('9f2c8547-8288-473e-a1fd-9748b6012d30','71',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',73),('9f6a5248-7833-4470-b4ea-7dd5d67b4e6b','89',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',99),('a2f0ddcd-b2fd-4dd8-b9a1-b95d20dafe92','47',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',53),('a4eb9dfa-c8ea-44f0-9950-f923783fa55f','103',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',103),('a51f99c0-a37a-47d6-a2ab-1c05533ccf4f','39',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',39),('a65f4851-088c-4544-8077-c9180b5b2094','81',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',81),('a7831ca3-6ca5-44db-8b0f-5085c26fa1c9','1',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',11),('a7adf168-ee3b-4032-80f4-3548d98115fb','74',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',70),('a91d4240-182c-4d20-8c47-cdf1821dd880','16',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',16),('a98048e5-7be4-4d90-ab45-60d180075fe7','68',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',76),('aafe6b3f-14d2-4bba-b2da-8868f58975c6','41',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',41),('ab17c77a-5340-4d9e-9025-c872c490905c','33',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',23),('ac80b9e1-33db-4d6f-a2db-320aec7a4015','107',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',107),('ac96b084-d2eb-442a-bb60-392dced3a629','95',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',93),('ad89c8e4-3f4a-4763-bd98-cf26bfd684fc','19',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',19),('af25b064-e2d9-4145-8500-fbad46c65c31','63',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',63),('afdf3621-0410-43a2-9740-22ee73241de2','6',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',6),('b14f0827-054b-4ec0-a636-2bf4ef049a04','17',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',17),('b15afef5-9486-409b-8fba-526a8d383293','70',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',74),('b16d20b5-636d-4a38-8f70-d074146f9447','58',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',58),('b3abdd5e-5714-416f-b454-6b27f61100f5','94',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',94),('b3ef6319-cf44-4968-b52f-dd1170241dbb','100',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',100),('b4a9b6a0-ae98-4ef2-a471-d80df8d4cfe2','96',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',92),('b4d57009-fc43-4a82-be19-975ba8cbe5be','103',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',103),('b6c22f95-1920-45a1-9d43-a8a7b7140ccb','37',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',37),('b6f87c01-f202-44fe-8fa2-52e794978495','30',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',26),('b7bcd06d-8960-40d8-a427-2db9d1aea9d5','99',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',89),('b96dd58c-a2e6-4c5c-84e2-a22b8a675713','86',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',86),('ba397f5b-d20a-4b20-8ea2-78dde79f5164','25',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',31),('bae86a12-38c3-4fa9-8872-19c3c65cdad4','32',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',24),('bb7e3299-c82d-4f1c-ad45-f5f4d61082a3','88',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',88),('bbdb84a5-9ccd-450b-8e58-48fb9ac355d0','89',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',99),('bbfaab92-654f-42c4-b4e1-e00684473574','9',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',3),('bd655c90-814d-4c75-8cc7-1bc1592d9005','15',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',15),('be395114-d31b-47c7-a3a7-b31db1b9f4ca','88',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',88),('be41d818-aeab-4f30-a73b-366f8cc996c1','63',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',63),('bf5ae7e2-44bd-4fdf-9229-95fec6b6eabf','14',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',14),('c0247674-cca4-4458-be8c-f06f884a53de','22',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',22),('c173ac7c-beaf-418c-b12d-c90ce477b3a0','3',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',9),('c18144ff-0a63-4602-83fb-4fe084084abf','60',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',60),('c2a8d100-c4e2-47cd-9474-4db57787f019','63',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',63),('c42977a6-8ecc-4ae9-8192-6cadf5003b9d','8',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',4),('c46f1170-08b7-4a88-8612-54db9bc2c550','50',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',50),('c7859f1e-e70c-4d49-9f08-aef5eaa99a9e','76',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',68),('c83360b7-f3c0-463b-8a1c-f0f64e545b97','97',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',91),('c8b83c64-369f-404c-a9ba-cf8c97cb8ca3','104',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',104),('ca08d3de-d888-4266-adfa-a76d53d2fea7','72',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',72),('ca5dc2ee-c233-422b-9719-a943f559fd9d','12',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',12),('cac142ea-414e-4718-b2f1-1a793fbdaa2e','40',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',40),('caf18599-cdf6-4acf-94d9-80b4d1960e50','55',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',45),('cbfc4c5e-552e-42c1-aeee-d8b62f5f3679','45',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',55),('cc9e24bc-043b-4836-a52f-91aabc58fc1c','15',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',15),('cda5aeb6-11f3-4910-8e7e-b10e9cf003c9','31',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',25),('cdd0cd54-f205-4ada-8c5c-4189230bc106','50',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',50),('cde55249-faa3-48c9-a03b-b8504771bfa8','67',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',77),('cedf613c-94e0-4688-8d77-175716033f42','21',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',21),('cef2dd26-7c26-4a59-a251-56f9a6fcfc11','45',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',55),('cf28003d-d507-485b-9744-3b0a7b9160bb','35',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',35),('cff88873-fd14-412f-80d7-be89925c3ff5','38',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',38),('d1304480-f0b8-4c35-b294-57ea42ce5297','52',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',48),('d2461ce8-578d-4d8c-b64c-b8c4f5689ba1','82',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',82),('d263557f-f9e4-473d-b304-fa7251ff6124','69',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',75),('d26f16c2-6764-477c-8326-700356c3e679','68',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',76),('d3c37b18-a2e0-416d-99f4-5d6d1a4b737c','44',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',44),('d4a2891d-65e1-4aba-a129-4dcb8b426add','57',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',57),('d528ea44-e474-4ebb-9897-4cefe39cad2b','80',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',80),('d5581c70-8b43-47d7-88a8-ab897b9b4f8e','58',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',58),('d5e0756b-f058-4f1f-9256-21288cc89afb','79',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',79),('d62e161a-c6f7-46f9-890c-b9062abaddeb','44',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',44),('d8ec2c35-7e86-4c81-aa53-7b8856496e27','97',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',91),('d9beb6ce-44a0-4654-b1d2-d260b02eb890','23',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',33),('da08066b-0685-4000-9f40-c25f207e4dbf','29',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',27),('dad7da44-ace8-40b5-913d-123a74c6e392','24',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',32),('db903f44-8edf-4e1b-baa0-e240973ba099','34',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',34),('dc32bfb3-3bec-4d8a-aa06-7878460333f1','110',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',110),('dce8e2c3-ade0-451b-b74c-389534c791f7','100',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',100),('dcf830cb-395d-4529-a806-933836720494','34',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',34),('de7e9140-7e1e-4994-b70c-dd3fbfce212a','76',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',68),('de9d9ba3-90d4-46cd-8b9d-e620c8adfa8b','83',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',83),('df27c949-86ec-45e6-ad3a-e4da27d17471','98',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',90),('df5c179d-e966-4698-afe4-2618f478deb8','32',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',24),('dfa67185-5607-48ff-9adf-161dbf5604c5','66',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',66),('dfc6ed17-8360-40eb-911a-0ab0e180b4a0','41',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',41),('e0291373-dbc7-4540-b47b-335e68f60c3b','105',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',105),('e079cd80-b562-410e-8cde-ec58bd515eb4','1',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',11),('e0e7256c-09ac-46e2-9c04-d7fd08836e0f','77',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',67),('e0fe4a7e-d4e0-4d01-b384-36b79883092c','110',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',110),('e1a987a8-b14b-48e0-8c61-f96d17d92ca0','106',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',106),('e1c29339-0f60-47a9-bed6-b585f70b0ad6','67',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',77),('e2da7815-3228-4daf-a705-4abc79d34dbb','54',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',46),('e33dac4f-7510-4c82-b0c6-96d5341df46d','78',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',78),('e3ae09a1-1ff9-4a33-a417-1c1714f89bd0','84',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',84),('e3f7a7ca-6d2a-4fc1-be1f-c140bfe6babd','56',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',56),('e443909d-2b52-4e53-a37d-12d780379a79','89',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',99),('e46159a1-be5b-4254-893d-3be1be7c6a72','43',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',43),('e5192c01-9594-472e-a223-a8efb947dc0d','19',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',19),('e5bdb019-129d-4fde-bdb8-3eb5aca7d488','56',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',56),('e749dd29-36db-476e-96e6-e07d736f98af','59',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',59),('e77dad5b-81c3-4abb-86b0-a6d7ad44a79b','100',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',100),('e7cdbef1-f2b5-4b62-8854-f3d510629797','22',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',22),('e83b3f8c-8545-4e99-b6c1-8bb6635827e2','88',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',88),('e8478742-a577-43cf-b15a-1dd2d80ba9d3','54',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',46),('e9f8fe95-2a88-41b2-8612-79f0272b03e2','84',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',84),('ea61c7a3-2b85-4704-98e4-c3f9496a0026','23',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',33),('ea991721-8e75-4366-b364-476be00591e7','73',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',71),('ed8f049e-897d-4ba3-9fc3-e9cf75ecf0a8','27',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',29),('edc9a73e-66fb-4679-a360-b944e0ab1bc7','101',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',101),('ef76c5ef-1f13-42d4-9d39-67582e7dde63','14',21,'2021-04-16 02:41:54','2021-04-16 02:41:54',14),('ef97f2ad-7f89-4629-8f17-0348e2bc4245','21',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',21),('efa751d3-008c-4a90-9a1e-8190fabecfdf','93',21,'2021-04-16 02:41:54','2021-04-16 02:41:54',95),('efaa5631-7ecf-4bf0-84f5-8e8bf042956c','72',21,'2021-04-16 02:41:54','2021-04-16 02:41:54',72),('efede58c-285e-43c2-b835-5ab5a76c0894','13',21,'2021-04-16 02:41:54','2021-04-16 02:41:54',13),('f00f5cb6-893f-4739-9837-f433f30a19d8','78',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',78),('f0107b02-770e-442a-91c7-d72f3f1f85a2','45',21,'2021-04-16 02:41:54','2021-04-16 02:41:54',55),('f123d40b-2889-4f4b-b15b-7c30d9edd6d8','67',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',77),('f15b7fd5-f1ca-4996-9191-dbc43ce6a109','11',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',1),('f1b3dc3f-c2d9-4f50-92ef-8279ebcec7c4','106',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',106),('f1fdd67b-ad31-4a33-a276-6a4151049925','59',21,'2021-04-16 02:41:54','2021-04-16 02:41:54',59),('f3166bc5-ef35-4c91-a3e7-d31c870a6333','71',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',73),('f446b43d-8d2b-4dae-b2cd-8fa3dba00f2a','54',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',46),('f4a20258-ad95-498d-abe5-6c6b329365e4','39',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',39),('f4c638b6-71f5-47b4-876a-30cd86c49f5e','80',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',80),('f507dcc9-0999-4c7d-8a35-43e2fa193874','72',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',72),('f50ed576-a874-4b7a-b7d6-204bc34a0118','87',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',87),('f5369715-9a7c-4b17-9a3d-1a1e1e42d014','74',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',70),('f57a0077-790c-49f0-9802-fbcd8456d87c','68',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',76),('f6775b3f-6cad-4468-8bc5-61d216857ae7','108',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',108),('f70c9189-f43c-40a5-8ada-259cb37068e4','103',21,'2021-04-16 02:41:54','2021-04-16 02:41:54',103),('f7172975-acfd-48f4-ab6b-79a7b6869e01','87',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',87),('f7355c22-0333-4a4d-b3d6-d3e0b71f219a','99',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',89),('f74bbbfb-1024-4a91-a337-20707167049b','43',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',43),('f8ae6b74-a443-4617-abb9-61338686bb50','61',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',61),('f94775f2-a895-49f5-8016-88418d851dd7','8',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',4),('f94b3adc-8216-4ede-8eab-9df6df38d183','4',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',8),('f94ebeeb-9139-48a5-ace2-93fa9aa187a9','110',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',110),('f980a44f-8553-4531-a1b5-aa78b7bcbd14','107',21,'2021-04-16 02:41:54','2021-04-16 02:41:54',107),('f9c856a3-06fb-49d2-9a34-f7a2eb0a9b93','109',21,'2021-04-16 02:41:54','2021-04-16 02:41:54',109),('f9fd84a7-0ea1-4667-a795-4134a801d58c','62',21,'2021-04-16 02:41:54','2021-04-16 02:41:54',62),('fa7e5abc-6883-4b52-aeeb-76017e840c9b','44',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',44),('fb25ecdb-791b-4233-a70e-f3dc3cef9165','58',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',58),('fbd4d7cf-04bf-4f63-98c3-80dd86a2f8b0','73',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',71),('fbea9b3e-deb4-4bab-a71c-dde15b299036','47',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',53),('fc21dfd7-6e11-408f-a62f-ce9b70b112c2','86',23,'2021-05-03 00:13:50','2021-05-03 00:13:50',86),('fdf2c2f4-5169-47e8-8262-4debe95d47c8','73',21,'2021-04-16 02:41:54','2021-04-16 02:41:54',71),('ff14b660-0ded-4156-939d-8eb4678b9eb7','42',22,'2021-04-20 11:48:32','2021-04-20 11:48:32',42),('ffc2d0b8-3bc9-4304-ac7c-85905357d158','11',21,'2021-04-16 02:41:54','2021-04-16 02:41:54',1),('ffc8afbb-7837-4a24-aa57-4fdbeabfa1ca','45',20,'2021-05-03 07:48:24','2021-05-03 07:48:24',55);
/*!40000 ALTER TABLE `gantungan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `logs`
--

DROP TABLE IF EXISTS `logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `logs` (
  `id` varchar(60) DEFAULT NULL,
  `controller` text,
  `topic` varchar(100) DEFAULT NULL,
  `source_ip` varchar(60) DEFAULT NULL,
  `browser_agent` text,
  `activity` text,
  `reason` text,
  `status` varchar(60) DEFAULT NULL,
  `created_ad` datetime DEFAULT NULL,
  `login_data` text,
  `user_id` varchar(60) DEFAULT NULL,
  `user_application` varchar(60) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `logs`
--

LOCK TABLES `logs` WRITE;
/*!40000 ALTER TABLE `logs` DISABLE KEYS */;
INSERT INTO `logs` VALUES ('096ec8cf-b24c-446c-97e4-c5779e0a4dc7','auth','authentication','127.0.0.1','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36','user berhasil melakukan login','','SUCCESS','2021-05-02 10:02:36','{\"id\":\"75177627-0980-4bf4-b7d6-a9c2b69a73e0\",\"name\":\"Admin Gacor\",\"username\":\"admin@gacorapp.id\",\"email\":\"admin@gacorapp.id\",\"password\":\"$2y$13$yL1RRyklIl3e9V\\/9a1ZDrueu9tN7dA4accLIk2kIMVutFDCJ2i\\/Sa\",\"created_at\":\"2021-05-02 09:59:00\",\"updated_at\":\"2021-05-02 09:59:00\",\"is_deleted\":0,\"last_signin_at\":null,\"last_signout_at\":null,\"last_access\":null,\"role_id\":\"5dc266a2-09d8-402e-9fcf-9b64a558f092\"}','75177627-0980-4bf4-b7d6-a9c2b69a73e0','JOSBOS'),('47e4bfab-74b4-4fd6-8811-cf57752af01b','auth','authentication','127.0.0.1','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36','user gagal melakukan login','Password yang anda masukan salah.','ERROR','2021-05-02 10:03:39','Guest','','JOSBOS'),('0ded991b-725e-4610-a0c5-645ba43d4065','auth','authentication','127.0.0.1','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36','user berhasil melakukan login','','SUCCESS','2021-05-02 10:03:48','{\"id\":\"75177627-0980-4bf4-b7d6-a9c2b69a73e0\",\"name\":\"Admin Gacor\",\"username\":\"admin@gacorapp.id\",\"email\":\"admin@gacorapp.id\",\"password\":\"$2y$13$yL1RRyklIl3e9V\\/9a1ZDrueu9tN7dA4accLIk2kIMVutFDCJ2i\\/Sa\",\"created_at\":\"2021-05-02 09:59:00\",\"updated_at\":\"2021-05-02 09:59:00\",\"is_deleted\":0,\"last_signin_at\":null,\"last_signout_at\":null,\"last_access\":null,\"role_id\":\"5dc266a2-09d8-402e-9fcf-9b64a558f092\"}','75177627-0980-4bf4-b7d6-a9c2b69a73e0','JOSBOS'),('b8463d4c-a402-4275-8e8c-edafb148604f','auth','authentication','127.0.0.1','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36','user berhasil melakukan login','','SUCCESS','2021-05-02 10:06:12','{\"id\":\"75177627-0980-4bf4-b7d6-a9c2b69a73e0\",\"name\":\"Admin Gacor\",\"username\":\"admin@gacorapp.id\",\"email\":\"admin@gacorapp.id\",\"password\":\"$2y$13$yL1RRyklIl3e9V\\/9a1ZDrueu9tN7dA4accLIk2kIMVutFDCJ2i\\/Sa\",\"created_at\":\"2021-05-02 09:59:00\",\"updated_at\":\"2021-05-02 09:59:00\",\"is_deleted\":0,\"last_signin_at\":null,\"last_signout_at\":null,\"last_access\":null,\"role_id\":\"5dc266a2-09d8-402e-9fcf-9b64a558f092\"}','75177627-0980-4bf4-b7d6-a9c2b69a73e0','JOSBOS'),('7510bc63-d8d4-44be-915d-7f5d48378f68','auth','authentication','127.0.0.1','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36','user melogout akunnya','','SUCCESS','2021-05-02 10:07:44','{\"id\":\"75177627-0980-4bf4-b7d6-a9c2b69a73e0\",\"name\":\"Admin Gacor\",\"username\":\"admin@gacorapp.id\",\"email\":\"admin@gacorapp.id\",\"password\":\"$2y$13$yL1RRyklIl3e9V\\/9a1ZDrueu9tN7dA4accLIk2kIMVutFDCJ2i\\/Sa\",\"created_at\":\"2021-05-02 09:59:00\",\"updated_at\":\"2021-05-02 09:59:00\",\"is_deleted\":0,\"last_signin_at\":null,\"last_signout_at\":null,\"last_access\":null,\"role_id\":\"5dc266a2-09d8-402e-9fcf-9b64a558f092\"}','75177627-0980-4bf4-b7d6-a9c2b69a73e0','JOSBOS'),('f53a3eef-41c5-46d1-bfc4-6aff1f78cee6','auth','authentication','127.0.0.1','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36','user berhasil melakukan login','','SUCCESS','2021-05-02 11:24:39','{\"id\":\"75177627-0980-4bf4-b7d6-a9c2b69a73e0\",\"name\":\"Admin Gacor\",\"username\":\"admin@gacorapp.id\",\"email\":\"admin@gacorapp.id\",\"password\":\"$2y$13$yL1RRyklIl3e9V\\/9a1ZDrueu9tN7dA4accLIk2kIMVutFDCJ2i\\/Sa\",\"created_at\":\"2021-05-02 09:59:00\",\"updated_at\":\"2021-05-02 09:59:00\",\"is_deleted\":0,\"last_signin_at\":null,\"last_signout_at\":null,\"last_access\":null,\"role_id\":\"5dc266a2-09d8-402e-9fcf-9b64a558f092\"}','75177627-0980-4bf4-b7d6-a9c2b69a73e0','JOSBOS'),('8c407e9e-9f18-4d04-9294-6839553237a6','auth','authentication','127.0.0.1','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36','user melogout akunnya','','SUCCESS','2021-05-02 11:26:08','{\"id\":\"75177627-0980-4bf4-b7d6-a9c2b69a73e0\",\"name\":\"Admin Gacor\",\"username\":\"admin@gacorapp.id\",\"email\":\"admin@gacorapp.id\",\"password\":\"$2y$13$yL1RRyklIl3e9V\\/9a1ZDrueu9tN7dA4accLIk2kIMVutFDCJ2i\\/Sa\",\"created_at\":\"2021-05-02 09:59:00\",\"updated_at\":\"2021-05-02 09:59:00\",\"is_deleted\":0,\"last_signin_at\":null,\"last_signout_at\":null,\"last_access\":null,\"role_id\":\"5dc266a2-09d8-402e-9fcf-9b64a558f092\"}','75177627-0980-4bf4-b7d6-a9c2b69a73e0','JOSBOS');
/*!40000 ALTER TABLE `logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaksi`
--

DROP TABLE IF EXISTS `transaksi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `transaksi` (
  `id` varchar(60) NOT NULL,
  `peserta_booking_id` varchar(60) DEFAULT NULL,
  `nominal` varchar(255) DEFAULT NULL,
  `created_ad` datetime DEFAULT NULL,
  `updated_ad` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaksi`
--

LOCK TABLES `transaksi` WRITE;
/*!40000 ALTER TABLE `transaksi` DISABLE KEYS */;
/*!40000 ALTER TABLE `transaksi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jenis_burung`
--

DROP TABLE IF EXISTS `jenis_burung`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `jenis_burung` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `created_ad` datetime DEFAULT NULL,
  `updated_ad` datetime DEFAULT NULL,
  `user_id` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jenis_burung`
--

LOCK TABLES `jenis_burung` WRITE;
/*!40000 ALTER TABLE `jenis_burung` DISABLE KEYS */;
INSERT INTO `jenis_burung` VALUES (3,'Kakak Tua','2021-04-15 10:18:58',NULL,NULL),(4,'Kacer','2021-04-15 10:19:03',NULL,NULL),(5,'Perkutut','2021-04-20 11:46:23',NULL,NULL);
/*!40000 ALTER TABLE `jenis_burung` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `juri`
--

DROP TABLE IF EXISTS `juri`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `juri` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nama_juri` varchar(255) DEFAULT NULL,
  `kode_juri` varchar(255) DEFAULT NULL,
  `event_id` int DEFAULT NULL,
  `created_ad` datetime DEFAULT NULL,
  `updated_ad` datetime DEFAULT NULL,
  `telp_juri` varchar(255) DEFAULT NULL,
  `email_juri` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `juri`
--

LOCK TABLES `juri` WRITE;
/*!40000 ALTER TABLE `juri` DISABLE KEYS */;
INSERT INTO `juri` VALUES (5,'Stephen Coy','XauHzgGVJh',6,'2021-04-04 13:36:16',NULL,'081388055771','stephencoy@gmail.com');
/*!40000 ALTER TABLE `juri` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `nama_lengkap` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `nohp` varchar(255) DEFAULT NULL,
  `created_ad` datetime DEFAULT NULL,
  `updated_ad` datetime DEFAULT NULL,
  `role` int DEFAULT '2' COMMENT '1=superadm 2=event organize',
  `is_active` int DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'nugrahaazizluthfi@gmail.com','$2y$13$RWnM2.J0FyqdjIRQMreFEOZVu/G/MT1hCsf59i0ncHS4WjA9Eu2ra','Luthfi Aziz Nugraha','nugrahaazizluthfi@gmail.com','089507854000','2021-04-04 09:31:27',NULL,2,1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event_penilaian`
--

DROP TABLE IF EXISTS `event_penilaian`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `event_penilaian` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `type_penilaian_id` int DEFAULT NULL,
  `nilai` decimal(25,2) DEFAULT NULL,
  `event_id` int DEFAULT NULL,
  `created_ad` datetime DEFAULT NULL,
  `updated_ad` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=90 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event_penilaian`
--

LOCK TABLES `event_penilaian` WRITE;
/*!40000 ALTER TABLE `event_penilaian` DISABLE KEYS */;
INSERT INTO `event_penilaian` VALUES (75,6,0.00,18,'2021-04-15 23:16:53',NULL),(76,5,37.50,18,'2021-04-15 23:16:53',NULL),(77,1,100.00,18,'2021-04-15 23:16:53',NULL),(81,4,37.00,22,'2021-04-20 11:52:09',NULL),(82,3,40.00,22,'2021-04-20 11:52:09',NULL),(83,2,15.00,22,'2021-04-20 11:52:09',NULL),(84,1,100.00,22,'2021-04-20 11:52:09',NULL),(85,4,37.00,20,'2021-05-02 22:10:53',NULL),(86,1,100.00,20,'2021-05-02 22:10:53',NULL),(87,2,15.00,20,'2021-05-02 22:10:53',NULL),(88,4,37.00,23,'2021-05-03 07:38:04',NULL),(89,1,100.00,23,'2021-05-03 07:38:04',NULL);
/*!40000 ALTER TABLE `event_penilaian` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `penilaian`
--

DROP TABLE IF EXISTS `penilaian`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `penilaian` (
  `id` varchar(60) NOT NULL,
  `gantungan_id` varchar(60) DEFAULT NULL,
  `nilai` decimal(25,0) DEFAULT NULL,
  `type_nilai` int DEFAULT NULL,
  `juri_id` int DEFAULT NULL,
  `timestmp` varchar(25) DEFAULT NULL,
  `created_ad` datetime DEFAULT NULL,
  `updated_ad` datetime DEFAULT NULL,
  `type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT 'touch',
  `event_id` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `penilaian`
--

LOCK TABLES `penilaian` WRITE;
/*!40000 ALTER TABLE `penilaian` DISABLE KEYS */;
INSERT INTO `penilaian` VALUES ('2f559236-1f74-4446-bd64-edf6287e9026','bbfaab92-654f-42c4-b4e1-e00684473574',15,2,5,'1620041671','2021-05-03 18:34:31',NULL,'touch',20),('4cd6df5e-7f69-4497-9b78-62432abc4ca7','bbfaab92-654f-42c4-b4e1-e00684473574',100,1,5,'1620041670','2021-05-03 18:34:30',NULL,'touch',20),('5a8e756b-4637-4978-97ef-80fcbd154e7d','f15b7fd5-f1ca-4996-9191-dbc43ce6a109',10,2,5,'1620032839','2021-05-03 16:07:19',NULL,'touch',20),('5cd9c315-349a-4172-ba11-ec5ad6f15195','bbfaab92-654f-42c4-b4e1-e00684473574',100,1,5,'1620041668','2021-05-03 18:34:28',NULL,'touch',20),('9ab56f1d-021e-467c-8a63-bed6eb508a6c','f15b7fd5-f1ca-4996-9191-dbc43ce6a109',400,4,5,'1620034850','2021-05-03 16:40:50',NULL,'manual',20),('ab381748-f984-446b-9ade-3f7b7afdfdc4','f15b7fd5-f1ca-4996-9191-dbc43ce6a109',10,1,5,'1620032845','2021-05-03 16:07:25',NULL,'touch',20),('e2bfcf5e-7ad2-4e24-91bf-6d21d44fc50c','f15b7fd5-f1ca-4996-9191-dbc43ce6a109',400,2,5,'1620034850','2021-05-03 16:40:50',NULL,'manual',20),('e5a8d17b-f821-405f-992e-36275218d572','8ea72d76-68af-42b8-97bd-ab5386b7a743',37,4,5,'1620041667','2021-05-03 18:34:27',NULL,'touch',20),('e8071b93-328c-455a-b5ac-44e49c42edce','8ea72d76-68af-42b8-97bd-ab5386b7a743',37,4,5,'1620041673','2021-05-03 18:34:33',NULL,'touch',20),('ed62af39-8202-4b9f-8c33-e06b1a58d3f8','f15b7fd5-f1ca-4996-9191-dbc43ce6a109',400,1,5,'1620034850','2021-05-03 16:40:50',NULL,'manual',20),('ede6846b-4b69-4d56-8c36-540c6dd3f48d','f15b7fd5-f1ca-4996-9191-dbc43ce6a109',10,4,5,'1620032841','2021-05-03 16:07:21',NULL,'touch',20);
/*!40000 ALTER TABLE `penilaian` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-05-03 20:26:18
