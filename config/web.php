<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'modules' => [
        'bo' => [
            'class' => 'app\modules\bo\BusinessOperational'
        ],
        'eo' => [
            'class' => 'app\modules\eo\EventOrganizer'
        ],
        'juri' => [
            'class' => 'app\modules\juri\Juri'
        ],
    ],
    'components' => [ 
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'burungManuk12@@##2021#FahmiHil',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'class'=>'yii\web\User',
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => false,
            'authTimeout' => 60*30,
            'loginUrl' => ['dashboard/login'],
            'identityCookie' => [
                'name' => '_panelUser',
            ]
        ],
        'userbo' => [
            'class'=>'yii\web\User',
            'identityClass' => 'app\modules\bo\models\User',
            'enableAutoLogin' => false,
            'authTimeout' => 60*30,
            'loginUrl' => ['bo/auth/signin'],
            'identityCookie' => [
                'name' => '_panelUser',
            ]
        ],
        'usereo' => [
            'class'=>'yii\web\User',
            'identityClass' => 'app\modules\eo\models\UserEo',
            'enableAutoLogin' => false,
            'authTimeout' => 60*30,
            'loginUrl' => ['eo/eo-auth/login'],
            'identityCookie' => [
                'name' => '_panelEo',
            ]
        ],
        'userjuri' => [
            'class'=>'yii\web\User',
            'identityClass' => 'app\modules\juri\models\UserJuri',
            'enableAutoLogin' => false,
            'authTimeout' => 60*30,
            'loginUrl' => ['juri/juri-auth/login'],
            'identityCookie' => [
                'name' => '_panelJuri',
            ]
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                'eo' => 'eo/default/index',
                // Event
                'eo/event' => 'eo/default/event',
                'eo/event/edit' => 'eo/default/edit',
                'eo/event/create' => 'eo/default/create',
                'eo/event/save-event' => 'eo/default/save-event',
                'eo/event/delete' => 'eo/default/delete',
                'eo/event/add-penilaian' => 'eo/default/add-penilaian',
                'eo/event/save-add-penilaian' => 'eo/default/save-add-penilaian',

                // Juri
                'eo/juri' => 'eo/juri/juri',
                'eo/juri/edit' => 'eo/juri/edit',
                'eo/juri/create' => 'eo/juri/create',
                'eo/juri/save-juri' => 'eo/juri/save-juri',
                'eo/juri/update-juri' => 'eo/juri/update-juri',
                'eo/juri/delete' => 'eo/juri/delete',

                // Login Eo
                'eo/login' => 'eo/eo-auth/login',
                /* register page alias */
                'eo/register' => 'eo/eo-auth/register',
                'eo/register-otp' => 'eo/eo-auth/otp',
                /* login page alias */
                'eo/notfound' => 'eo/eo-auth/not-found',
                'eo/register-success' => 'eo/eo-auth/register-success',

                'eo/settings/tipe-gantungan' => 'eo/tipe-gantungan/gantungan',
                
            ],
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    // $config['bootstrap'][] = 'debug';
    // $config['modules']['debug'] = [
    //     'class' => 'yii\debug\Module',
    //     // uncomment the following to add your IP if you are not connecting from localhost.
    //     //'allowedIPs' => ['127.0.0.1', '::1'],
    // ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
