<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;

class BaseController extends Controller{
    public $dataResponse = [
        'data' => null,
        'meta' => [
            'code' => 200,
            'status' => 'success',
            'message' => null
        ],
    ];

    public function asJsonSuccess($data = null, $message = null){
        $this->dataResponse['meta']['message'] = $message;
        $this->dataResponse['data'] = $data;

        return $this->asJson($this->dataResponse);
    }

    public function asJsonError($data = null, $message = null, $code = 400){
        $this->dataResponse['meta']['status'] = 'error';
        $this->dataResponse['meta']['code'] = $code;
        $this->dataResponse['meta']['message'] = $message;
        $this->dataResponse['data'] = $data;

        return $this->asJson($this->dataResponse);
    }
}
