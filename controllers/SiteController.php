<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;

class SiteController extends Controller
{
    public $layout = '@app/views/layouts/plain.php';

    //default top menu
    public $header;
    public $headerText;
    
    //default bottom menu
    public $menu;
    public $menuText;
    
    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
