<?php


use yii\db\Schema;
use yii\db\Migration;

/**
 * Handles the creation of table `{{%icons}}`.
 */
class m210415_050359_create_icons_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%icons}}', [
            'id' => Schema::TYPE_PK,
            'icon' => $this->string(100),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%icons}}');
    }
}
