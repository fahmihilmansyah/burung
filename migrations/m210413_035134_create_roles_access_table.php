<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%roles_access}}`.
 */
class m210413_035134_create_roles_access_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%roles_access}}', [
            'id' => $this->string(60),
            'menu_id' => $this->integer(),
            'role_id' => $this->string(60),
            'PRIMARY KEY(id)',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%roles_access}}');
    }
}
