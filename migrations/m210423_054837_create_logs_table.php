<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%logs}}`.
 */
class m210423_054837_create_logs_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%logs}}', [
            'id' => $this->string(60),
            'controller' => $this->text(),
            'topic' => $this->string(100),
            'source_ip' => $this->string(60),
            'browser_agent' => $this->text(),
            'activity' => $this->text(),
            'reason' => $this->text(),
            'status' => $this->string(60),
            'created_ad' => $this->dateTime(),
            'login_data' => $this->text(),
            'user_id' => $this->string(60),
            'user_application' => $this->string(60),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%logs}}');
    }
}
