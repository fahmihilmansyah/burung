<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%users}}`.
 */
class m210413_033942_create_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%admins}}', [
            'id' => $this->string(60),
            'name' => $this->string(50),
            'username' => $this->string(50),
            'email' => $this->string(50),
            'password' => $this->text(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'is_deleted' => $this->integer(1)->defaultValue(0),
            'last_signin_at' => $this->dateTime(),
            'last_signout_at' => $this->dateTime(),
            'last_access' => $this->string(60),
            'role_id' => $this->string(60),
            'PRIMARY KEY(id)',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%admins}}');
    }
}
