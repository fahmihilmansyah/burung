<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Handles the creation of table `{{%menus}}`.
 */
class m210413_035503_create_menus_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%menus}}', [
            'id' => Schema::TYPE_PK,
            'sort_number' => $this->integer()->defaultValue(0),
            'name' => $this->string(50),
            'path' => $this->string(200),
            'icon' => $this->string(50),
            'parent' => $this->string(60)->defaultValue('*'),
            'is_active' => $this->integer(1)->defaultValue(0),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'is_deleted' => $this->integer(1)->defaultValue(0),
            'last_access' => $this->string(60),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%menus}}');
    }
}
