<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Url;
use yii\helpers\Html;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <link
      href="https://fonts.googleapis.com/css2?family=Rubik:ital,wght@0,400;0,500;0,600;0,700;1,400;1,500;1,600;1,700&display=swap"
      rel="stylesheet"
    />
    <link
      rel="stylesheet"
      href="https://unicons.iconscout.com/release/v3.0.0/css/line.css"
    />
    <link
      rel="stylesheet"
      href="https://unicons.iconscout.com/release/v3.0.0/css/solid.css"
    />
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/fontawesome.css" integrity="sha384-eHoocPgXsiuZh+Yy6+7DsKAerLXyJmu2Hadh4QYyt+8v86geixVYwFqUvMU8X90l" crossorigin="anonymous"/>
     <script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <?php $this->head() ?>
</head>
<?php $this->beginBody() ?>
<body>
    <div class="AppWrapper">
        <?php 
            if(isset($this->context->header)){
                echo $this->context->renderPartial($this->context->header);
            }
        ?>
        <?= $content ?>
        <?php 
            if(isset($this->context->menu)){
                echo $this->context->renderPartial($this->context->menu);
            }else{
        ?>
            <div class="MainMenu">
                <?= $this->context->renderPartial('//default/_mainmenu'); ?>
            </div>
        <?php
            }
        ?>
    </div>
    <div id="loading" class="loading">
        <?php $img = Url::to('@web/images/ringloading.gif');     ?>
        <img src="<?= $img; ?>" alt="loading"/>
    </div>
</body>
<?php $this->endBody() ?>
</html>
<?php $this->endPage() ?>
<script>
    function stickyHeader(){
        var header = document.getElementById("HeaderSticky");
        var content = document.getElementById("SectionSticky");

        if(header !== null)
        {
            var sticky = header.offsetTop;
            window.onscroll = function() {myFunction()};

            function myFunction() {
                if (window.pageYOffset > sticky) {
                    header.classList.add("AppSimpleHeader--sticky");
                    content.classList.add("paddy-content");
                } else {
                    header.classList.remove("AppSimpleHeader--sticky");
                    content.classList.remove("paddy-content");
                }
            }
        }
    }

    stickyHeader();
</script>
