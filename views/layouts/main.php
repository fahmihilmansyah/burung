<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <style>
        body{
            padding:20px;
        }

        h5{
            font-weight:bold;
            font-size: 18px;
        }

        .list{
        }

        ul li{ padding: 8px;  }

        ul li ul li{
            padding-left:15px;
        }

        ul li a{
            position:relative;
            top:5px;
            padding:5px;
            background: #336633;
            border-radius:5px;
            color: #fff;
            text-decoration: none;
        }

        ul li a:hover{
            text-decoration:none;
            color:#fff;
        }

        .wrapper{
            background: #fff;
            padding: 10px;
            border-radius: 5px;
        }
    </style>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <div class="container">
        <?= $content ?>
    </div>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
