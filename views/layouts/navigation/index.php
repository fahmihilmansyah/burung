<?php 

use yii\helpers\Url;

$menus = Yii::$app->userbo->identity ?? null; 
$baseurl = Url::home(true).'bo/';
?>
<nav class="pcoded-navbar">
    <div class="pcoded-inner-navbar main-menu">
        <div class="pcoded-navigatio-lavel">Navigation</div>
        <ul class="pcoded-item pcoded-left-item">
            <?php 
                if(!empty($menus)){
                    foreach ($menus->role->menus as $key => $menu) {
                        if(count($menu->childrens)){
            ?>  
                        <li class="pcoded-hasmenu">
                            <a href="<?= $menu->path !== '#' ? ($baseurl.$menu->path): 'javascript:void(0)'; ?>">
                                <span class="pcoded-micon"><i class="feather <?= $menu->icon ?? 'icon-home'; ?>"></i></span>
                                <span class="pcoded-mtext"><?= $menu->name ?? ''; ?></span>
                            </a>
                            <ul class="pcoded-submenu">
                                <?php 
                                    foreach ($menu->childrens as $key => $childrens) {
                                ?>
                                    <li>
                                        <a href="<?= $childrens->path !== '#' ? ($baseurl.$childrens->path) : 'javascript:void(0)'; ?>">
                                            <span class="pcoded-micon"><i class="feather <?= $childrens->icon ?? 'icon-home'; ?>"></i></span>
                                            <span class="pcoded-mtext"><?= $childrens->name ?? ''; ?></span>
                                        </a>
                                    </li>
                                <?php
                                    }
                                ?>
                            </ul>
                        </li>
            <?php
                        } else {
            ?>
                        <li>
                            <a href="<?= $menu->path !== '#' ? ($baseurl.$menu->path) : 'javascript:void(0)'; ?>">
                                <span class="pcoded-micon"><i class="feather <?= $menu->icon ?? ''; ?>"></i></span>
                                <span class="pcoded-mtext"><?= $menu->name ?? ''; ?></span>
                            </a>
                        </li>
            <?php
                        }
                    }
                } else { 
            ?>
                    <li class="active pcoded-trigger">
                        <a href="javascript:void(0)">
                            <span class="pcoded-micon"><i class="feather icon-home"></i></span>
                            <span class="pcoded-mtext">Dashboard</span>
                        </a>
                    </li>
                    <li class="pcoded-hasmenu">
                        <a href="javascript:void(0)">
                            <span class="pcoded-micon"><i class="feather icon-home"></i></span>
                            <span class="pcoded-mtext">Menu</span>
                        </a>
                        <ul class="pcoded-submenu">
                            <li>
                                <a href="index-1.htm">
                                    <span class="pcoded-mtext">Default</span>
                                </a>
                            </li>
                            <li class="">
                                <a href="dashboard-crm.htm">
                                    <span class="pcoded-mtext">CRM</span>
                                </a>
                            </li>
                            <li class=" ">
                                <a href="dashboard-analytics.htm">
                                    <span class="pcoded-mtext">Analytics</span>
                                    <span class="pcoded-badge label label-info ">NEW</span>
                                </a>
                            </li>
                        </ul>
                    </li>
            <?php 
                }
            ?>
        </ul>
    </div>
</nav>