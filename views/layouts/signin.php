<?php 
use yii\helpers\Url;
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>Gacor App - Signin </title>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="#">
    <meta name="keywords" content="Admin , Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
    <meta name="author" content="#">
    <!-- Favicon icon -->
    <link rel="icon" href="libraries\assets\images\favicon.ico" type="image/x-icon">
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800" rel="stylesheet">
    <!-- Required Fremwork -->
    <link rel="stylesheet" type="text/css" href="<?= Url::to('@web/admintydashboard/libraries/bower_components/bootstrap/css/bootstrap.min.css'); ?>">
    <!-- themify-icons line icon -->
    <link rel="stylesheet" type="text/css" href="<?= Url::to('@web/admintydashboard/libraries/assets/icon/themify-icons/themify-icons.css'); ?>">
    <!-- ico font -->
    <link rel="stylesheet" type="text/css" href="<?= Url::to('@web/admintydashboard/libraries/assets/icon/icofont/css/icofont.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?= Url::to('@web/admintydashboard/libraries/bower_components/pnotify/css/pnotify.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?= Url::to('@web/admintydashboard/libraries/bower_components/pnotify/css/pnotify.brighttheme.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?= Url::to('@web/admintydashboard/libraries/bower_components/pnotify/css/pnotify.buttons.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?= Url::to('@web/admintydashboard/libraries/bower_components/pnotify/css/pnotify.history.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?= Url::to('@web/admintydashboard/libraries/bower_components/pnotify/css/pnotify.mobile.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?= Url::to('@web/admintydashboard/libraries/assets/pages/pnotify/notify.css'); ?>">
    <!-- Style.css -->
    <link rel="stylesheet" type="text/css" href="<?= Url::to('@web/admintydashboard/libraries/assets/css/style.css'); ?>">

    <link rel="stylesheet" type="text/css" href="<?= Url::to('@web/css/bo.css'); ?>">
</head>

<body class="fix-menu">
    <!-- Pre-loader start -->
    <div class="theme-loader">
        <div class="ball-scale">
            <div class='contain'>
                <div class="ring"><div class="frame"></div></div>
                <div class="ring"><div class="frame"></div></div>
                <div class="ring"><div class="frame"></div></div>
                <div class="ring"><div class="frame"></div></div>
                <div class="ring"><div class="frame"></div></div>
                <div class="ring"><div class="frame"></div></div>
                <div class="ring"><div class="frame"></div></div>
                <div class="ring"><div class="frame"></div></div>
                <div class="ring"><div class="frame"></div></div>
                <div class="ring"><div class="frame"></div></div>
            </div>
        </div>
    </div>
    <!-- Pre-loader end -->

    <section class="login-block">
        <?= $content ?>
    </section>
    
    <script type="text/javascript" src="<?= Url::to('@web/admintydashboard/libraries/bower_components/jquery/js/jquery.min.js'); ?>"></script>
    <script type="text/javascript" src="<?= Url::to('@web/admintydashboard/libraries/bower_components/jquery-ui/js/jquery-ui.min.js'); ?>"></script>
    <script type="text/javascript" src="<?= Url::to('@web/admintydashboard/libraries/bower_components/popper.js/js/popper.min.js'); ?>"></script>
    <script type="text/javascript" src="<?= Url::to('@web/admintydashboard/libraries/bower_components/bootstrap/js/bootstrap.min.js'); ?>"></script>
    <!-- modernizr js -->
    <script type="text/javascript" src="<?= Url::to('@web/admintydashboard/libraries/bower_components/modernizr/js/modernizr.js'); ?>"></script>
    <script type="text/javascript" src="<?= Url::to('@web/admintydashboard/libraries/bower_components/modernizr/js/css-scrollbars.js'); ?>"></script>

    <!-- pnotify -->
    <script type="text/javascript" src="<?= Url::to('@web/admintydashboard/libraries/bower_components/pnotify/js/pnotify.js'); ?>"></script>
    <script type="text/javascript" src="<?= Url::to('@web/admintydashboard/libraries/bower_components/pnotify/js/pnotify.desktop.js'); ?>"></script>
    <script type="text/javascript" src="<?= Url::to('@web/admintydashboard/libraries/bower_components/pnotify/js/pnotify.buttons.js'); ?>"></script>
    <script type="text/javascript" src="<?= Url::to('@web/admintydashboard/libraries/bower_components/pnotify/js/pnotify.confirm.js'); ?>"></script>
    <script type="text/javascript" src="<?= Url::to('@web/admintydashboard/libraries/bower_components/pnotify/js/pnotify.callbacks.js'); ?>"></script>
    <script type="text/javascript" src="<?= Url::to('@web/admintydashboard/libraries/bower_components/pnotify/js/pnotify.animate.js'); ?>"></script>
    <script type="text/javascript" src="<?= Url::to('@web/admintydashboard/libraries/bower_components/pnotify/js/pnotify.history.js'); ?>"></script>
    <script type="text/javascript" src="<?= Url::to('@web/admintydashboard/libraries/bower_components/pnotify/js/pnotify.mobile.js'); ?>"></script>
    <script type="text/javascript" src="<?= Url::to('@web/admintydashboard/libraries/bower_components/pnotify/js/pnotify.nonblock.js'); ?>"></script>
    <!-- i18next.min.js -->
    <script type="text/javascript" src="<?= Url::to('@web/admintydashboard/libraries/bower_components/i18next/js/i18next.min.js'); ?>"></script>
    <script type="text/javascript" src="<?= Url::to('@web/admintydashboard/libraries/bower_components/i18next-xhr-backend/js/i18nextXHRBackend.min.js'); ?>"></script>
    <script type="text/javascript" src="<?= Url::to('@web/admintydashboard/libraries/bower_components/i18next-browser-languagedetector/js/i18nextBrowserLanguageDetector.min.js'); ?>"></script>
    <script type="text/javascript" src="<?= Url::to('@web/admintydashboard/libraries/bower_components/jquery-i18next/js/jquery-i18next.min.js'); ?>"></script>
    <script type="text/javascript" src="<?= Url::to('@web/admintydashboard/libraries/assets/js/common-pages.js'); ?>"></script>
    <script type="text/javascript" src="<?= Url::to('@web/js/jqvalidate/jquery.validate.min.js'); ?>"></script>
    <script type="text/javascript" src="<?= Url::to('@web/js/jqvalidate/additional-methods.min.js'); ?>"></script>
    <script type="text/javascript" src="<?= Url::to('@web/js/global.js'); ?>"></script>
    <script type="text/javascript" src="<?= Url::to('@web/js/signup.js'); ?>"></script>
</body>
</html>
