<?php 
    use yii\helpers\Url;
?>
<div class="MasjedMainMenu">
    <div class="MasjedMainMenu__gateButton">
        <a href="<?= Url::toRoute(['/login']); ?>" class="btn">Masuk</a>
        <a href="<?= Url::toRoute(['/register']); ?>" class="btn MasjedMainMenu__gateButton--main">Daftar</a>
    </div>
</div>