<?php 
use yii\helpers\Url;
?>
<div id="HeaderSticky" class="AppSimpleHeader AppSimpleHeader--white">
    <div class="AppSimpleHeader__wrapper">
        <div>
            <?php $backlink = isset($this->context->backLink) ? $this->context->backLink : 'site/index'; ?>
            <a href="<?= Url::toRoute([$backlink]); ?>"><i class="uil uil-angle-left icon-large"></i></a>
        </div>
        <div>
            <p>
                <?php 
                    $menu = isset($this->context->headerText) ? $this->context->headerText : 'Page Menu';
                    echo $menu;
                ?>
            </p>
        </div>
    </div>
</div>