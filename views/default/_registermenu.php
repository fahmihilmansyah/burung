<?php 
    use yii\helpers\Url;
?>
<div class="AppMainMenu">
    <div class="AppMainMenu__gateButton">
        <button class="btn AppMainMenu__gateButton--main" 
                onclick="doRegister('<?php echo Url::toRoute(['eo-auth/do-register']); ?>')">
                Daftar
        </button>
    </div>
</div>