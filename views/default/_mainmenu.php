<?php 
    use yii\helpers\Url;
?>

<div class="MasjedMainMenu">
    <a href="<?= Url::toRoute(['/']); ?>" class="MasjedMainMenu__button MasjedMainMenu--active">
        <i class="MasjedMainMenu__icon uil uil-home-alt"></i>
        <span>Beranda</span>
    </a>
    <a href="#" class="MasjedMainMenu__button">
        <i class="keranjangTotalTarget">0</i>
        <i class="MasjedMainMenu__icon uil uil-shopping-cart-alt"></i>
        <span>Keranjang</span>
    </a>
    <a href="#" class="MasjedMainMenu__button">
        <i class="MasjedMainMenu__icon uil uil-file-graph"></i>
        <span>Laporan</span>
    </a>
    <a href="<?= Url::toRoute(['settings/index']); ?>" class="MasjedMainMenu__button">
        <i class="MasjedMainMenu__icon uil uil-setting"></i>
        <span>Pengaturan</span>
    </a>
</div>