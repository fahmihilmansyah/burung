<?php

namespace app\modules\eo\models;

class JuriAssignment extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'juri_assignment';
    }
}