<?php

namespace app\modules\eo\models;

class TypePenilaian extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'type_penilaian';
    }

    public function getEventPenilaian()
    {
        return $this->hasOne(EventPenilaian::className(), ['type_penilaian_id' => 'id']);
    }
}