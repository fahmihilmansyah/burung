<?php

namespace app\modules\eo\models;

class EventPenilaian extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'event_penilaian';
    }

    public function getEvent()
    {
        return $this->hasOne(Event::className(), ['id' => 'event_id']);
    }

    public function getPenilaian()
    {
        return $this->hasOne(TypePenilaian::className(), ['id' => 'type_penilaian_id']);
    }
}