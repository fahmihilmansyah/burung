<?php

namespace app\modules\eo\models;

class Event extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'event';
    }

    public function getEventPenilaian()
    {
        return $this->hasOne(EventPenilaian::className(), ['event_id' => 'id']);
    }

    public function getTipeGantungan(){
        return $this->hasOne(TypeGantungan::className(), ['id' => 'type_gantungan_id']);
    }

    public function getJenisBurung(){
        return $this->hasOne(JenisBurung::className(), ['id' => 'jenis_burung']);
    }
}