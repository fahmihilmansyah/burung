<?php

namespace app\modules\eo\models;

class UserEoOtp extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users_otp';
    }
}