<?php

namespace app\modules\eo\models;

class Settings extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'settings';
    }

    public function getUser(){
        return $this->hasOne(UserEo::className(), ['id' => ['user_id']]);
    }
}