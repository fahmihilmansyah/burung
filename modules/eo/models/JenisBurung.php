<?php

namespace app\modules\eo\models;

class JenisBurung extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'jenis_burung';
    }
}