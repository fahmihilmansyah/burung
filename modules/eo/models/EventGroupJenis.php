<?php

namespace app\modules\eo\models;

class EventGroupJenis extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'event_group_jenis_burung';
    }

    public function getJenisBurung(){
        return $this->hasOne(JenisBurung::className(),['id' => 'jenis_burung_id']);
    }
}