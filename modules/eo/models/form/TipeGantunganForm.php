<?php 

namespace app\modules\eo\models\form;

class TipeGantunganForm extends \yii\base\Model
{
    public $id;
    public $name;
    public $jumlah_row;
    public $jumlah_col;
    public $type_gnt;
    public $user_id;

    public function rules()
    {
        return [
            [['id','user_id'],'safe'],
            // required
            [['name'],'required','message'=>'Inputan ini wajib diisi.'],
            [['jumlah_row'],'required','message'=>'Inputan ini wajib diisi.'],
            [['jumlah_col'],'required','message'=>'Inputan ini wajib diisi.'],
            [['type_gnt'],'required','message'=>'Inputan ini wajib diisi.'],
        ];
    }
}
