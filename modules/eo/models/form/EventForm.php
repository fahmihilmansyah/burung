<?php 

namespace app\modules\eo\models\form;

class EventForm extends \yii\base\Model
{
    public $id;
    public $nama_event;
    public $tgl_event;
    public $nominal;
    public $waktu;
    // public $tipe_penilaian_id;

    public function rules()
    {
        return [
            [['id'],'safe'],
            [['nama_event','tgl_event'],'string'],
            [['nominal'],'integer'],
            // required
            [['nama_event'],'required','message'=>'Inputan ini wajib diisi.'],
            [['tgl_event'],'required','message'=>'Inputan ini wajib diisi.'],
            [['waktu'],'required','message'=>'Inputan ini wajib diisi.'],
            [['nominal'],'required','message'=>'Inputan ini wajib diisi.'],
        ];
    }
}
