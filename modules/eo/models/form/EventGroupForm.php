<?php 

namespace app\modules\eo\models\form;

class EventGroupForm extends \yii\base\Model
{
    public $id;
    public $name;
    public $type_class;
    public $desc;
    // public $tipe_penilaian_id;

    public function rules()
    {
        return [
            [['id'],'safe'],
            // required
            [['name'],'required','message'=>'Inputan ini wajib diisi.'],
            [['type_class'],'required','message'=>'Inputan ini wajib diisi.'],
            [['desc'],'required','message'=>'Inputan ini wajib diisi.'],
        ];
    }
}
