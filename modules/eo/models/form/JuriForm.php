<?php 

namespace app\modules\eo\models\form;

class JuriForm extends \yii\base\Model
{
    public $id;
    public $nama_juri;
    public $pemilik_tempat_id;
    public $kode_juri;
    public $event_id;
    public $telp_juri;
    public $email_juri;

    public function rules()
    {
        return [
            [['id'],'safe'],
            // required
            [['nama_juri'],'required','message'=>'Inputan ini wajib diisi.'],
            [['pemilik_tempat_id'],'required','message'=>'Inputan ini wajib diisi.'],
            [['event_id'],'required','message'=>'Inputan ini wajib diisi.'],
            [['telp_juri'],'required','message'=>'Inputan ini wajib diisi.'],
            [['email_juri'],'required','message'=>'Inputan ini wajib diisi.'],
        ];
    }
}
