<?php 

namespace app\modules\eo\models\form;

class JenisBurungForm extends \yii\base\Model
{
    public $id;
    public $name;
    public $user_id;

    public function rules()
    {
        return [
            [['id','user_id'],'safe'],
            // required
            [['name'],'required','message'=>'Inputan ini wajib diisi.'],
        ];
    }
}
