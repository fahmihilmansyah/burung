<?php 

namespace app\modules\eo\models\form;

class EventAddPenilaianForm extends \yii\base\Model
{
    public $tipe_penilaian_id;

    public function rules()
    {
        return [
            // required
            ['tipe_penilaian_id','each','rule'=>['required','message' => 'penilaian tidak boleh dikosongkan']],
        ];
    }
}
