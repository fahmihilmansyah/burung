<?php 

namespace app\modules\eo\models\form;

class EventGroupAddJenisForm extends \yii\base\Model
{
    public $tipe_penilaian_id;

    public function rules()
    {
        return [
            // required
            ['jenis_burung_id','each','rule'=>['required','message' => 'jenis burung tidak boleh dikosongkan']],
        ];
    }
}
