<?php 

namespace app\modules\eo\models\form;

class SettingsForm extends \yii\base\Model
{
    public $id;
    public $nama_pemilik;
    public $alamat;
    public $notlp;
    public $jadwal_pelaksaan;

    public function rules()
    {
        return [
            [['id','user_id'],'safe'],
            // required
            [['nama_pemilik'],'required','message'=>'Inputan ini wajib diisi.'],
            [['alamat'],'required','message'=>'Inputan ini wajib diisi.'],
            [['notlp'],'required','message'=>'Inputan ini wajib diisi.'],
            [['jadwal_pelaksaan'],'required','message'=>'Inputan ini wajib diisi.'],
        ];
    }
}
