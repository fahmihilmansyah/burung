<?php

namespace app\modules\eo\models;

class EventGroup extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'event_group';
    }

    public function getJenisBurungs(){
        return $this->hasMany(JenisBurung::className(), ['id' => 'event_group_id']);
    }
}