<?php

namespace app\modules\eo\models;
use Yii;
use yii\base\Model;
use app\modules\eo\models\UserEo;

class UserEoLogin extends Model
{
	public $username;
	public $password;
	public $rememberMe = true;

	public $userData = false;
	
	public function rules()
    {
        return [
            // username and password are both required
            [['username', 'password'], 'required'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

	public function authenticate()
    {
		$result = [];
		
        if ($this->validate()) {
			$dataUser = $this->getUser();

			if(!$dataUser){
				$result['status'] = false;
				$result['message'] = 'Anda tidak terdaftar pada sistem kami.';

				return $result;
			}

			if(!Yii::$app->getSecurity()->validatePassword($this->password, $dataUser->password)){
				$result['status'] = false;
				$result['message'] = 'Password yang anda masukan salah.';

				return $result;
			}

            Yii::$app->usereo->login($this->getUser(), ($this->rememberMe ? 3600*24*30 : 0));

			$result['status'] = true;
			$result['message'] = 'Login berhasil.';
			return $result;
        }

		$result['status'] = false;
		$result['message'] = 'Login gagal, inputan tidak diisi.';

        return $result;
    }

	public function getUser()
	{
		return !$this->userData ? UserEo::findByUsername($this->username): false;
	}
}