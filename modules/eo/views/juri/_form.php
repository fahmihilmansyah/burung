<div class="row">
    <div class="col-xs-12">
        <?php echo $form->field($juriModel, 'pemilik_tempat_id')->dropdownList($pemilikTempat,
                        ['prompt'=>'Pilih Pemilik Tempat'],
                        ['id' => 'pemilik_tempat_id']
                    )->label('Pemilik Tempat'); 
        ?>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <?php echo $form->field($juriModel, 'event_id')->dropdownList($eventOptions,
                        ['prompt'=>'Pilih Event'],
                        ['id' => 'event_id']
                    )->label('Event'); 
        ?>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <?= $form->field($juriModel, 'nama_juri')->textInput()->label('Nama') ?>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <?= $form->field($juriModel, 'telp_juri')->textInput()->label('Telpon') ?>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <?= $form->field($juriModel, 'email_juri')->textInput()->label('Email') ?>
    </div>
</div>

<?php if(!empty($id)){ ?>
        <div class="row" style="margin-bottom:40px;">
            <div class="col-xs-8">
                <?= $form->field($juriModel, 'kode_juri')->textInput(['id' => 'kode_juri'])->label('Kode Juri') ?>
            </div>
            <div class="col-xs-4" style="padding:0px;">
                <a href="javascript:void(0)" onclick="generateKode()" class="btn btn-primary" style="margin:0px !important; position:relative; top:25px; left:10px;">Generate Kode</a>
            </div>
        </div>
<?php } ?>
