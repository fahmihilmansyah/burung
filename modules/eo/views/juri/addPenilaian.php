<?php 
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;
    use wbraganca\dynamicform\DynamicFormWidget;

    $this->title = 'GacorApp';
?>
<div class="container-fluid">
    <div class="AppForm">
        <?php 
            $form = ActiveForm::begin([
                'action' => ['default/add-penilaian-event'],
                'method' => 'POST',
                'id' => 'dynamic-form',
                'enableAjaxValidation' => true,
                'enableClientValidation'=> false,
                'validationUrl' => ['default/ajax-validation'],
                'options' => ['class' => 'form-horizontal'],
            ]);
        ?>

        <?php 
            DynamicFormWidget::begin([
                'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                'widgetBody' => '.container-items', // required: css class selector
                'widgetItem' => '.item', // required: css class
                'limit' => 999, // the maximum times, an element can be cloned (default 999)
                'min' => 1, // 0 or 1 (default 1)
                'insertButton' => '.add-item', // css class
                'deleteButton' => '.remove-item', // css class
                'model' => $eventModel[0],
                'formId' => 'dynamic-form',
                'formFields' => [
                    'tipe_penilaian_id',
                ],
            ]); 
        ?>


        <div class="container-items">
            <button type="button" class="add-item btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
            <?php foreach ($eventModel as $i => $rowModel): ?>
                <div class="item panel panel-default"><!-- widgetBody -->
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-12">
                                <?php echo $form->field($rowModel, "tipe_penilaian_id[".($i+1)."]")->dropdownList($typePenilaian,
                                                        ['prompt'=>'Pilih Tipe Penilaian'],
                                                    )->label('Tipe Penilaian');  
                                ?>
                                <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>

        <?php DynamicFormWidget::end(); ?>
        
        <?php ActiveForm::end() ?>
        
        <div class="preview">
            <div class="previewWrapper">
                <div id="previewTarget"></div>
                <div class="closeBtn" onclick="closePreview()"><i class="uil uil-times"></i></div>
            </div>
        </div>
    </div>
</div>