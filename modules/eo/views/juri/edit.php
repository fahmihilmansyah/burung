<?php 
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;

    $this->title = 'GacorApp';
?>
<div class="container-fluid">
    <div class="AppForm">
        <?php 
            $form = ActiveForm::begin([
                'action' => ['juri/update-juri'],
                'method' => 'POST',
                'id' => 'juriForm',
                // 'enableAjaxValidation' => true,
                'enableClientValidation'=>true,
                // 'validationUrl' => ['default/ajax-validation'],
                'options' => ['class' => 'form-horizontal'],
            ]);
        ?>
        
        <?php 
            echo $form->field($juriModel, 'id')
                    ->hiddenInput(['value'=> $juriModel->id])->label(false);
        ?>

        <?= $this->render('@app/modules/eo/views/juri/_form', [
                            'form' => $form, 
                            'juriModel' => $juriModel,
                            'pemilikTempat' => $pemilikTempat,
                            'eventOptions' => $eventOptions,
                            'id' => $juriModel->id
                        ]); ?>
        
        <?php ActiveForm::end() ?>
        
        <div class="preview">
            <div class="previewWrapper">
                <div id="previewTarget"></div>
                <div class="closeBtn" onclick="closePreview()"><i class="uil uil-times"></i></div>
            </div>
        </div>
    </div>
</div>
