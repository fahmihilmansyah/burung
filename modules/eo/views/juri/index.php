<?php 
    use yii\helpers\Url;
    
    $this->title = 'GacorApp';
?>

<div class="AppEvent">
    <?php if (Yii::$app->session->hasFlash('success')): ?>
        <div class="alert alert-success alert-dismissable">
            <h4 style="font-size:11px"><i class="uil uil-check"></i>Success!</h4>
            <?= Yii::$app->session->getFlash('success') ?>
        </div>
    <?php endif; ?>

    <?php if (Yii::$app->session->hasFlash('error')): ?>
        <div class="alert alert-danger alert-dismissable">
            <h4 style="font-size:11px"><i class="uil uil-check"></i>Error!</h4>
            <?= Yii::$app->session->getFlash('error') ?>
        </div>
    <?php endif; ?>

    <?php 
        if(!empty($juris)){
            foreach ($juris as $key => $juri) {
    ?>
            <div class="AppCard">
                <div class="body">
                    <div class="row">
                        <div class="col-xs-12">
                            <h5><?= $juri->nama_juri ?? ''; ?></h5>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <h4>
                                Email<br/>
                                <span><?= $juri->email_juri ?? ''; ?></span>
                            </h4>
                        </div>
                        <div class="col-xs-6">
                            <h4>
                                No Telp<br/>
                                <span><?= $juri->telp_juri ?? ''; ?></span>
                            </h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <h4>
                                Kode Juri<br/>
                                <span><?= $juri->kode_juri ?? ''; ?></span>
                            </h4>
                        </div>
                    </div>
                </div>
                <div class="footer">
                    <a href="<?= Url::toRoute(['juri/edit', 'id' => $juri->id]); ?>" class="btn btn-primary"><i class="uil uil-edit"></i> Edit</a>
                    <a href="javascript:void(0)" class="btn btn-danger" onclick="deleteData('<?= Url::toRoute(['juri/delete','id' => $juri->id]); ?>')"><i class="uil uil-trash"></i> Delete</a>
                </div>
            </div>
    <?php
            }
        }else{
    ?>
            <div class="AppCard">
                No Data
            </div>
    <?php 
        }
    ?>
</div>

<script>
</script>