<?php 
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;
    use kartik\date\DatePicker;

    $this->title = 'GacorApp';
?>
<div class="container-fluid">
    <div class="AppForm">
        <?php 
            $form = ActiveForm::begin([
                'action' => ['event-group/save-event-group'],
                'method' => 'POST',
                'id' => 'eventGroupForm',
                // 'enableAjaxValidation' => true,
                'enableClientValidation'=>true,
                // 'validationUrl' => ['default/ajax-validation'],
                'options' => ['class' => 'form-horizontal'],
            ]);
        ?>

        <?= $this->render('@app/modules/eo/views/event-group/_form', [
                            'form' => $form, 
                            'eventGroupModel' => $eventGroupModel,
                        ]); ?>

        <?php ActiveForm::end() ?>
    </div>
</div>
