<?php use yii\helpers\Url; ?>
<div class="AppMainMenu">
    <button class="btn AppMainMenu__submitButton AppForm--btnSave" onclick="doJenis('<?= Url::toRoute(['event-group/save-add-penilaian']); ?>')">
        <i class="uil uil-save"></i> <br/>Save
    </button>
    <button class="btn AppMainMenu__submitButton AppForm--btnPreview" onclick="addJenis('<?= Url::toRoute(['event-group/get-options']); ?>')">
        <i class="uil uil-plus"></i> <br/>Jenis
    </button>
</div>