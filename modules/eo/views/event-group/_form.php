<?php 
    use kartik\date\DatePicker; 
?>

<div class="row">
    <div class="col-xs-12">
        <?= $form->field($eventGroupModel, 'name')->textInput()->label('Nama Event') ?>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <?= $form->field($eventGroupModel, 'desc')->textArea()->label('Description') ?>
    </div>
</div>
