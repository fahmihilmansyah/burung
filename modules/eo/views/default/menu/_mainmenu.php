<?php use yii\helpers\Url; ?>
<div class="AppMainMenu">
    <a href="<?= Url::toRoute(['default/index']); ?>" class="AppMainMenu__button AppMainMenu--active">
        <i class="AppMainMenu__icon uil uil-home-alt"></i>
        <span>Beranda</span>
    </a>
    <a href="<?= Url::toRoute(['settings/index']); ?>" class="AppMainMenu__button">
        <i class="AppMainMenu__icon uil uil-setting"></i>
        <span>Setting</span>
    </a>
    <a href="<?= Url::toRoute(['eo-auth/logout']); ?>" class="AppMainMenu__button">
        <i class="AppMainMenu__icon uil uil-sign-out-alt"></i>
        <span>Keluar</span>
    </a>
</div>