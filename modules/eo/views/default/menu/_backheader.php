<?php use yii\helpers\Url; ?>
<div id="HeaderSticky" class="AppSimpleHeader">
    <div class="AppSimpleHeader__wrapper">
        <div>
        <?php 
            if(isset($this->context->backLink))
            {
                $backlink = [$this->context->backLink];
                if(is_array($this->context->backLink)){
                    $backlink = $this->context->backLink;
                }
            }
        ?>
            <a href="<?= isset($this->context->backLink) ? Url::toRoute($backlink) : '#'; ?>"><i class="uil uil-angle-left icon-large"></i></a>
        </div>
        <div>
            <p>
                <?php 
                    $menu = isset($this->context->headerText) ? $this->context->headerText : 'Page Menu';
                    echo $menu;
                ?>
            </p>
        </div>
    </div>
</div>