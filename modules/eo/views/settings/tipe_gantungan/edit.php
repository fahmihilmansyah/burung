<?php 
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;
    use kartik\date\DatePicker;

    $this->title = 'GacorApp';
?>
<div class="container-fluid">
    <div class="AppForm">
        <?php 
            $form = ActiveForm::begin([
                'action' => ['tipe-gantungan/update'],
                'method' => 'POST',
                'id' => 'tipeGantunganForm',
                // 'enableAjaxValidation' => true,
                'enableClientValidation'=>true,
                // 'validationUrl' => ['default/ajax-validation'],
                'options' => ['class' => 'form-horizontal'],
            ]);
        ?>

        
        <?php 
            echo $form->field($tipeGantunganModel, 'id')
                    ->hiddenInput(['value'=> $tipeGantunganModel->id])->label(false);
        ?>

        <?= $this->render('@app/modules/eo/views/settings/tipe_gantungan/_form', [
                            'form' => $form, 
                            'tipeGantunganModel' => $tipeGantunganModel,
                        ]); ?>

        <?php ActiveForm::end() ?>
        
        <div class="preview">
            <div class="previewWrapper">
                <div id="previewTarget"></div>
                <div class="closeBtn" onclick="closePreview()"><i class="uil uil-times"></i></div>
            </div>
        </div>
    </div>
</div>
