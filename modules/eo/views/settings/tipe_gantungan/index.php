<?php 
    use yii\helpers\Url;
    
    $this->title = 'GacorApp';
?>

<div class="AppEvent">
    <?php if (Yii::$app->session->hasFlash('success')): ?>
        <div class="alert alert-success alert-dismissable">
            <h4 style="font-size:11px"><i class="uil uil-check"></i>Success!</h4>
            <?= Yii::$app->session->getFlash('success') ?>
        </div>
    <?php endif; ?>

    <?php if (Yii::$app->session->hasFlash('error')): ?>
        <div class="alert alert-danger alert-dismissable">
            <h4 style="font-size:11px"><i class="uil uil-check"></i>Error!</h4>
            <?= Yii::$app->session->getFlash('error') ?>
        </div>
    <?php endif; ?>

    <?php 
        if(!empty($typeGantungans)){
            foreach ($typeGantungans as $key => $gantungan) {
    ?>
            <div class="AppCard">
                <div class="body">
                    <div class="row">
                        <div class="col-xs-12">
                            <h5><?= $gantungan->name ?? ''; ?></h5>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <h4>
                                Jumlah Baris<br/>
                                <span><?= $gantungan->jumlah_row ?? ''; ?></span>
                            </h4>
                        </div>
                        <div class="col-xs-6">
                            <h4>
                                Jumlah Kolom<br/>
                                <span><?= $gantungan->jumlah_col ?? ''; ?></span>
                            </h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <h4>
                                Tipe gantungan<br/>
                                <span><?= $gantungan->type_gnt ?? ''; ?></span>
                            </h4>
                        </div>
                        <div class="col-xs-6">
                            <h4>
                                Kouta Peserta<br/>
                                <?php 
                                    $kuota = intval($gantungan->jumlah_col) * intval($gantungan->jumlah_row);
                                ?>
                                <span><?= $kuota ?? 0; ?></span>
                            </h4>
                        </div>
                    </div>
                </div>
                <div class="footer">
                    <a href="<?= Url::toRoute(['tipe-gantungan/edit', 'id' => $gantungan->id]); ?>" class="btn btn-primary"><i class="uil uil-edit"></i> Edit</a>
                    <a href="javascript:void(0)" class="btn btn-danger" onclick="deleteData('<?= Url::toRoute(['tipe-gantungan/delete','id' => $gantungan->id]); ?>')"><i class="uil uil-trash"></i> Delete</a>
                </div>
            </div>
    <?php
            }
        }else{
    ?>
            <div class="AppCard">
                No Data
            </div>
    <?php 
        }
    ?>
</div>

<script>
</script>