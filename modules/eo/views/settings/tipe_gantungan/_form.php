<div class="row">
    <div class="col-xs-12">
        <?= $form->field($tipeGantunganModel, 'name')->textInput()->label('Judul') ?>
    </div>
</div>
<div class="row">
    <div class="col-xs-6">
        <?= $form->field($tipeGantunganModel, 'jumlah_row')->textInput(['id' => 'jml_col'])->label('Jumlah Baris') ?>
    </div>
    <div class="col-xs-6">
        <?= $form->field($tipeGantunganModel, 'jumlah_col')->textInput(['id' => 'jml_row'])->label('Jumlah Kolom') ?>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <?php echo $form->field($tipeGantunganModel, 'type_gnt')->dropdownList([
                            'ASC' => 'ASC', 
                            'DESC' => 'DESC',
                            'ASCSLINE' => 'ASC - SLINE',
                            'DESCSLINE' => 'DESC - SLINE',
                        ],
                        ['prompt'=>'Pilih Tipe Gantungan'],
                        ['id' => 'type_gnt']
                    )->label('Tipe Gantungan'); 
        ?>
    </div>
</div>