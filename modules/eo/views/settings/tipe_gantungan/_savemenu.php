<?php use yii\helpers\Url; ?>
<div class="AppMainMenu">
    <button class="btn AppMainMenu__submitButton AppForm--btnSave" onclick="submit('tipeGantunganForm')">
        <i class="uil uil-save"></i> <br/>Save
    </button>
    <button class="btn AppMainMenu__submitButton AppForm--btnPreview" onclick="preview('tipegantunganform')">
        <i class="uil uil-eye"></i> <br/>Preview
    </button>
</div>