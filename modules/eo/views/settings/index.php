<?php 
    use yii\helpers\Url;
?>
<div class="AppProfile">
    <div class="container-fluid">
        <div class="AppProfile__toko">
            <h5>Pengaturan Acuan</h5>
            <ul>
                <li>
                    <a href="<?= Url::toRoute(['jenis-burung/jenis-burung']); ?>" class="btn">
                        <i class="uil uil-apps"></i><span>Jenis Burung</span>
                    </a>
                </li>
                <li>
                    <a href="<?= Url::toRoute(['tipe-gantungan/gantungan']); ?>" class="btn">
                        <i class="uil uil-apps"></i><span>Tipe Gantungan</span>
                    </a>
                </li>
                <li>
                    <a href="<?= Url::toRoute(['settings/hari-tempat']); ?>" class="btn">
                        <i class="uil uil-calendar-alt"></i><span>Hari & Tempat Event</span>
                    </a>
                </li>
            </ul>
        </div>
        <div class="spacer"></div>
        <div class="AppProfile__toko">
            <h5>Keamanan</h5>
            <ul>
                <li>
                    <a href="#" class="btn">
                        <i class="uil uil-keyhole-square"></i><span>Ganti Password</span>
                    </a>
                </li>
            </ul>
        </div>
        <div class="spacer"></div>
        <div class="AppProfile__toko">
            <h5>Bantuan</h5>
            <ul>
                <li>
                    <a href="#" class="btn">
                        <i class="uil uil-question-circle"></i><span>Pertanyaan & Jawaban</span>
                    </a>
                </li>
                <li>
                    <a href="#" class="btn">
                        <i class="uil uil-info-circle"></i><span>Tentang Aplikasi</span>
                    </a>
                </li>
                <li>
                    <a href="#" class="btn">
                        <i class="uil uil-receipt"></i><span>Syarat & Ketentuan</span>
                    </a>
                </li>
                <li>
                    <a href="#" class="btn">
                        <i class="uil uil-comment-dots"></i><span>Kontak Kami</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>