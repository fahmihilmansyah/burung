<?php 
    use yii\helpers\Url;
    
    $this->title = 'GacorApp';
?>

<div class="AppEvent">
    <?php if (Yii::$app->session->hasFlash('success')): ?>
        <div class="alert alert-success alert-dismissable">
            <h4 style="font-size:11px"><i class="uil uil-check"></i>Success!</h4>
            <?= Yii::$app->session->getFlash('success') ?>
        </div>
    <?php endif; ?>

    <?php if (Yii::$app->session->hasFlash('error')): ?>
        <div class="alert alert-danger alert-dismissable">
            <h4 style="font-size:11px"><i class="uil uil-check"></i>Error!</h4>
            <?= Yii::$app->session->getFlash('error') ?>
        </div>
    <?php endif; ?>

    <?php 
        if(!empty($JenisBurungs)){
            foreach ($JenisBurungs as $key => $jenisburung) {
    ?>
            <div class="AppCard">
                <div class="body">
                    <div class="row">
                        <div class="col-xs-12">
                            <h5><?= $jenisburung->name ?? ''; ?></h5>
                        </div>
                    </div>
                </div>
                <div class="footer">
                    <a href="<?= Url::toRoute(['jenis-burung/edit', 'id' => $jenisburung->id]); ?>" class="btn btn-primary"><i class="uil uil-edit"></i> Edit</a>
                    <a href="javascript:void(0)" class="btn btn-danger" onclick="deleteData('<?= Url::toRoute(['jenis-burung/delete','id' => $jenisburung->id]); ?>')"><i class="uil uil-trash"></i> Delete</a>
                </div>
            </div>
    <?php
            }
        }else{
    ?>
            <div class="AppCard">
                No Data
            </div>
    <?php 
        }
    ?>
</div>

<script>
</script>