<?php 
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;
    use kartik\date\DatePicker;

    $this->title = 'GacorApp';
?>
<div class="container-fluid">
    <div class="AppForm">
        <?php 
            $form = ActiveForm::begin([
                'action' => ['jenis-burung/update'],
                'method' => 'POST',
                'id' => 'jenisBurungForm',
                // 'enableAjaxValidation' => true,
                'enableClientValidation'=>true,
                // 'validationUrl' => ['default/ajax-validation'],
                'options' => ['class' => 'form-horizontal'],
            ]);
        ?>

        
        <?php 
            echo $form->field($jenisBurungModel, 'id')
                    ->hiddenInput(['value'=> $jenisBurungModel->id])->label(false);
        ?>

        <?= $this->render('@app/modules/eo/views/settings/jenis_burung/_form', [
                            'form' => $form, 
                            'jenisBurungModel' => $jenisBurungModel,
                        ]); ?>

        <?php ActiveForm::end() ?>
        
        <div class="preview">
            <div class="previewWrapper">
                <div id="previewTarget"></div>
                <div class="closeBtn" onclick="closePreview()"><i class="uil uil-times"></i></div>
            </div>
        </div>
    </div>
</div>
