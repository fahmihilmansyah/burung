<?php 
    use yii\helpers\Url;
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;

    $this->title = 'GacorApp';
?>
<div class="AppProfile">
    <div class="container-fluid">
        <div class="AppLogin__form">
        <?php 
            $form = ActiveForm::begin([
                'method' => 'POST',
                'id' => 'formHari',
                'options' => ['class' => 'form-horizontal'],
            ]);
        ?>
            <div class="AppProfile__toko">
                <h5>Tempat Pemilik</h5>
                <div class="spacer"></div>
                <div>
                    <label>Nama Pemilik</label>
                    <input type="text" class="form-control AppInputText" placeholder="Ketikan nama pemilik disini" name="nama_pemilik" value="<?= $owner->nama_pemilik ?? ''; ?>">
                    <div class="error-wrapper"></div>
                </div>
                <br/>
                <div>
                    <label>No Telpon</label>
                    <input type="text" class="form-control AppInputText" placeholder="Ketikan no telpon disini" name="notlp" value="<?= $owner->notlp ?? ''; ?>">
                    <div class="error-wrapper"></div>
                </div>
                <br/>
                <div>
                    <label>Alamat</label>
                    <textarea class="form-control AppInputText" placeholder="Ketikan alamat disini" name="alamat" ><?= $owner->alamat ?? ''; ?></textarea>
                    <div class="error-wrapper"></div>
                </div>
                <br/>
            </div>
            <div class="spacer"></div>
            <div class="AppProfile__toko">
                <h5>Hari Pelaksanaan Event</h5>
                <div class="spacer"></div>
                <div class="myCheckbox">
                    <div class="checkboxlist"> 
                        <label><input type="checkbox" placeholder="Ketikan nama pemilik disini" name="jadwal_pelaksanaan[]" value="1" <?= $senin ?? ''; ?>>&nbsp;Senin</label>
                    </div>  
                    <div class="checkboxlist"> 
                        <label><input type="checkbox" placeholder="Ketikan nama pemilik disini" name="jadwal_pelaksanaan[]" value="2" <?= $selasa ?? ''; ?>>&nbsp;Selasa</label>
                    </div>
                    <div class="checkboxlist"> 
                        <label><input type="checkbox" placeholder="Ketikan nama pemilik disini" name="jadwal_pelaksanaan[]" value="3" <?= $rabu ?? ''; ?>>&nbsp;Rabu</label>
                    </div>  
                    <div class="checkboxlist"> 
                        <label><input type="checkbox" placeholder="Ketikan nama pemilik disini" name="jadwal_pelaksanaan[]" value="4" <?= $kamis ?? ''; ?>>&nbsp;Kamis</label>
                    </div>
                    <div class="checkboxlist"> 
                        <label><input type="checkbox" placeholder="Ketikan nama pemilik disini" name="jadwal_pelaksanaan[]" value="5" <?= $jumat ?? ''; ?>>&nbsp;Jum'at</label>
                    </div>
                    <div class="checkboxlist"> 
                        <label><input type="checkbox" placeholder="Ketikan nama pemilik disini" name="jadwal_pelaksanaan[]" value="6" <?= $sabtu ?? ''; ?>>&nbsp;Sabtu</label>
                    </div>
                    <div class="checkboxlist"> 
                        <label><input type="checkbox" placeholder="Ketikan nama pemilik disini" name="jadwal_pelaksanaan[]" value="0" <?= $minggu ?? ''; ?>>&nbsp;Minggu</label>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end() ?>
            <div class="spacer"></div>
        </div>
    </div>
</div>