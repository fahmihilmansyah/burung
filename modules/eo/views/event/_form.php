<?php 
    use kartik\date\DatePicker; 
?>

<div class="row">
    <div class="col-xs-12">
        <?= $form->field($eventModel, 'nama_event')->textInput()->label('Nama Event') ?>
    </div>
</div>
<input id="listOfDays" type="hidden" value="tes">
<div class="row">
    <div class="col-xs-12">
    <?php echo $form->field($eventModel, 'tgl_event')
                ->widget(DatePicker::className(),[
                    'options' => [
                        'placeholder' => 'Pilih tanggal event'
                    ],
                    'removeButton' => false,
                    'type' => DatePicker::TYPE_COMPONENT_APPEND,
                    'pluginOptions' => [
                        'autoclose'=> true,
                        'format' => 'dd M yyyy',
                        'beforeShowDay' => new \yii\web\JsExpression("function (date) {
                            var day = date.getDay();
                            if (day == 1) {
                            return [false,'','Unavailable']; 
                            } else { 
                            return [true];
                            }
                        }"),
                    ]
                ]); 
    ?>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <?= $form->field($eventModel, 'nominal')->textInput()->label('Harga Tiket') ?>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <?= $form->field($eventModel, 'waktu')->textInput()->label('Waktu') ?>
    </div>
</div>
