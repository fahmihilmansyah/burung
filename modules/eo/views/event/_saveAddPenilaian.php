<?php use yii\helpers\Url; ?>
<div class="AppMainMenu">
    <button class="btn AppMainMenu__submitButton AppForm--btnSave" onclick="doPenilaian('<?= Url::toRoute(['default/save-add-penilaian','groupid' => $this->context->groupid]); ?>')">
        <i class="uil uil-save"></i> <br/>Save
    </button>
    <button class="btn AppMainMenu__submitButton AppForm--btnPreview" onclick="addPenilaian('<?= Url::toRoute(['default/get-options']); ?>')">
        <i class="uil uil-plus"></i> <br/>Penilaian
    </button>
</div>