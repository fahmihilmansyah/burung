<?php 
    use yii\helpers\Url; 
?>
<div class="container-fluid">
    <div class=AppGantungan>
        <div class="spacer"></div>
        <h5>Daftar Gantungan</h5>
        <div id="gantungan" class="gantungan" column="<?= $column; ?>">
            <?php 
                foreach ($gantungans as $key => $gantungan) {
                    echo '<div class="owl-carousel">';
                    foreach ($gantungan as $key => $record) {
                        $actionDaftar = Url::toRoute(['add-juri/spesific-add','id' => $id, 'groupid' => $groupid, 'gnt_id' => $record['id']]);
            ?>
                    <a href="<?= $actionDaftar; ?>" >
                        <div class="gantunganCol" gntId="<?= $record['id']; ?>" evId="<?= $record['event_id']; ?>">
                            <p><?= $record['no_gnt']; ?></p>
                            <div class="totalpoint">
                                <span>juri terdaftar</span>
                                <span id="point-<?= $record['id']; ?>" class="point"><?= $record['totalPoint']; ?></span>
                            </div>
                        </div>
                    </a>
            <?php
                    }
                    echo '</div>';
                }
            ?>
        </div>
    </div>
</div>