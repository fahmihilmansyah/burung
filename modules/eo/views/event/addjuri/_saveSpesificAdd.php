<?php use yii\helpers\Url; ?>
<div class="AppMainMenu">
    <button class="btn AppMainMenu__submitButton AppForm--btnSave" onclick="doAddGeneral('<?= Url::toRoute(['add-juri/save-spesific-add','groupid' => $this->context->groupid]); ?>')">
        <i class="uil uil-save"></i> <br/>Save
    </button>
    <button class="btn AppMainMenu__submitButton AppForm--btnPreview" onclick="addGeneral('<?= Url::toRoute(['add-juri/get-options']); ?>')">
        <i class="uil uil-plus"></i> <br/>Juri
    </button>
</div>