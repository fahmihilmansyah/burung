<?php 
    use yii\helpers\Url;
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;

    $this->title = 'GacorApp';
?>
<div class="container-fluid">   
    <div class="spacer"></div>
    <div class="AppForm">
        <?php 
            $form = ActiveForm::begin([
                'method' => 'POST',
                'id' => 'formAddPenilaian',
                'options' => ['class' => 'form-horizontal'],
            ]);
        ?>  
            <input type="hidden" value="<?= $groupid; ?>" name="groupid">
            <input type="hidden" value="<?= $id; ?>" name="id">
            <div id="target-penilaian">

            <?php 
                if(count($listsjuri))
                {
                    $i = 0;
                    foreach ($listsjuri as $key => $list) {
            ?>
                    <div class="AppFormGroup">
                        <label for="nama_toko">Juri<span class="delete" onclick="deleteAddGeneral(this)"><i class="uil uil-trash-alt"></i></span></label>
                        <?php echo Html::dropDownList('juri_id['.$i.']',
                                                        $list->juri_id, 
                                                        $juri, 
                                                        [
                                                            'prompt' => '--- Pilih ---',
                                                            'class' => 'form-control tipepenilaiandropdown',
                                                        ]); 
                                                    ?>
                        <div class="error-wrapper"></div>
                    </div>
            <?php 
                    $i++;
                    }
                }else{
            ?>
                    <div class="AppFormGroup">
                        <label for="nama_toko">Juri<span class="delete" onclick="deleteAddGeneral(this)"><i class="uil uil-trash-alt"></i></span></label>
                        <?php echo Html::dropDownList('juri_id[0]',
                                                        '', 
                                                        $juri, 
                                                        [
                                                            'prompt' => '--- Pilih ---',
                                                            'class' => 'form-control tipepenilaiandropdown',
                                                        ]); 
                                                    ?>
                        <div class="error-wrapper"></div>
                    </div>
            <?php 
                }
            ?>
            </div>
        <?php ActiveForm::end() ?>
    </div>
</div>