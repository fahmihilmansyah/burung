<?php 
    use yii\helpers\Url;
    use app\modules\eo\models\JuriAssignment;
    use app\modules\eo\models\EventPenilaian;
    
    $this->title = 'GacorApp';
?>

<div class="AppEvent">
    <?php if (Yii::$app->session->hasFlash('success')): ?>
        <div class="alert alert-success alert-dismissable">
            <h4 style="font-size:11px"><i class="uil uil-check"></i>Success!</h4>
            <?= Yii::$app->session->getFlash('success') ?>
        </div>
    <?php endif; ?>

    <?php if (Yii::$app->session->hasFlash('error')): ?>
        <div class="alert alert-danger alert-dismissable">
            <h4 style="font-size:11px"><i class="uil uil-check"></i>Error!</h4>
            <?= Yii::$app->session->getFlash('error') ?>
        </div>
    <?php endif; ?>

    <?php 
        if(!empty($juris)){
            foreach ($juris as $key => $juri) {
    ?>
            <div class="AppCard">
                <div class="body">
                    <div class="row">
                        <div class="col-xs-12">
                            <h5><?= $juri->nama_juri ?? ''; ?></h5>
                        </div>
                    </div>
                </div>
                <div class="footer">
                    <a href="<?= Url::toRoute(['add-juri-spesific/spesific-edit', 'id' => $id,'groupid' => $this->context->groupid,'juriid' => $juri->id]); ?>" class="btn btn-primary"><i class="uil uil-plus"></i> Edit</a>
                    <a href="javascript:void(0)" class="btn btn-danger" onclick="deleteData('<?= Url::toRoute(['default/delete','id' => $juri->id,'groupid' => $this->context->groupid]); ?>')"><i class="uil uil-trash"></i> Delete</a>
                </div>
            </div>
    <?php
            }
        }else{
    ?>
           <div class="AppCard">
                No Data
            </div>
    <?php 
        }
    ?>
</div>

<script>
</script>