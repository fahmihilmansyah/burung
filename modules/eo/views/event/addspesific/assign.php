<?php 
    use yii\helpers\Url; 
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;
?>

<?php 

    $action = Url::toRoute(['add-juri-spesific/save-spesific-add','groupid' => $this->context->groupid]); 
    if(isset($juriid)){
        $action = Url::toRoute(['add-juri-spesific/update-spesific-add','groupid' => $this->context->groupid]); 
    }
   
    $form = ActiveForm::begin([
        'action' => $action,
        'method' => 'POST',
        'id' => 'assignForm',
        // 'enableAjaxValidation' => true,
        'enableClientValidation'=>false,
        // 'validationUrl' => ['default/ajax-validation'],
        'options' => ['class' => 'form-horizontal'],
    ]);
?>
<input type="hidden" name="groupid" value="<?= $groupid ?? ''; ?>">
<input type="hidden" name="id" value="<?= $id ?? ''; ?>">
<div class="container-fluid">
    <div class=AppGantunganStd>
        <div class="AppFormGroup">
            <label for="nama_toko">Juri</label>
            <?php 
            if(isset($juriid)){
                echo '<input type="hidden" name="juri_id" value="'.$juriid.'">';
                echo '<p style="font-weight:bold; font-size:18px">'.$juri->nama_juri ?? ''.'</p>';
            }else{
                echo Html::dropDownList('juri_id',
                                            '', 
                                            $juriOptions, 
                                            [
                                                'prompt' => '--- Pilih ---',
                                                'class' => 'form-control tipepenilaiandropdown',
                                            ]); 
            }
            ?>
            <div class="error-wrapper"></div>
        </div>

        <div class="spacer"></div>
        <h5>Daftar Gantungan</h5>
        <div class="spacer"></div>
        <div>
            <button type="button" id="checkall" class="btn btn-primary">
                Check All
            </button>
            <button type="button" id="uncheckall" class="btn btn-danger">
                Uncheck All
            </button>
        </div>
        <div class="spacer"></div>
        <div id="gantungan" class="gantungan" column="<?= $column; ?>">
            <?php 
                $i = 0;
                foreach ($gantungans as $key => $gantungan) {
                    echo '<label><input class="parentCheck" parentId="'.$i.'" type="checkbox" value="<?= $i; ?>"/> Pilih Baris</label>'; 
                    echo '<div class="owl-carousel">';
                    foreach ($gantungan as $key => $record) {
                        $actionDaftar = Url::toRoute(['add-juri/spesific-add','id' => $id, 'groupid' => $groupid, 'gnt_id' => $record['id']]);
            ?>
                    <label>
                        <div class="gantunganCol" gntId="<?= $record['id']; ?>" evId="<?= $record['event_id']; ?>">
                            <p><?= $record['no_gnt']; ?></p>
                        </div>

                        <?php 
                            $array = isset($gantunganAssignment) ? $gantunganAssignment : [];
                            $checked = in_array($record['id'],$array)  ? 'checked' : '';
                        ?>
                        <input class="childCheck" 
                                references="<?= $i; ?>" 
                                type="checkbox" 
                                name="gantungan_id[]" 
                                style="display:block; position:relative; margin:0 auto; margin-top:2px;" 
                                value="<?= $record['id']; ?>"
                                <?= $checked; ?>
                                />
                    </label>
            <?php
                    }
                    echo '</div>';
                    $i++;
                }
            ?>
        </div>
        <div class="spacer"></div>
    </div>
</div>

<?php ActiveForm::end() ?>