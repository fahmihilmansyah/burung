<?php 
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;

    $this->title = 'GacorApp';
?>
<div class="container-fluid">
    <div class="AppForm">
        <?php 
            $form = ActiveForm::begin([
                'action' => ['default/update-event'],
                'method' => 'POST',
                'id' => 'eventForm',
                // 'enableAjaxValidation' => true,
                'enableClientValidation'=>true,
                // 'validationUrl' => ['default/ajax-validation'],
                'options' => ['class' => 'form-horizontal'],
            ]);
        ?>
        
        <?php 
            echo $form->field($eventModel, 'id')
                    ->hiddenInput(['value'=> $eventModel->id])->label(false);
        ?>

        <?= $this->render('@app/modules/eo/views/event/_form', [
                            'form' => $form, 
                            'eventModel' => $eventModel,
                            'pemilikTempat' => $pemilikTempat 
                        ]); ?>
        
        <?php ActiveForm::end() ?>
        
        <div class="preview">
            <div class="previewWrapper">
                <div id="previewTarget"></div>
                <div class="closeBtn" onclick="closePreview()"><i class="uil uil-times"></i></div>
            </div>
        </div>
    </div>
</div>
