<?php 
    use yii\helpers\Url;
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;

    $this->title = 'GacorApp';
?>
<div class="container-fluid">   
    <div class="spacer"></div>
    <div class="AppForm">
        <?php 
            $form = ActiveForm::begin([
                'method' => 'POST',
                'id' => 'formAddPenilaian',
                'options' => ['class' => 'form-horizontal'],
            ]);
        ?>  
            <input type="hidden" value="<?= $id; ?>" name="id">
            <div id="target-penilaian">

            <?php 
                if(count($penilaians))
                {
                    $i = 0;
                    foreach ($penilaians as $key => $penilaian) {
            ?>
                    <div class="AppFormGroup">
                        <label for="nama_toko">Tipe Penilaian<span class="delete" onclick="deletePenilaian(this)"><i class="uil uil-trash-alt"></i></span></label>
                        <?php echo Html::dropDownList('tipe_penilaian_id['.$i.']',
                                                        $penilaian->type_penilaian_id, 
                                                        $typePenilaian, 
                                                        [
                                                            'prompt' => '--- Pilih ---',
                                                            'class' => 'form-control tipepenilaiandropdown',
                                                        ]); 
                                                    ?>
                        <div class="error-wrapper"></div>
                    </div>
            <?php 
                    $i++;
                    }
                }else{
            ?>
                    <div class="AppFormGroup">
                        <label for="nama_toko">Tipe Penilaian<span class="delete" onclick="deletePenilaian(this)"><i class="uil uil-trash-alt"></i></span></label>
                        <?php echo Html::dropDownList('tipe_penilaian_id[0]',
                                                        '', 
                                                        $typePenilaian, 
                                                        [
                                                            'prompt' => '--- Pilih ---',
                                                            'class' => 'form-control tipepenilaiandropdown',
                                                        ]); 
                                                    ?>
                        <div class="error-wrapper"></div>
                    </div>
            <?php 
                }
            ?>
            </div>
        <?php ActiveForm::end() ?>
    </div>
</div>