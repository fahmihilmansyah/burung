<?php use yii\helpers\Url; ?>
<div class="AppMainMenu">
<?php 
    $action = ['default/save-event'];
    if(isset($this->context->typeform) && $this->context->typeform === 'edit')
        $action = ['default/update-event'];
?>
    <button class="btn AppMainMenu__submitButton AppForm--btnSave" 
            onclick="saveEvent('<?= Url::toRoute($action); ?>')">
        <i class="uil uil-save"></i> <br/>Save
    </button>
</div>