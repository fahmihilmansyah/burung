<?php 
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;
    use kartik\date\DatePicker;

    $this->title = 'GacorApp';
?>
<div class="container-fluid">
    <div class="AppForm">
        <?php 
            $form = ActiveForm::begin([
                'action' => ['default/save-event'],
                'method' => 'POST',
                'id' => 'eventForm',
                // 'enableAjaxValidation' => true,
                'enableClientValidation'=>false,
                // 'validationUrl' => ['default/ajax-validation'],
                'options' => ['class' => 'form-horizontal'],
            ]);
        ?>
            <input name="id" type="hidden" value="<?= $id ?? ''; ?>">
            <input name="groupid" type="hidden" value="<?= $groupid ?? ''; ?>">
            <div class="row">
                <div class="col-xs-12">
                    <div class="form-group">
                        <label for="email" class="label-input">Nama Class Event</label>
                        <input type="text" 
                                class="form-control" 
                                name="nama_event" 
                                value="<?= $event->nama_event ?? ''; ?>"
                            />
                        <div class="error-wrapper"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="form-group">
                        <label for="email" class="label-input">Tipe Gantungan</label>
                        <?php echo Html::dropDownList('type_gantungan_id',
                                        $event->type_gantungan_id ?? '', 
                                        $typeGantungan, 
                                        [
                                            'prompt' => 'Pilih Tipe Gantungan',
                                            'class' => 'form-control icondropdown',
                                        ]); 
                        ?>
                        <div class="error-wrapper"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-4">
                    <div class="form-group">
                        <label for="email" class="label-input">Harga Tiket</label>
                        <input type="text" 
                                class="form-control" 
                                name="harga_tiket" 
                                value="<?= $event->nominal ?? '0'; ?>"
                            />
                        <div class="error-wrapper"></div>
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="form-group" style="margin-left:5px">
                        <input id="jadwapelaksanaan" type="hidden" value="<?= $disabledDays['jadwal_pelaksanaan'] ?? ''; ?>">
                        <label for="email" class="label-input">Tanggal Pelaksanaan</label>
                        <?php 
                            $tgl_event = '';
                            if(isset($event->tgl_event))
                                $tgl_event = date('d M Y',strtotime($event->tgl_event));
                        ?>
                        <input 
                                id="tanggal_pelaksanaan"
                                type="text" 
                                class="form-control" 
                                name="tgl_pelaksanaan" 
                                value="<?= $tgl_event; ?>"
                                readonly
                            />
                        <div class="error-wrapper"></div>
                    </div>
                </div>
                <div class="col-xs-2">
                    <div id="btnCalendar" class="btn btn-primary btn-calendar"><i class="uil uil-calendar-alt"></i></div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="form-group">
                        <label for="email" class="label-input">Waktu Pelaksanaan</label>
                        <input type="text" 
                                class="form-control" 
                                name="waktu" 
                                value="<?= $event->waktu ?? ''; ?>"
                            />
                        <div class="error-wrapper"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="form-group">
                        <label for="email" class="label-input">Jenis Burung</label>
                        <?php echo Html::dropDownList('jenis_burung_id',
                                        $event->jenis_burung ?? '', 
                                        $jenisBurung, 
                                        [
                                            'prompt' => 'Pilih Jenis Burung',
                                            'class' => 'form-control icondropdown',
                                        ]); 
                        ?>
                        <div class="error-wrapper"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="form-group">
                        <label for="email" class="label-input">Jenis Penilaian</label>
                        <label>
                            <input type="checkbox" 
                                name="penilaian_khusus" 
                                value="1"
                                <?= isset($event->is_khusus) ? (($event->is_khusus === 1) ? 'checked' : '') : ''; ?>
                            /> Penilaian Khusus
                        </label>
                        <div class="error-wrapper"></div>
                    </div>
                </div>
            </div>
        <?php ActiveForm::end() ?>
        
        <div class="preview">
            <div class="previewWrapper">
                <div id="previewTarget"></div>
                <div class="closeBtn" onclick="closePreview()"><i class="uil uil-times"></i></div>
            </div>
        </div>
    </div>
</div>
