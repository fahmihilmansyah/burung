<?php 
use yii\helpers\Url;
$this->title = 'Gacor Application - Registrasi Berhasil';
?>
<div class="AppSuccess">
    <div class="AppSuccess__box">
    
        <?php $img = Url::to('@web/images/confirmed.svg');     ?>
        <img src="<?= $img; ?>" alt="notfound"/>
    </div>
    <p>Selamat, akunmu sudah dapat digunakan. <br/> login ke aplikasimu sekarang.</p>

    <div style="text-align:center; margin-top:10px;">
        <a href="<?= Url::toRoute(['eo-auth/login']); ?>" class="btn btn-primary Apputton Apputton--primary" style="font-size:18px;"><i class="uil uil-sign-in-alt"></i> Login</a>
    </div>
</div>