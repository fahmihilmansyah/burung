<?php 
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;

    $this->title = 'Gacor Application - Registrasi';
?>

<div class="AppRegister">
    <h5>Lengkapi Data Kamu</h5>
    
    <h4>Sudah punya akun? <a href="#" class="red">Login disini</a></h4>

    <div class="AppRegister__form">
        <?php 
            $form = ActiveForm::begin([
                'method' => 'POST',
                'id' => 'formRegister',
                'options' => ['class' => 'form-horizontal'],
            ]);
        ?>
        <div>
            <label for="namalengkap" class="label-input">Nama Lengkap</label>
            <input type="text" class="form-control AppInputText" name="nama_lengkap">
            <div class="error-wrapper"></div>
        </div>
        <div>
            <label for="username" class="label-input">Email</label>
            <input type="text" class="form-control AppInputText" name="username">
            <div class="error-wrapper"></div>
        </div>
        <div>
            <label for="password" class="label-input">Password</label>
            <input type="password" class="form-control AppInputText" name="password">
            <div class="error-wrapper"></div>
        </div>
        <div>
            <label for="notelpon" class="label-input">No. Handphone</label>
            <input type="text" class="form-control MasjedInputText" name="notelp">
            <div class="error-wrapper"></div>
        </div>
        <?php ActiveForm::end() ?>
    </div>
    <h4>
        Dengan klik Daftar, Anda telah menyetujui <a href="#" class="blue">Aturan Penggunaan</a> dan 
        <a href="#" class="blue">Kebijakan Privasi</a> dari kami.
    </h4>
</div>