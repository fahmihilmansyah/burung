<?php 
use yii\helpers\Url;
?>
<div class="AppOtp">
    <h5>Masukan Kode Otentikasi</h5>
    <h4>Kami telah mengirimkan kode otentikasi ke alamat email<br/>example@gmail.com</h4>
    <div class="AppOtp__wrapper">
        <div id="otp" class="AppOtp__box">
            <input type="text" maxlength="1" id="first">
            <input type="text" maxlength="1" id="second">
            <input type="text" maxlength="1" id="third">
            <input type="text" maxlength="1" id="fourth">
            <input type="text" maxlength="1" id="fifth">
            <input type="text" maxlength="1" id="sixth">
        </div>
    </div>
    <div id="targetMessage" class="targetMessage margin-top:20px;"></div>
    <div style="text-align:center;">
        <button onclick="submitOtp('<?= Url::toRoute(['eo-auth/otp-confirmed']); ?>','<?= $otpid; ?>')" type="button" class="btn AppButton AppButton--primary" style="font-size:17px;" id="btnKonfirmasi">
            <i class="uil uil-sync"></i> Konfirmasi
        </button>
    </div>
</div>