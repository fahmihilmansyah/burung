<?php

namespace app\modules\eo\controllers;

use Yii;
use app\controllers\BaseController as Controller;
use yii\web\Response;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use thamtech\uuid\helpers\UuidHelper;
use app\modules\eo\models\JenisBurung;
use app\modules\eo\models\EventGroup;
use app\modules\eo\models\EventGroupJenis;
use app\modules\eo\models\form\EventGroupForm;
use app\modules\eo\models\form\EventGroupAddJenisForm;
use yii\filters\AccessControl;

/**
 * Default controller for the `eo` module
 */
class EventGroupController extends Controller
{
    public $layout = '@app/views/layouts/app.php';

    public $header;
    public $headerText;
    public $menu = '@app/modules/eo/views/default/menu/_mainmenu';

    public $backLink = 'event-group/index';
  
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'user' => 'usereo',
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }
    
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
   
    /**
     * List Section
     * ::Start
     */
    public function actionList()
    {
        $this->header = '@app/modules/eo/views/default/menu/_backheader';
        $this->headerText = "Daftar Event";
        $this->menu = '@app/modules/eo/views/event-group/_mainmenu';

        $this->backLink = 'default/index';
        
        $model['eventGroups'] = EventGroup::find()->orderBy(['id' => SORT_DESC])->all();

        return $this->render('@app/modules/eo/views/event-group/index', $model);
    }
    /**
     * List Section
     * ::End
     */

    /**
     * Create Section
     * ::Start
     */
    public function actionCreate()
    {
        $this->header = '@app/modules/eo/views/default/menu/_backheader';
        $this->headerText = "Buat Event";
        $this->menu = '@app/modules/eo/views/event-group/_savemenu';

        $this->backLink = 'event-group/list';

        $model['eventGroupModel'] = new EventGroupForm();

        return $this->render('@app/modules/eo/views/event-group/create', $model);
    }

    public function actionSaveEventGroup(){
        $request = Yii::$app->request->post()['EventGroupForm'];

        $transaction = Yii::$app->db->beginTransaction();
        $userId= Yii::$app->usereo->getId() ?? null;

        try {
            $model = new EventGroup();
            $model->name = $request['name'];
            $model->desc = $request['desc'];
            $model->user_id = $userId;
            $model->created_ad = date('Y-m-d H:i:s');
            $model->save();

            Yii::$app->getSession()->setFlash('success', 'Your EventGroup was created successfully!');

            $transaction->commit();
        } catch (\Throwable $th) {
            Yii::$app->getSession()->setFlash('error', $th->getMessage());

            $transaction->rollback();
        }

        return $this->redirect(['event-group/list']);
    }
    /**
     * Create Section
     * ::End
     */

    /**
     * Edit Section
     * ::Start
     */
    public function actionEdit()
    {
        $this->header = '@app/modules/eo/views/default/menu/_backheader';
        $this->headerText = "Update Event";
        $this->menu = '@app/modules/eo/views/event-group/_savemenu';

        $this->backLink = 'event-group/list';

        $id = Yii::$app->request->get()['id'];  
        $EventGroup = EventGroup::findOne($id);

        $model = new EventGroupForm();
        $model->id = $EventGroup->id;
        $model->desc = $EventGroup->desc;
        $model->name = $EventGroup->name;
            
        $data['eventGroupModel'] =$model;

        return $this->render('@app/modules/eo/views/event-group/edit', $data);
    }

    public function actionUpdate(){
        $request = Yii::$app->request->post()['EventGroupForm'];
        $transaction = Yii::$app->db->beginTransaction();

        try {
            $model = EventGroup::findOne($request['id']);
            $model->name = $request['name'];
            $model->desc = $request['desc'];
            $model->updated_ad = date('Y-m-d H:i:s');
            $model->save();

            Yii::$app->getSession()->setFlash('success', 'Your EventGroup was edited successfully!');

            $transaction->commit();
        } catch (\Throwable $th) {
            Yii::$app->getSession()->setFlash('error', 'Error while editing your EventGroup.'.$th->getMessage());

            $transaction->rollback();
        }

        return $this->redirect(['event-group/list']);
    }
    /**
     * Edit Section
     * ::End
     */

     
    /**
     * Add Penilaian Section
     * ::Start
     */

    public function actionAddJenis()
    {
        $this->header = '@app/modules/eo/views/default/menu/_backheader';
        $this->headerText = "Tambah Jenis Burung";
        $this->menu = '@app/modules/eo/views/event-group/_saveAddJenis';

        $this->backLink = 'event-group/list';
        
        $id = Yii::$app->request->get()['id'];

        $model['EventGroupModel'] = new EventGroupAddJenisForm();
        $model['jenisBurung'] = JenisBurung::find()
                                    ->select(['name'])
                                    ->indexBy('id')
                                    ->column();
        $model['burungs'] = EventGroupJenis::find(['event_group_id' => $id])->all();
        $model['id'] = $id; 

        return $this->render('@app/modules/eo/views/event-group/addJenis', $model);
    }

    public function actionGetOptions(){
        $data = JenisBurung::find()->all();

        return $this->asJsonSuccess($data,'[Success] Save data...');
    }

    public function actionSaveAddPenilaian(){
        $requests = Yii::$app->request->post();
        
        $transaction = Yii::$app->db->beginTransaction();

        try {
            $deleteEventGroup = EventGroupJenis::deleteAll(['event_group_id' => $requests['id']]);

            foreach ($requests['jenis_burung_id'] as $key => $request) {
                $typePenilaian = JenisBurung::findOne(['id' => $request]);

                $EventGroup = new EventGroupJenis();
                $EventGroup->event_group_id = $requests['id'];
                $EventGroup->jenis_burung_id = $request;
                $EventGroup->created_ad = date('Y-m-d H:i:s');
                $EventGroup->save();
            }

            $transaction->commit();

            $callback['callback'] = Url::home(true).'eo/event-group/list';
        } catch (\Throwable $th) {
            $transaction->rollback();
            return $this->asJsonError([$th->getMessage()],'[Error] Save data...');
        }

        return $this->asJsonSuccess($callback,'[Success] Save data...');
    }

    /**
     * Add Penilaian Section
     * ::End
     */

    /**
     * Delete Section
     * ::Start
     */
    public function actionDelete(){
        $id = Yii::$app->request->get()['id'];
        
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $model = EventGroup::findOne($id)->delete();

            $transaction->commit();

            Yii::$app->getSession()->setFlash('success', 'Your EventGroup was deleted successfully!');
        } catch (\Throwable $th) {
            //throw $th;
            $transaction->rollback();
            
            Yii::$app->getSession()->setFlash('error', 'Error while deleting your EventGroup.'.$th->getMessage());
        }

        return $this->redirect(['event-group/list']);
    }
    /**
     * Delete Section
     * ::End
     */

    private function snakeLoop($snakeLength, $chunk, $type = 'ASC'){
        function createArrayLoop($number){
            $temp = [];
            for ($i=1; $i <= $number ; $i++) { 
                $temp[$i] = $i;
            }

            return $temp;
        }

        $array = createArrayLoop($snakeLength);
        $newArrays = array_chunk($array, $chunk);
        
        if($type == 'DESC'){
            foreach ($newArrays as $key => $newArray) {
                $newArrays[$key] = array_reverse($newArray, true);
            }
        } else if($type === 'ASCSLINE'){
            foreach ($newArrays as $key => $newArray) {
                if(($key + 1) % 2 == 0){
                    $newArrays[$key] = array_reverse($newArray, true);
                }
            }
        } else if($type === 'DESCSLINE'){
            foreach ($newArrays as $key => $newArray) {
                $newArrays[$key] = array_reverse($newArray, true);
            }
            
            foreach ($newArrays as $key => $newArray) {
                if(($key + 1) % 2 == 0){
                    $newArrays[$key] = array_reverse($newArray, true);
                }
            }
        }

        return $newArrays;
    }
}
