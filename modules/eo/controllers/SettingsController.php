<?php

namespace app\modules\eo\controllers;

use Yii;
use app\controllers\BaseController as Controller;
use yii\web\Response;
use yii\helpers\Url;
use thamtech\uuid\helpers\UuidHelper;
use yii\filters\AccessControl;
use app\modules\eo\models\form\SettingsForm;
use app\modules\eo\models\PemilikTempat;

/**
 * Default controller for the `eo` module
 */
class SettingsController extends Controller
{
    public $layout = '@app/views/layouts/app.php';

    public $header;
    public $headerText;
    public $menu = '@app/modules/eo/views/default/menu/_mainmenu';

    public $backLink = 'default/event';

    public function actionIndex(){
        return $this->render('@app/modules/eo/views/settings/index');
    }

    public function actionHariTempat(){
        $this->header = '@app/modules/eo/views/default/menu/_backheader';
        $this->headerText = "Settings - Hari & Tempat Pelaksanaan";
        $this->menu = '@app/modules/eo/views/settings/haritempat/_savemenu';
        
        $this->backLink = 'settings/index';
        
        $userid = Yii::$app->usereo->identity->getId() ?? ''; 

        $pemilik = PemilikTempat::find()->where(['user_id' => $userid]);
        $owner = $pemilik->one();

        $model['owner'] = $owner;

        $jadwal = [];
        if(!empty($owner))
            $jadwal = explode(",",$owner->jadwal_pelaksanaan);
        
        $model['minggu'] = in_array("0", $jadwal) ? 'checked' : '';
        $model['senin'] = in_array("1", $jadwal) ? 'checked' : '';
        $model['selasa'] = in_array("2", $jadwal) ? 'checked' : '';
        $model['rabu'] = in_array("3", $jadwal) ? 'checked' : '';
        $model['kamis'] = in_array("4", $jadwal) ? 'checked' : '';
        $model['jumat'] = in_array("5", $jadwal) ? 'checked' : '';
        $model['sabtu'] = in_array("6", $jadwal) ? 'checked' : '';

        if($pemilik->count() < 1){
            $new = new PemilikTempat();
            $new->created_ad = date('Y-m-d H:i:s');
            $new->user_id = $userid;
            $new->save();
        }

        return $this->render('@app/modules/eo/views/settings/haritempat/index',$model);
    }

    public function actionHariTempatUpdate(){
        $request = Yii::$app->request->post();
        $userid = Yii::$app->usereo->identity->getId() ?? ''; 

        $jadwal_pelaksanaan = !empty($request['jadwal_pelaksanaan']) ? implode(",",$request['jadwal_pelaksanaan']) : null;
       
        $transaction = Yii::$app->db->beginTransaction();
        Yii::$app->timeZone = 'Asia/Jakarta';

        try {
            $pemilik = PemilikTempat::findOne(['user_id' => $userid]);
            $pemilik->nama_pemilik = $request['nama_pemilik'];
            $pemilik->notlp = $request['notlp'];
            $pemilik->alamat = $request['alamat'];
            $pemilik->jadwal_pelaksanaan = $jadwal_pelaksanaan;
            $pemilik->user_id = $userid;
            $pemilik->updated_ad = date('Y-m-d H:i:s');

            if(!$pemilik->save()){
                throw Exception('Unable to save record.');
            }

            $callback['callback'] = Url::home(true).'eo/settings';
            $transaction->commit();
        } catch (\Throwable $th) {
            $transaction->rollback();
            return $this->asJsonError([$th->getMessage()],'[Error] Save data...');
        }

        return $this->asJsonSuccess($callback,'[Success] Save data...');
    }
}