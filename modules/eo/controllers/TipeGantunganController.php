<?php

namespace app\modules\eo\controllers;

use Yii;
use yii\web\Controller;
use yii\web\Response;
use yii\widgets\ActiveForm;
use app\modules\eo\models\TypeGantungan;
use app\modules\eo\models\form\TipeGantunganForm;

/**
 * Default controller for the `eo` module
 */
class TipeGantunganController extends Controller
{
    public $layout = '@app/views/layouts/app.php';

    public $header;
    public $headerText;
    public $menu = '@app/modules/eo/views/default/menu/_mainmenu';

    public $backLink = 'settings/tipe-gantungan';

    public $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
   
    /**
     * List Section
     * ::Start
     */
    public function actionGantungan()
    {
        $this->header = '@app/modules/eo/views/default/menu/_backheader';
        $this->headerText = "Settings: List Tipe Gantungan";
        $this->menu = '@app/modules/eo/views/settings/tipe_gantungan/_mainmenu';

        $this->backLink = 'settings/index';

        $model['typeGantungans'] = TypeGantungan::find()->orderBy(['id' => SORT_DESC])->all();

        return $this->render('@app/modules/eo/views/settings/tipe_gantungan/index', $model);
    }
    /**
     * List Section
     * ::End
     */

    /**
     * Create Section
     * ::Start
     */
    public function actionCreate()
    {
        $this->header = '@app/modules/eo/views/default/menu/_backheader';
        $this->headerText = "Settings - Create Tipe Gantungan";
        $this->menu = '@app/modules/eo/views/settings/tipe_gantungan/_savemenu';

        $model['tipeGantunganModel'] = new TipeGantunganForm();

        return $this->render('@app/modules/eo/views/settings/tipe_gantungan/create', $model);
    }

    public function actionSave(){
        $request = Yii::$app->request->post()['TipeGantunganForm'];

        $transaction = Yii::$app->db->beginTransaction();

        try {
            $model = new TypeGantungan();
            $model->name = $request['name'];
            $model->jumlah_row = $request['jumlah_row'];
            $model->jumlah_col = $request['jumlah_col'];
            $model->type_gnt = $request['type_gnt'];
            $model->created_ad = date('Y-m-d H:i:s');
            $model->save();
            
            Yii::$app->getSession()->setFlash('success', 'Your tipe gantungan was created successfully!');

            $transaction->commit();
        } catch (\Throwable $th) {
            Yii::$app->getSession()->setFlash('error', $th->getMessage().'Error while submitting your tipe gantungan.');

            $transaction->rollback();
        }

        return $this->redirect(['tipe-gantungan/gantungan']);
    }
    /**
     * Create Section
     * ::End
     */

    /**
     * Edit Section
     * ::Start
     */
    public function actionEdit()
    {
        $this->header = '@app/modules/eo/views/default/menu/_backheader';
        $this->headerText = "Event - Edit Tipe Gantungan";
        $this->menu = '@app/modules/eo/views/settings/tipe_gantungan/_savemenu';

        $id = Yii::$app->request->get()['id'];
        $tipeGantungan = TypeGantungan::findOne($id);

        $model = new TipeGantunganForm();
        $model->id = $tipeGantungan->id;
        $model->name = $tipeGantungan->name;
        $model->jumlah_row =  $tipeGantungan->jumlah_row;
        $model->jumlah_col = $tipeGantungan->jumlah_col;
        $model->type_gnt = $tipeGantungan->type_gnt;
            
        $data['tipeGantunganModel'] =$model;

        return $this->render('@app/modules/eo/views/settings/tipe_gantungan/edit', $data);
    }

    public function actionUpdate(){
        $request = Yii::$app->request->post()['TipeGantunganForm'];
        $transaction = Yii::$app->db->beginTransaction();
        $userid = Yii::$app->usereo->identity->getId() ?? ''; 

        try {
            $model = TypeGantungan::findOne($request['id']);
            $model->name = $request['name'];
            $model->jumlah_row =  $request['jumlah_row'];
            $model->jumlah_col = $request['jumlah_col'];
            $model->type_gnt = $request['type_gnt'];
            $model->updated_ad = date('Y-m-d H:i:s');
            $model->save();
            
            Yii::$app->getSession()->setFlash('success', 'Your tipe gantungan was edited successfully!');

            $transaction->commit();
        } catch (\Throwable $th) {
            Yii::$app->getSession()->setFlash('error', 'Error while editing your tipe gantungan.'.$th->getMessage());

            $transaction->rollback();
        }

        return $this->redirect(['tipe-gantungan/gantungan']);
    }
    /**
     * Edit Section
     * ::End
     */

    /**
    * Delete Section
    * ::Start
    */
    public function actionDelete(){
        $id = Yii::$app->request->get()['id'];
        
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $model = TypeGantungan::findOne($id)->delete();

            $transaction->commit();
            
            Yii::$app->getSession()->setFlash('success', 'Your tipe gantungan was deleted successfully!');
        } catch (\Throwable $th) {
            //throw $th;
            $transaction->rollback();
            Yii::$app->getSession()->setFlash('error', 'Error while deleting your tipe gantungan.');
        }

        return $this->redirect(['juri/juri']);
    }
    /**
     * Delete Section
     * ::End
     */
 
    function generate_string($input, $strength = 16) {
        $input_length = strlen($input);
        $random_string = '';
        for($i = 0; $i < $strength; $i++) {
            $random_character = $input[mt_rand(0, $input_length - 1)];
            $random_string .= $random_character;
        }
    
        return $random_string;
    }
}
