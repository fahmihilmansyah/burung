<?php

namespace app\modules\eo\controllers;

use Yii;
use app\controllers\BaseController as Controller;
use yii\web\Response;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use thamtech\uuid\helpers\UuidHelper;
use app\modules\eo\models\Event;
use app\modules\eo\models\EventPenilaian;
use app\modules\eo\models\TypePenilaian;
use app\modules\eo\models\TypeGantungan;
use app\modules\eo\models\PemilikTempat;
use app\modules\eo\models\EventGroupJenis;
use app\modules\eo\models\Gantungan;
use app\modules\eo\models\Penilaian;
use app\modules\eo\models\Juri;
use app\modules\eo\models\JuriAssignment;
use app\modules\eo\models\form\EventForm;
use app\modules\eo\models\form\EventAddPenilaianForm;
use yii\filters\AccessControl;

/**
 * Default controller for the `eo` module
 */
class AddJuriController extends Controller
{
    public $layout = '@app/views/layouts/app.php';

    public $header;
    public $headerText;
    public $menu = '@app/modules/eo/views/default/menu/_mainmenu';

    public $backLink = 'default/event';
    public $groupid;

    public function actionGeneral(){
        $groupid = Yii::$app->request->get()['groupid'];
        $this->header = '@app/modules/eo/views/default/menu/_backheader';
        $this->headerText = "Tambah Juri";
        $this->menu = '@app/modules/eo/views/event/addjuri/_saveGeneral';
        $this->groupid = $groupid;
        $this->backLink = ['default/event','groupid' => $groupid];

        $id = Yii::$app->request->get()['id'];

        $model['juri'] = Juri::find()
                                    ->select(['nama_juri'])
                                    ->indexBy('id')
                                    ->column();
        $model['listsjuri'] = JuriAssignment::find()
                                            ->select('juri_id')
                                            ->distinct()
                                            ->where(['event_id' => $id])
                                            ->all();
        $model['id'] = $id; 
        $model['groupid'] = $groupid; 

        return $this->render('@app/modules/eo/views/event/addjuri/general', $model);
    }

    public function actionSaveGeneral(){
        $requests = Yii::$app->request->post();
        
        $transaction = Yii::$app->db->beginTransaction();

        try {
            $deleteEvent = JuriAssignment::deleteAll(['event_id' => $requests['id']]);

            foreach ($requests['juri_id'] as $key => $request) {
                $juri = Juri::findOne(['id' => $request]);

                $gantungans = Gantungan::find()->where(['event_id' => $requests['id']])->all();

                foreach ($gantungans as $key => $gantungan) {
                    $event = new JuriAssignment();
                    $event->event_id = $requests['id'];
                    $event->gnt_id = $gantungan->id;
                    $event->juri_id = $juri->id;
                    $event->save();
                }
            }

            $transaction->commit();

            $callback['callback'] = Url::home(true).'eo/event?groupid='.Yii::$app->request->get()['groupid'];
        } catch (\Throwable $th) {
            $transaction->rollback();
            return $this->asJsonError([$th->getMessage()],'[Error] Save data...');
        }

        return $this->asJsonSuccess($callback,'[Success] Save data...');
    }

    public function actionSpesific(){
        $groupid = Yii::$app->request->get()['groupid'];
        $id = Yii::$app->request->get()['id'];
        $this->layout = '@app/views/layouts/top.php';
        $this->header = '@app/modules/eo/views/default/menu/_backheader';
        $this->headerText = "Tambah Juri";
        $this->menu = '@app/modules/eo/views/event/addjuri/_saveSpesific';
        $this->groupid = $groupid;
        $this->backLink = ['default/event','groupid' => $groupid];

        $model['id'] = $id; 
        $model['groupid'] = $groupid; 

        $event = Event::find()->where(['id' => $id])->one();
        
        $tipeGantungan = TypeGantungan::find()->where(['id' => $event->type_gantungan_id])->one();
            
        $col = $tipeGantungan->jumlah_col ?? 0;
        $row = $tipeGantungan->jumlah_row ?? 0;
        $type_gnt = $tipeGantungan->type_gnt ?? 0;
        
        $length = $row * $col;
        $chunk = $col;
        $loops = $this->snakeLoop($length, $chunk, $type_gnt);
        
        $gantungans = [];

        if(!empty($loops)){
            foreach ($loops as $k1 => $loop) {
                foreach ($loop as $k2  => $value) {
                    $gantunganData = Gantungan::find()
                                            ->where([
                                                    'event_id' => $event->id,
                                                    'no_gnt' => $value,
                                            ])
                                            ->one();

                    if(!empty($gantunganData)){
                        $gantungans[$k1][$k2]['id'] = $gantunganData->id;
                        $gantungans[$k1][$k2]['no_gnt'] = $gantunganData->no_gnt;
                        $gantungans[$k1][$k2]['event_id'] = $gantunganData->event_id;

                        $countJuri = JuriAssignment::find()
                                                ->select('juri_id')
                                                ->distinct()
                                                ->where(['event_id' => $gantunganData->event_id, 'gnt_id' => $gantunganData->id])
                                                ->count();

                        $gantungans[$k1][$k2]['totalPoint'] = $countJuri ?? 0;
                    }
                    else{
                        $gantungans[$k1][$k2]['id'] = $value;
                        $gantungans[$k1][$k2]['no_gnt'] = $value;
                        $gantungans[$k1][$k2]['event_id'] = $event->id;
                        $gantungans[$k1][$k2]['totalPoint'] = 0;
                    }
                }
            }
        }
        
        $model['gantungans'] = $gantungans;
        $model['column'] = $tipeGantungan->jumlah_col;

        return $this->render('@app/modules/eo/views/event/addjuri/spesific', $model);
    }

    public function actionSpesificAdd(){
        $groupid = Yii::$app->request->get()['groupid'];
        $id = Yii::$app->request->get()['id'];
        $gnt_id = Yii::$app->request->get()['gnt_id'];

        $this->header = '@app/modules/eo/views/default/menu/_backheader';
        $this->headerText = "Tambah Juri";
        $this->menu = '@app/modules/eo/views/event/addjuri/_saveSpesificAdd';
        $this->groupid = $groupid;
        $this->backLink = ['add-juri/spesific','id' => $id, 'groupid' => $groupid];

        $model['juri'] = Juri::find()
                                    ->select(['nama_juri'])
                                    ->indexBy('id')
                                    ->column();
        $model['listsjuri'] = JuriAssignment::find()
                                            ->select('juri_id')
                                            ->distinct()
                                            ->where(['event_id' => $id, 'gnt_id' => $gnt_id])
                                            ->all();
        $model['id'] = $id; 
        $model['groupid'] = $groupid; 
        $model['gnt_id'] = $gnt_id; 

        return $this->render('@app/modules/eo/views/event/addjuri/spesificAdd', $model);
    }

    public function actionSaveSpesificAdd(){
        $requests = Yii::$app->request->post();
        $transaction = Yii::$app->db->beginTransaction();

        try {
            $deleteEvent = JuriAssignment::deleteAll([
                                                'event_id' => $requests['id'],
                                                'gnt_id' => $requests['gnt_id']
                                            ]);

            foreach ($requests['juri_id'] as $key => $request) {
                $juri = Juri::findOne(['id' => $request]);

                $event = new JuriAssignment();
                $event->event_id = $requests['id'];
                $event->gnt_id = $requests['gnt_id'];
                $event->juri_id = $juri->id;
                $event->save();
            }

            $transaction->commit();

            $callback['callback'] = Url::home(true).'eo/add-juri/spesific?id='.$requests['id'].'&groupid='.$requests['groupid'].'&gnt_id='.$requests['gnt_id'];
        } catch (\Throwable $th) {
            $transaction->rollback();
            return $this->asJsonError([$th->getMessage()],'[Error] Save data...');
        }

        return $this->asJsonSuccess($callback,'[Success] Save data...');
    }

    public function actionGetOptions(){
        $data = Juri::find()->all();

        return $this->asJsonSuccess($data,'[Success] Save data...');
    }

    private function snakeLoop($snakeLength, $chunk, $type = 'ASC'){
        function createArrayLoop($number){
            $temp = [];
            for ($i=1; $i <= $number ; $i++) { 
                $temp[$i] = $i;
            }

            return $temp;
        }

        $array = createArrayLoop($snakeLength);
        $newArrays = array_chunk($array, $chunk);
        
        if($type == 'DESC'){
            foreach ($newArrays as $key => $newArray) {
                $newArrays[$key] = array_reverse($newArray, true);
            }
        } else if($type === 'ASCSLINE'){
            foreach ($newArrays as $key => $newArray) {
                if(($key + 1) % 2 == 0){
                    $newArrays[$key] = array_reverse($newArray, true);
                }
            }
        } else if($type === 'DESCSLINE'){
            foreach ($newArrays as $key => $newArray) {
                $newArrays[$key] = array_reverse($newArray, true);
            }
            
            foreach ($newArrays as $key => $newArray) {
                if(($key + 1) % 2 == 0){
                    $newArrays[$key] = array_reverse($newArray, true);
                }
            }
        }

        return $newArrays;
    }
}