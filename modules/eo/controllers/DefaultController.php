<?php

namespace app\modules\eo\controllers;

use Yii;
use app\controllers\BaseController as Controller;
use yii\web\Response;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use thamtech\uuid\helpers\UuidHelper;
use app\modules\eo\models\Event;
use app\modules\eo\models\EventPenilaian;
use app\modules\eo\models\TypePenilaian;
use app\modules\eo\models\TypeGantungan;
use app\modules\eo\models\PemilikTempat;
use app\modules\eo\models\EventGroupJenis;
use app\modules\eo\models\Gantungan;
use app\modules\eo\models\form\EventForm;
use app\modules\eo\models\form\EventAddPenilaianForm;
use yii\filters\AccessControl;

/**
 * Default controller for the `eo` module
 */
class DefaultController extends Controller
{
    public $layout = '@app/views/layouts/app.php';

    public $header;
    public $headerText;
    public $menu = '@app/modules/eo/views/default/menu/_mainmenu';

    public $backLink = 'default/event';
    public $groupid;
    public $typeform = 'add';
  
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'user' => 'usereo',
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }
    
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
   
    /**
     * List Section
     * ::Start
     */
    public function actionEvent()
    {
        $groupid = Yii::$app->request->get()['groupid'];
        $this->header = '@app/modules/eo/views/default/menu/_backheader';
        $this->headerText = "Daftar Class";
        $this->menu = '@app/modules/eo/views/event/_mainmenu';
        $this->backLink = 'event-group/list';
        $this->groupid = $groupid;
        
        $model['events'] = Event::find()
                                ->where(['event_group_id' => $groupid])
                                ->orderBy(['id' => SORT_DESC])->all();

        return $this->render('@app/modules/eo/views/event/index', $model);
    }
    /**
     * List Section
     * ::End
     */

    /**
     * Create Section
     * ::Start
     */
    public function actionCreate()
    {
        $groupid = Yii::$app->request->get()['groupid'];

        $this->header = '@app/modules/eo/views/default/menu/_backheader';
        $this->headerText = "Buat Class";
        $this->menu = '@app/modules/eo/views/event/_savemenu';
        $this->backLink = ['default/event','groupid' => $groupid];

        $model['groupid'] = $groupid;
        $model['eventModel'] = new EventForm();
        $model['typeGantungan'] = TypeGantungan::find()
                                    ->select(['name'])
                                    ->indexBy('id')
                                    ->column();

        $model['jenisBurung'] = EventGroupJenis::find()
                                    ->joinWith(['jenisBurung'])
                                    ->select(['jenis_burung.name'])
                                    ->where(['event_group_jenis_burung.event_group_id' => $groupid])
                                    ->indexBy('event_group_jenis_burung.jenis_burung_id')
                                    ->column();

        $model['disabledDays'] = Yii::$app->usereo->identity->settings ?? '';

        return $this->render('@app/modules/eo/views/event/create', $model);
    }

    public function actionSaveEvent(){
        $request = Yii::$app->request->post();

        $transaction = Yii::$app->db->beginTransaction();
        $userId= Yii::$app->usereo->getId() ?? null;

        try {
            $model = new Event();
            $model->nama_event = $request['nama_event'];
            $model->tgl_event = date('Y-m-d H:i:s',strtotime($request['tgl_pelaksanaan']));
            $model->nominal = $request['harga_tiket'];
            $model->waktu = $request['waktu'];
            $model->jenis_burung = $request['jenis_burung_id'];
            $model->type_gantungan_id = $request['type_gantungan_id'];
            $model->user_id = $userId;
            $model->event_group_id = $request['groupid'];
            $model->is_khusus = isset($request['penilaian_khusus']) ? $request['penilaian_khusus'] : '0';
            $model->created_ad = date('Y-m-d H:i:s');
            $model->save();

            $tipeGantungan = TypeGantungan::find()->where(['id' => $request['type_gantungan_id']])->one();
            
            $col = $tipeGantungan->jumlah_col ?? 0;
            $row = $tipeGantungan->jumlah_row ?? 0;
            $type_gnt = $tipeGantungan->type_gnt ?? 0;
            
            $length = $row * $col;
            $chunk = $col;
            $loops = $this->snakeLoop($length, $chunk, $type_gnt);

            $i = 1;
            foreach ($loops as $key => $loop) {
                foreach ($loop as $key => $value) {
                    $id = UuidHelper::uuid();
                    $gantungan = new Gantungan();
                    $gantungan->id = $id;
                    $gantungan->no_gnt = $value;
                    $gantungan->no_urut = $i;
                    $gantungan->event_id = $model->id;
                    $gantungan->created_ad = date('Y-m-d H:i:s');
                    $gantungan->updated_ad = date('Y-m-d H:i:s');
                    $gantungan->save();
                    $i++;
                }
            }

            Yii::$app->getSession()->setFlash('success', 'Your event was created successfully!');

            $callback = ['callback' => Url::toRoute(['default/event', 'groupid' => $request['groupid']])];
            
            $transaction->commit();
        } catch (\Throwable $th) {
            Yii::$app->getSession()->setFlash('error', $th->getMessage());

            $transaction->rollback();
            return $this->asJsonError([$th->getMessage()],'Gagal menghapus data.');
        }

        return $this->asJsonSuccess($callback,'Berhasil menghapus data.');
    }
    /**
     * Create Section
     * ::End
     */

    /**
     * Edit Section
     * ::Start
     */
    public function actionEdit()
    {
        $groupid = Yii::$app->request->get()['groupid'];
        $id = Yii::$app->request->get()['id'];

        $this->header = '@app/modules/eo/views/default/menu/_backheader';
        $this->headerText = "Update Class";
        $this->menu = '@app/modules/eo/views/event/_savemenu';
        $this->typeform = 'edit';
        $this->backLink = ['default/event','groupid' => $groupid];

        $event = Event::findOne($id);

        $data['typeGantungan'] = TypeGantungan::find()
                ->select(['name'])
                ->indexBy('id')
                ->column();

        $data['jenisBurung'] = EventGroupJenis::find()
                ->joinWith(['jenisBurung'])
                ->select(['jenis_burung.name'])
                ->where(['event_group_jenis_burung.event_group_id' => $groupid])
                ->indexBy('event_group_jenis_burung.jenis_burung_id')
                ->column();

        $data['event'] = Event::find()->where(['id' => $id])->one();
        $data['groupid'] = $groupid;
        $data['id'] = $id;

        return $this->render('@app/modules/eo/views/event/create', $data);
    }

    public function actionUpdateEvent(){
        $request = Yii::$app->request->post();
        $transaction = Yii::$app->db->beginTransaction();
        $userId= Yii::$app->usereo->getId() ?? null;

        try {
            $model = Event::findOne($request['id']);
            $model->nama_event = $request['nama_event'];
            $model->tgl_event = date('Y-m-d H:i:s',strtotime($request['tgl_pelaksanaan']));
            $model->nominal = $request['harga_tiket'];
            $model->waktu = $request['waktu'];
            $model->jenis_burung = $request['jenis_burung_id'];
            $model->type_gantungan_id = $request['type_gantungan_id'];
            $model->user_id = $userId;
            $model->event_group_id = $request['groupid'];
            $model->is_khusus = isset($request['penilaian_khusus']) ? $request['penilaian_khusus'] : '0';
            $model->updated_ad = date('Y-m-d H:i:s');
            $model->save();

            $tipeGantungan = TypeGantungan::find()->where(['id' => $request['type_gantungan_id']])->one();
            
            $col = $tipeGantungan->jumlah_col ?? 0;
            $row = $tipeGantungan->jumlah_row ?? 0;
            $type_gnt = $tipeGantungan->type_gnt ?? 0;
            
            $length = $row * $col;
            $chunk = $col;
            $loops = $this->snakeLoop($length, $chunk, $type_gnt);
            
            Gantungan::deleteAll(['event_id' => $request['id']]);

            $i = 1;
            foreach ($loops as $key => $loop) {
                foreach ($loop as $key => $value) {
                    $id = UuidHelper::uuid();
                    $gantungan = new Gantungan();
                    $gantungan->id = $id;
                    $gantungan->no_gnt = $value;
                    $gantungan->no_urut = $i;
                    $gantungan->event_id = $request['id'];
                    $gantungan->created_ad = date('Y-m-d H:i:s');
                    $gantungan->updated_ad = date('Y-m-d H:i:s');
                    $gantungan->save();
                    $i++;
                }
            }

            Yii::$app->getSession()->setFlash('success', 'Your event was edited successfully!');

            $callback = ['callback' => Url::toRoute(['default/event', 'groupid' => $request['groupid']])];

            $transaction->commit();
        } catch (\Throwable $th) {
            Yii::$app->getSession()->setFlash('error', 'Error while editing your event.'.$th->getMessage());

            $transaction->rollback();
            return $this->asJsonError([$th->getMessage()],'Gagal menghapus data.');
        }

        return $this->asJsonSuccess($callback,'Berhasil menghapus data.');
    }
    /**
     * Edit Section
     * ::End
     */

     
    /**
     * Add Penilaian Section
     * ::Start
     */

    public function actionAddPenilaian()
    {
        $id = Yii::$app->request->get()['id'];
        $groupid = Yii::$app->request->get()['groupid'];

        $this->header = '@app/modules/eo/views/default/menu/_backheader';
        $this->headerText = "Tambah Penilaian";
        $this->menu = '@app/modules/eo/views/event/_saveAddPenilaian';
        $this->groupid = $groupid;
        $this->backLink = ['default/event','groupid' => $groupid];

        $model['eventModel'] = new EventAddPenilaianForm;
        $model['typePenilaian'] = TypePenilaian::find()
                                    ->select(['nama_type'])
                                    ->indexBy('id')
                                    ->column();
        $model['penilaians'] = EventPenilaian::find()->where(['event_id' => $id])->all();
        $model['id'] = $id; 

        return $this->render('@app/modules/eo/views/event/addPenilaian', $model);
    }

    public function actionGetOptions(){
        $data = TypePenilaian::find()->all();

        return $this->asJsonSuccess($data,'[Success] Save data...');
    }

    public function actionSaveAddPenilaian(){
        $requests = Yii::$app->request->post();
        
        $transaction = Yii::$app->db->beginTransaction();

        try {
            $deleteEvent = EventPenilaian::deleteAll(['event_id' => $requests['id']]);

            foreach ($requests['tipe_penilaian_id'] as $key => $request) {
                $typePenilaian = TypePenilaian::findOne(['id' => $request]);

                $event = new EventPenilaian();
                $event->event_id = $requests['id'];
                $event->type_penilaian_id = $request;
                $event->nilai = $typePenilaian->nilai;
                $event->created_ad = date('Y-m-d H:i:s');
                $event->save();
            }

            $transaction->commit();

            $callback['callback'] = Url::home(true).'eo/event?groupid='.Yii::$app->request->get()['groupid'];
        } catch (\Throwable $th) {
            $transaction->rollback();
            return $this->asJsonError([$th->getMessage()],'[Error] Save data...');
        }

        return $this->asJsonSuccess($callback,'[Success] Save data...');
    }

    public function actionAjaxValidation(){
        $mymodel = new EventAddPenilaianForm; 
        
        $mymodel->load(Yii::$app->request->post());

        Yii::$app->response->format = Response::FORMAT_JSON; 
        
        return ActiveForm::validate($mymodel);
    }

    /**
     * Add Penilaian Section
     * ::End
     */

    /**
     * Delete Section
     * ::Start
     */
    public function actionDelete(){
        $id = Yii::$app->request->get()['id'];
        $groupid = Yii::$app->request->get()['groupid'];
        
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $model = Event::findOne($id)->delete();

            $transaction->commit();

            Yii::$app->getSession()->setFlash('success', 'Your event was deleted successfully!');
        } catch (\Throwable $th) {
            //throw $th;
            $transaction->rollback();
            
            Yii::$app->getSession()->setFlash('error', 'Error while deleting your event.');
        }

        return $this->redirect(['default/event','groupid' => $groupid]);
    }
    /**
     * Delete Section
     * ::End
     */

    private function snakeLoop($snakeLength, $chunk, $type = 'ASC'){
        function createArrayLoop($number){
            $temp = [];
            for ($i=1; $i <= $number ; $i++) { 
                $temp[$i] = $i;
            }

            return $temp;
        }

        $array = createArrayLoop($snakeLength);
        $newArrays = array_chunk($array, $chunk);
        
        if($type == 'DESC'){
            foreach ($newArrays as $key => $newArray) {
                $newArrays[$key] = array_reverse($newArray, true);
            }
        } else if($type === 'ASCSLINE'){
            foreach ($newArrays as $key => $newArray) {
                if(($key + 1) % 2 == 0){
                    $newArrays[$key] = array_reverse($newArray, true);
                }
            }
        } else if($type === 'DESCSLINE'){
            foreach ($newArrays as $key => $newArray) {
                $newArrays[$key] = array_reverse($newArray, true);
            }
            
            foreach ($newArrays as $key => $newArray) {
                if(($key + 1) % 2 == 0){
                    $newArrays[$key] = array_reverse($newArray, true);
                }
            }
        }

        return $newArrays;
    }
}
