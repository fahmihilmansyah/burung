<?php

namespace app\modules\eo\controllers;

use Yii;
use yii\web\Controller;
use yii\web\Response;
use yii\widgets\ActiveForm;
use app\modules\eo\models\Event;
use app\modules\eo\models\Juri;
use app\modules\eo\models\TypePenilaian;
use app\modules\eo\models\PemilikTempat;
use app\modules\eo\models\form\JuriForm;
use app\modules\eo\models\form\EventAddPenilaianForm;

/**
 * Default controller for the `eo` module
 */
class JuriController extends Controller
{
    public $layout = '@app/views/layouts/app.php';

    public $header;
    public $headerText;
    public $menu = '@app/modules/eo/views/default/menu/_mainmenu';

    public $backLink = 'juri/juri';

    public $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
   
    /**
     * List Section
     * ::Start
     */
    public function actionJuri()
    {
        $this->header = '@app/modules/eo/views/default/menu/_backheader';
        $this->headerText = "Event [List Juri]";
        $this->menu = '@app/modules/eo/views/juri/_mainmenu';

        $this->backLink = 'default/index';

        $model['juris'] = Juri::find()->orderBy(['id' => SORT_DESC])->all();

        return $this->render('@app/modules/eo/views/juri/index', $model);
    }
    /**
     * List Section
     * ::End
     */

    /**
     * Create Section
     * ::Start
     */
    public function actionCreate()
    {
        $this->header = '@app/modules/eo/views/default/menu/_backheader';
        $this->headerText = "Event [Create Juri]";
        $this->menu = '@app/modules/eo/views/juri/_savemenu';

        $model['juriModel'] = new JuriForm();
        $model['pemilikTempat'] = PemilikTempat::find()
                                    ->select(['nama_pemilik'])
                                    ->indexBy('id')
                                    ->column();
        $model['eventOptions'] = Event::find()
                                    ->select(['nama_event'])
                                    ->indexBy('id')
                                    ->column();

        return $this->render('@app/modules/eo/views/juri/create', $model);
    }

    public function actionSaveJuri(){
        $request = Yii::$app->request->post()['JuriForm'];

        $transaction = Yii::$app->db->beginTransaction();

        try {
            $model = new Juri();
            $model->pemilik_tempat_id = $request['pemilik_tempat_id'];
            $model->event_id = $request['event_id'];
            $model->nama_juri = $request['nama_juri'];
            $model->kode_juri = $this->generate_string($this->permitted_chars, 10);
            $model->email_juri = $request['email_juri'];
            $model->telp_juri = $request['telp_juri'];
            $model->created_ad = date('Y-m-d H:i:s');
            $model->save();
            
            Yii::$app->getSession()->setFlash('success', 'Your juri was created successfully!');

            $transaction->commit();
        } catch (\Throwable $th) {
            Yii::$app->getSession()->setFlash('error', $th->getMessage().'Error while submitting your juri.');

            $transaction->rollback();
        }

        return $this->redirect(['juri/juri']);
    }
    /**
     * Create Section
     * ::End
     */

    /**
     * Edit Section
     * ::Start
     */
    public function actionEdit()
    {
        $this->header = '@app/modules/eo/views/default/menu/_backheader';
        $this->headerText = "Event [Edit Juri]";
        $this->menu = '@app/modules/eo/views/juri/_savemenu';

        $id = Yii::$app->request->get()['id'];
        $juri = Juri::findOne($id);

        $model = new JuriForm();
        $model->id = $juri->id;
        $model->pemilik_tempat_id = $juri->pemilik_tempat_id;
        $model->nama_juri = $juri->nama_juri;
        $model->kode_juri =  $juri->kode_juri;
        $model->email_juri = $juri->email_juri;
        $model->telp_juri = $juri->telp_juri;
        $model->event_id = $juri->event_id;
            
        $data['juriModel'] =$model;
        $data['pemilikTempat'] = PemilikTempat::find()
                                    ->select(['nama_pemilik'])
                                    ->indexBy('id')
                                    ->column();
        $data['eventOptions'] = Event::find()
                                    ->select(['nama_event'])
                                    ->indexBy('id')
                                    ->column();

        return $this->render('@app/modules/eo/views/juri/edit', $data);
    }

    public function actionUpdateJuri(){
        $request = Yii::$app->request->post()['JuriForm'];
        $transaction = Yii::$app->db->beginTransaction();

        try {
            $model = Juri::findOne($request['id']);
            $model->pemilik_tempat_id = $request['pemilik_tempat_id'];
            $model->event_id = $request['event_id'];
            $model->nama_juri = $request['nama_juri'];
            $model->kode_juri = $request['kode_juri'];
            $model->email_juri = $request['email_juri'];
            $model->telp_juri = $request['telp_juri'];
            $model->updated_ad = date('Y-m-d H:i:s');
            $model->save();
            
            Yii::$app->getSession()->setFlash('success', 'Your event was edited successfully!');

            $transaction->commit();
        } catch (\Throwable $th) {
            Yii::$app->getSession()->setFlash('error', 'Error while editing your event.'.$th->getMessage());

            $transaction->rollback();
        }

        return $this->redirect(['juri/juri']);
    }
    /**
     * Edit Section
     * ::End
     */

     
    /**
     * Add Penilaian Section
     * ::Start
     */

    public function actionAddPenilaian()
    {
        $this->header = '@app/modules/eo/views/default/menu/_backheader';
        $this->headerText = "Event [Add Penilaian Event]";
        $this->menu = '@app/modules/eo/views/event/_saveAddPenilaian';

        $id = Yii::$app->request->get()['id'];

        $model['eventModel'] = [new EventAddPenilaianForm];
        $model['typePenilaian'] = TypePenilaian::find()
                                    ->select(['nama_type'])
                                    ->indexBy('id')
                                    ->column();

        return $this->render('@app/modules/eo/views/event/addPenilaian', $model);
    }

    public function actionAjaxValidation(){
        $mymodel = new EventAddPenilaianForm; 
        
        $mymodel->load(Yii::$app->request->post());

        Yii::$app->response->format = Response::FORMAT_JSON; 
        
        return ActiveForm::validate($mymodel);
    }

    /**
     * Add Penilaian Section
     * ::End
     */

    /**
     * Delete Section
     * ::Start
     */
    public function actionDelete(){
        $id = Yii::$app->request->get()['id'];
        
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $model = Juri::findOne($id)->delete();

            $transaction->commit();
            
            Yii::$app->getSession()->setFlash('success', 'Your event was deleted successfully!');
        } catch (\Throwable $th) {
            //throw $th;
            $transaction->rollback();
            Yii::$app->getSession()->setFlash('error', 'Error while deleting your event.');
        }

        return $this->redirect(['juri/juri']);
    }
    /**
     * Delete Section
     * ::End
     */
 
    function generate_string($input, $strength = 16) {
        $input_length = strlen($input);
        $random_string = '';
        for($i = 0; $i < $strength; $i++) {
            $random_character = $input[mt_rand(0, $input_length - 1)];
            $random_string .= $random_character;
        }
    
        return $random_string;
    }
}
