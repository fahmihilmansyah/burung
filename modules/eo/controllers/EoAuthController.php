<?php

namespace app\modules\eo\controllers;

use Yii;
use app\controllers\BaseController as Controller;
use yii\web\Response;
use yii\helpers\Url;
use app\helpers\MyLibs;
use yii\widgets\ActiveForm;
use app\modules\eo\models\Event;
use app\modules\eo\models\EventPenilaian;
use app\modules\eo\models\TypePenilaian;
use app\modules\eo\models\PemilikTempat;
use app\modules\eo\models\UserEo;
use app\modules\eo\models\UserEoOtp;
use app\modules\eo\models\UserEoLogin as Login;
use app\modules\eo\models\form\EventForm;
use app\modules\eo\models\form\EventAddPenilaianForm;
use yii\filters\AccessControl;

/**
 * Eo Auth controller for the `eo` module
 */
class EoAuthController extends Controller
{
    public $layout = '@app/views/layouts/app.php';

    public $header;
    public $headerText;
    public $menu = '@app/modules/eo/views/default/menu/_mainmenu';

    public $backLink = 'default/event';

     /**
     * Gate Display Section
     * ::Start
     */

    //  Register Page
    public function actionRegister()
    {
        $this->checkGuest();
        
        $this->menu = '//default/_registermenu';
        $this->header = '//default/_registerheader';
        $this->headerText = 'Daftar EO';

        return $this->render('@app/modules/eo/views/auth/register');
    }
    
    public function actionDoRegister(){
        $this->checkGuest();

        $request = Yii::$app->request->post();
        $created_date = date('Y-m-d H:i:s');
        
        $transaction = Yii::$app->db->beginTransaction();

        try {
            $hash = Yii::$app->getSecurity();


            $model = new UserEo();
            $model->nama_lengkap = $request['nama_lengkap'];
            $model->username = $request['username'];
            $model->email = $request['username'];
            $model->password = $hash->generatePasswordHash($request['password']);
            $model->nohp = $request['notelp'];
            $model->created_ad = $created_date;

            if(!$model->save()){
                throw Exception('Unable to save record.');
            }

            Yii::$app->timeZone = 'Asia/Jakarta';
            
            $deleteBefore = UserEoOtp::deleteAll([ 'user_id'=>$model->id]);

            $otp = new UserEoOtp();
            $otp->id = $model->id;
            $otp->otp_code = MyLibs::digitRandom(6);
            $otp->otp_lifetime = date( "Y-m-d H:i:s", strtotime(date("Y-m-d H:i:s")." +5 minutes"));
            $otp->user_id = $model->id;
            
            if(!$otp->save()){
                throw Exception('Unable to save record.');
            }

            $html = Yii::$app->mailer
                               ->render(
                                    '@app/mail/otp', 
                                    ['otp_code' => $otp->otp_code], 
                                    '@app/mail/layouts/html'
                                );

            $mailData['fromName'] = 'Gacor Application';
            $mailData['senderName'] = 'no-reply@gacorapp.com';
            $mailData['to'] = $request['username'];
            $mailData['subject'] = 'Kode OTP Gacor Application';
            $mailData['konten'] = $html;

            MyLibs::SendMail($mailData);

            $callback['callback'] = Url::home(true).'eo/register-otp?otpid='.$otp->id;
            $transaction->commit();
        } catch (\Throwable $th) {
            $transaction->rollback();
            return $this->asJsonError([$th->getMessage()],'[Error] Save data...');
        }

        return $this->asJsonSuccess($callback,'[Success] Save data...');
    }

    public function actionOtp(){
        $this->checkGuest();

        $this->layout = '@app/views/layouts/top.php';
        $this->header = '//default/_registerheader';
        $this->headerText = 'Konfirmasi Kode OTP';

        $otpid = Yii::$app->request->get()['otpid'];

        $otp = UserEoOtp::findOne($otpid);

        if(empty($otp)){
            return $this->redirect(['eo-auth/not-found']);
        }

        $model['otpid'] = $otpid;

        return $this->render('@app/modules/eo/views/auth/otp', $model);
    }

    public function actionOtpConfirmed(){
        $this->checkGuest();

        $request = Yii::$app->request->post();

        $transaction = Yii::$app->db->beginTransaction();
        try {
            $userOtp = UserEoOtp::findOne(['id' => $request['otpid']]);

            if($userOtp->otp_code === $request['otpcode']){
                $user = UserEo::findOne(['id' => $userOtp->user_id]);
                $user->is_active = 1;
                $user->save();

                $message = ['callback' => Url::home(true).'eo/register-success' ];
            }else{
                return $this->asJsonError(['callback' => null],'Kode konfirmasi salah, silahkan ulangi');
            }

            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollback();
            return $this->asJsonError([$th->getMessage()],'[Error] Save data...');
        }

        return $this->asJsonSuccess($message,'[Success] Save data...');
        
    }
    /**
     * Gate Register Section
     * ::End
     */
     
    /**
     * Login Display Section
     * ::Start
     */

    //  Login Page
    public function actionLogin()
    {
        $this->checkGuest();

        $this->layout = '@app/views/layouts/plain.php';
        
        return $this->render('@app/modules/eo/views/auth/login');
    }

    public function actionDoSignin(){
        $this->checkGuest();
        
        $model = new Login();
        $model->setAttributes(Yii::$app->request->post());

        $authenticate = $model->authenticate();

        if(!$authenticate['status']){
            return $this->asJsonError(['callback' => null], $authenticate['message']);
        }
       
        return $this->asJsonSuccess(['callback' => Url::home(true).'eo'],'[Success] Save data...');
    }
    /**
     * Login Display Section
     * ::End
     */

    public function actionLogout(){
        $this->checkGuest();

        \Yii::$app->usereo->logout(true);

        $this->redirect(['eo-auth/login']);
    }

    public function actionNotFound()
    {
        $this->layout = '@app/views/layouts/top.php';
        $this->header = '//default/_registerheader';
        $this->headerText = 'Halaman Tidak Ditemukan';

        return $this->render('notfound');
    }

    public function actionRegisterSuccess()
    {
        $this->layout = '@app/views/layouts/top.php';
        $this->header = '//default/_registerheader';
        $this->headerText = 'Pendaftaran Berhasil';

        return $this->render('@app/modules/eo/views/auth/registersucces');
    }

    public function checkGuest(){
        if (!\Yii::$app->usereo->isGuest) return $this->redirect(['default/index']);
    }
}