<?php

namespace app\modules\eo\controllers;

use Yii;
use yii\web\Controller;
use yii\web\Response;
use yii\widgets\ActiveForm;
use app\modules\eo\models\JenisBurung;
use app\modules\eo\models\form\JenisBurungForm;

/**
 * Default controller for the `eo` module
 */
class JenisBurungController extends Controller
{
    public $layout = '@app/views/layouts/app.php';

    public $header;
    public $headerText;
    public $menu = '@app/modules/eo/views/default/menu/_mainmenu';

    public $backLink = 'settings/jenis-burung';

    public $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
   
    /**
     * List Section
     * ::Start
     */
    public function actionJenisBurung()
    {
        $this->header = '@app/modules/eo/views/default/menu/_backheader';
        $this->headerText = "Settings: List Tipe Jenis Burung";
        $this->menu = '@app/modules/eo/views/settings/jenis_burung/_mainmenu';

        $this->backLink = 'settings/index';

        $model['JenisBurungs'] = JenisBurung::find()->orderBy(['id' => SORT_DESC])->all();

        return $this->render('@app/modules/eo/views/settings/jenis_burung/index', $model);
    }
    /**
     * List Section
     * ::End
     */

    /**
     * Create Section
     * ::Start
     */
    public function actionCreate()
    {
        $this->header = '@app/modules/eo/views/default/menu/_backheader';
        $this->headerText = "Settings - Create Tipe Jenis Burung";
        $this->menu = '@app/modules/eo/views/settings/jenis_burung/_savemenu';

        $model['jenisBurungModel'] = new JenisBurungForm();

        return $this->render('@app/modules/eo/views/settings/jenis_burung/create', $model);
    }

    public function actionSave(){
        $request = Yii::$app->request->post()['JenisBurungForm'];

        $transaction = Yii::$app->db->beginTransaction();

        try {
            $model = new JenisBurung();
            $model->name = $request['name'];
            $model->created_ad = date('Y-m-d H:i:s');
            $model->save();
            
            Yii::$app->getSession()->setFlash('success', 'Your Jenis Burung was created successfully!');

            $transaction->commit();
        } catch (\Throwable $th) {
            Yii::$app->getSession()->setFlash('error', $th->getMessage().'Error while submitting your tipe Jenis Burung.');

            $transaction->rollback();
        }

        return $this->redirect(['jenis-burung/jenis-burung']);
    }
    /**
     * Create Section
     * ::End
     */

    /**
     * Edit Section
     * ::Start
     */
    public function actionEdit()
    {
        $this->header = '@app/modules/eo/views/default/menu/_backheader';
        $this->headerText = "Event - Edit Tipe Jenis Burung";
        $this->menu = '@app/modules/eo/views/settings/jenis_burung/_savemenu';

        $id = Yii::$app->request->get()['id'];
        $jenisBurung = JenisBurung::findOne($id);

        $model = new JenisBurungForm();
        $model->id = $jenisBurung->id;
        $model->name = $jenisBurung->name;
            
        $data['jenisBurungModel'] =$model;

        return $this->render('@app/modules/eo/views/settings/jenis_burung/edit', $data);
    }

    public function actionUpdate(){
        $request = Yii::$app->request->post()['JenisBurungForm'];
        $transaction = Yii::$app->db->beginTransaction();
        $userid = Yii::$app->usereo->identity->getId() ?? ''; 

        try {
            $model = JenisBurung::findOne($request['id']);
            $model->name = $request['name'];
            $model->updated_ad = date('Y-m-d H:i:s');
            $model->save();
            
            Yii::$app->getSession()->setFlash('success', 'Your Jenis Burung was edited successfully!');

            $transaction->commit();
        } catch (\Throwable $th) {
            Yii::$app->getSession()->setFlash('error', 'Error while editing your tipe Jenis Burung.'.$th->getMessage());

            $transaction->rollback();
        }

        return $this->redirect(['jenis-burung/jenis-burung']);
    }
    /**
     * Edit Section
     * ::End
     */

    /**
    * Delete Section
    * ::Start
    */
    public function actionDelete(){
        $id = Yii::$app->request->get()['id'];
        
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $model = JenisBurung::findOne($id)->delete();

            $transaction->commit();
            
            Yii::$app->getSession()->setFlash('success', 'Your Jenis Burung was deleted successfully!');
        } catch (\Throwable $th) {
            //throw $th;
            $transaction->rollback();
            Yii::$app->getSession()->setFlash('error', 'Error while deleting your Jenis Burung.');
        }

        return $this->redirect(['jenis-burung/jenis-burung']);
    }
    /**
     * Delete Section
     * ::End
     */
 
    function generate_string($input, $strength = 16) {
        $input_length = strlen($input);
        $random_string = '';
        for($i = 0; $i < $strength; $i++) {
            $random_character = $input[mt_rand(0, $input_length - 1)];
            $random_string .= $random_character;
        }
    
        return $random_string;
    }
}
