<?php

namespace app\modules\bo\models;
use yii\base\NotSupportedException;
use yii\db\ActiveRecord;
use yii\helpers\Security;
use yii\web\IdentityInterface;
use yii\behaviors\TimestampBehavior;

class User extends \yii\db\ActiveRecord  implements IdentityInterface
{
    public $auth_key;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'admins';
    }
 
    public static function findIdentity($id){
        return static::findOne($id);
    }

    public static function findIdentityByAccessToken($token, $type = null){
        throw new NotSupportedException('Not have access token field');
    }

    public function getId(){
        return $this->id;
    }

    public function getName(){
        return $this->name;
    }

    public function getAuthKey(){
        return $this->auth_key;
    }

    public function validateAuthKey($authKey){
        return $this->getAuthKey() === $authKey;
    }

    public static function findByUsername($username){
        return self::findOne(['username'=>$username]);
    }

    public static function findByEmail($email){
        return self::find()
                        ->with('role.menus.childrens')
                        ->where(['admins.email'=>$email])
                        ->one();
    }

    public function validatePassword($password){
        return Yii::$app->getSecurity()->validatePassword($password, $this->password);
    }

    public function getRole(){
        return $this->hasOne(Roles::className(), ['id' => 'role_id']);
    }

    public static function find()
    {
        return parent::find()->onCondition(['admins.is_deleted' => '0']);
    }
}