<?php

namespace app\modules\bo\models;

class Icons extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'icons';
    }

    public function getChildrens()
    {
        return $this->hasMany(self::className(), ['parent' => 'id'])
                    ->orderBy([
                        'menus.sort_number' => SORT_ASC
                    ]);
    }

    public function getRoles(){
        return $this->hasMany(Roles::className(), ['id' => 'role_id'])
                    ->viaTable('roles_access', ['role_id' => 'id']);
    }
}