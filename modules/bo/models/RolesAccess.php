<?php

namespace app\modules\bo\models;

class RolesAccess extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'roles_access';
    }
}