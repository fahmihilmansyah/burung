<?php

namespace app\modules\bo\models;

class Menu extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'menus';
    }

    public function getChildrens()
    {
        return $this->hasMany(self::className(), ['parent' => 'id'])
                    ->orderBy([
                        'menus.sort_number' => SORT_ASC
                    ]);
    }

    public function getRoles(){
        return $this->hasMany(Roles::className(), ['id' => 'role_id'])
                    ->viaTable('roles_access', ['role_id' => 'id']);
    }

    public static function find()
    {
        return parent::find()->onCondition(['menus.is_deleted' => '0']);
    }
}