<?php

namespace app\modules\bo\models;

class Roles extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'roles';
    }

    /**
    * Gets query for [[Users]].
    *
    * @return \yii\db\ActiveQuery
    */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['role_id' => 'id']);
    }

    public function getMenus(){
        return $this->hasMany(Menu::className(), ['id' => 'menu_id'])
                    ->viaTable('roles_access', ['role_id' => 'id'])
                    ->where(['menus.parent' => '0'])
                    ->andWhere(['menus.is_deleted' => '0'])
                    ->orderBy([
                        'menus.sort_number' => SORT_ASC
                    ]);
    }

    public static function find()
    {
        return parent::find()->onCondition(['roles.is_deleted' => '0']);
    }
}