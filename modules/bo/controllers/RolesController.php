<?php

namespace app\modules\bo\controllers;

use Yii;
use yii\helpers\Url;
use app\controllers\BaseController as Controller;
use app\modules\bo\models\Roles;
use app\modules\bo\models\RolesAccess;
use app\modules\bo\models\Menu;
use thamtech\uuid\helpers\UuidHelper;
use app\helpers\Logs;

/**
 * Default controller for the `bopanel` module
 */
class RolesController extends Controller
{
    public $layout = '@app/views/layouts/controlpanel.php';
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('@app/modules/bo/views/manajemen/roles/index');
    }

    public function actionDrawTable(){
        $request = Yii::$app->request->get();

        $draw = $request['draw'];
        $start = $request['start'];
        $rowperpage = $request['length']; // Rows display per page
        $searchKey = $request['q'] ?? '';

        $columnIndex_arr = $request['order'];
        $columnName_arr = $request['columns'];
        $order_arr = $request['order'];
        $search_arr = $request['search'];

        $columnIndex = $columnIndex_arr[0]['column']; // Column index
        $columnName = $columnName_arr[$columnIndex]['data']; // Column name
        $columnSortOrder = $order_arr[0]['dir'] === 'asc' ? SORT_ASC : SORT_DESC; // asc or desc
        $searchValue = $search_arr['value']; // Search value

        // Total records
        $totalRecords = Roles::find()
                                ->where([])
                                ->count();
        $totalRecordswithFilter = Roles::find()
                                ->where(['like', 'name', '%' . $searchValue . '%', false])
                                ->count();

        // Fetch records
        $records = Roles::find()
                    ->where(['like', 'name', '%' . $searchValue . '%', false])
                    ->orderBy([$columnName => $columnSortOrder])
                    ->limit($rowperpage)
                    ->offset($start)
                    ->all();

        $data_arr = array();

        foreach($records as $record){
            $btn = [
                'edit' => [
                    'to' => Url::toRoute(['roles/edit','id' => $record->id]),
                    'class' => 'btn btn-sm btn-warning',
                    'icon' => 'icofont icofont-ui-edit'
                ],
                'delete' => [
                    'to' => 'javascript:void(0)',
                    'class' => 'btn btn-sm btn-danger',
                    'icon' => 'icofont icofont-trash',
                    'onclick' => 'tableDelete(\''.$record->id.'\')',
                ]
            ];
            
            $created_at = !empty($record->created_at) ? date('d M Y',strtotime($record->created_at)) : '*';
            $updated_at = !empty($record->updated_at) ? date('d M Y',strtotime($record->updated_at)) : '*';

            $data_arr[] = array(
                'name' => $record->name ?? '*',
                'description' => $record->description ?? '*',
                'created_at' => $created_at,
                'updated_at' => $updated_at,
                'action' => $this->dtButton($btn)
            );
        }

        $response = array(
            'draw' => intval($draw),
            'iTotalRecords' => $totalRecords,
            'iTotalDisplayRecords' => $totalRecordswithFilter,
            'aaData' => $data_arr,
            'realData' => $records
        );

        return $this->asJsonDatatable($response,'Berhasil mendisplay data.');
    }

    public function actionCreate(){
        return $this->render('@app/modules/bo/views/manajemen/roles/form');
    }

    public function actionDrawMenusRoleTable(){
        $request = Yii::$app->request->get();

        $menus = Menu::find()->all();

        $data_arr = array();

        foreach ($menus as $key => $record) {

            $checked = RolesAccess::find()
                                ->where([
                                    'menu_id' => $record->id,
                                    'role_id' => $request['id']
                                ])->count();


            $checkbox =  '<input type="checkbox" name="roles_access[]" value="'.$record->id.'" ';
            if(intVal($record->parent) === 0) 
            {
                $checkbox .= 'class="parent" parentId="'.$record->id.'"';
            }else{
                $checkbox .= 'class="child" references="'.$record->parent.'"';
            }
            $checkbox .= ($checked > 0) ? 'checked' : '';
            $checkbox .= '/>';
                                   
            
            $data_arr[] = array(
                'tt_key' => intVal($record->id),
                'tt_parent' => (intVal($record->parent) === 0) ? intVal(0) : intVal($record->parent),
                'name' => $record->name,
                'path' => $record->path,
                'icon' => $record->icon,
                'created_at' => $record->created_at,
                'updated_at' => $record->updated_at,
                'action' => $checkbox
            );
        }

        return $this->asJson($data_arr,'[Success] display data...');
    }

    public function actionSave(){
        $request = Yii::$app->request->post();

        Yii::$app->timeZone = 'Asia/Jakarta';

        $transaction = Yii::$app->db->beginTransaction();

        try {
            $roles = new Roles();
            $roles->id = UuidHelper::uuid();
            $roles->name = $request['name'] ?? '';
            $roles->description = $request['description'] ?? '';
            $roles->created_at = date('Y-m-d H:i:s');

            if(!$roles->save()){
                throw Exception('Unable to save record.');
            }

            if(!empty($request['roles_access'])){
                foreach ($request['roles_access'] as $key => $value) {
                    $rolesAccess = new RolesAccess();
                    $rolesAccess->id = UuidHelper::uuid();
                    $rolesAccess->menu_id = $value;
                    $rolesAccess->role_id = $roles->id;
                    $rolesAccess->save();
                }
            }

            $callback = ['callback' => Url::toRoute(['roles/index'])];

            $transaction->commit();

            // log 
            Logs::save([
                'controller' => Yii::$app->controller->id,
                'topic' => 'CREATE RESOURCE',
                'activity' => 'user berhasil membuat roles',
                'status' => 'SUCCESS',
            ]);
        } catch (\Throwable $th) {
            $transaction->rollback();
            
            // log 
            Logs::save([
                'controller' => Yii::$app->controller->id,
                'topic' => 'CREATE RESOURCE',
                'activity' => 'user gagal membuat roles',
                'reason' => $th->getMessage(),
                'status' => 'ERROR'
            ]);

            return $this->asJsonError([$th->getMessage()],'Gagal menyimpan data.');
        }
        return $this->asJsonSuccess($callback,'Berhasil menyimpan data.');
    }

    public function actionEdit(){
        $id = Yii::$app->request->get()['id'];
        $model['id'] = $id;
        $model['role'] = Roles::find()->where(['id' => $id])->one();
        return $this->render('@app/modules/bo/views/manajemen/roles/form',$model);
    }

    public function actionUpdate(){
        $request = Yii::$app->request->post();

        Yii::$app->timeZone = 'Asia/Jakarta';

        $transaction = Yii::$app->db->beginTransaction();

        try {
            $roles = Roles::find()->where(['id' => $request['id']])->one();
            $roles->name = $request['name'] ?? '';
            $roles->description = $request['description'] ?? '';
            $roles->updated_at = date('Y-m-d H:i:s');

            if(!$roles->save()){
                throw Exception('Unable to save record.');
            }
            
            $deleteRolesAccess = RolesAccess::deleteAll(['role_id' => $request['id']]);

            if(!empty($request['roles_access'])){
                foreach ($request['roles_access'] as $key => $value) {
                    $rolesAccess = new RolesAccess();
                    $rolesAccess->id = UuidHelper::uuid();
                    $rolesAccess->menu_id = $value;
                    $rolesAccess->role_id = $roles->id;
                    $rolesAccess->save();
                }
            }

            $callback = ['callback' => Url::toRoute(['roles/index'])];

            $transaction->commit();

            // log 
            Logs::save([
                'controller' => Yii::$app->controller->id,
                'topic' => 'UPDATE RESOURCE',
                'activity' => 'user berhasil mengubah roles',
                'status' => 'SUCCESS',
            ]);

        } catch (\Throwable $th) {
            $transaction->rollback();

            // log 
            Logs::save([
                'controller' => Yii::$app->controller->id,
                'topic' => 'UPDATE RESOURCE',
                'activity' => 'user gagal mengubah roles',
                'reason' => $th->getMessage(),
                'status' => 'ERROR'
            ]);

            return $this->asJsonError([$th->getMessage()],'Gagal menyimpan data.');
        }
        return $this->asJsonSuccess($callback,'Berhasil menyimpan data.');
    }

    public function actionDelete(){
        $id = Yii::$app->request->get()['id'];
        
        $transaction = Yii::$app->db->beginTransaction();

        Yii::$app->timeZone = 'Asia/Jakarta';

        try {
            $model = Roles::findOne(['id' => $id]);
            $model->updated_at = date('Y-m-d H:i:s');
            $model->is_deleted = '1';
            if(!$model->save()){
                throw Exception('Unable to save record.');
            }

            $callback = ['callback' => Url::toRoute(['roles/index'])];

            $transaction->commit();

            // log 
            Logs::save([
                'controller' => Yii::$app->controller->id,
                'topic' => 'DELETE RESOURCE',
                'activity' => 'user berhasil menghapus roles',
                'status' => 'SUCCESS',
            ]);

        } catch (\Throwable $th) {
            $transaction->rollback();

            // log 
            Logs::save([
                'controller' => Yii::$app->controller->id,
                'topic' => 'DELETE RESOURCE',
                'activity' => 'user gagal menghapus roles',
                'reason' => $th->getMessage(),
                'status' => 'ERROR'
            ]);

            return $this->asJsonError([$th->getMessage()],'Gagal menghapus data.');
        }

        return $this->asJsonSuccess($callback,'Berhasil menghapus data.');
    }
}
