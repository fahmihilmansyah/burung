<?php

namespace app\modules\bo\controllers;

use Yii;
use yii\web\Controller;
use app\modules\bo\models\User;

/**
 * Default controller for the `bopanel` module
 */
class DefaultController extends Controller
{
    public $layout = '@app/views/layouts/controlpanel.php';
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $app = Yii::$app->userbo->identity;
        return $this->render('index');
    }
}
