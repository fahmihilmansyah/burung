<?php

namespace app\modules\bo\controllers;

use Yii;
use yii\helpers\Url;
use app\controllers\BaseController as Controller;
use app\modules\bo\models\Menu;
use app\modules\bo\models\Icons;
use app\helpers\Logs;


/**
 * Default controller for the `bopanel` module
 */
class MenusController extends Controller
{
    public $layout = '@app/views/layouts/controlpanel.php';
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('@app/modules/bo/views/manajemen/menus/index');
    }

    public function actionDrawTable(){
        $request = Yii::$app->request->get();

        $menus = Menu::find()->all();

        $data_arr = array();

        foreach ($menus as $key => $record) {
            $btn = [
                'edit' => [
                    'to' => Url::toRoute(['menus/edit','id' => $record->id]),
                    'class' => 'btn btn-sm btn-warning',
                    'icon' => 'icofont icofont-ui-edit'
                ],
                'delete' => [
                    'to' => 'javascript:void(0)',
                    'class' => 'btn btn-sm btn-danger',
                    'icon' => 'icofont icofont-trash',
                    'onclick' => 'tableDelete(\''.$record->id.'\')',
                ]
            ];
                        
            $data_arr[] = array(
                'tt_key' => intVal($record->id),
                'tt_parent' => ($record->parent === '*') ? intVal(0) : intVal($record->parent),
                'name' => $record->name,
                'path' => $record->path,
                'icon' => $record->icon,
                'created_at' => $record->created_at,
                'updated_at' => $record->updated_at,
                'action' => $this->dtButton($btn)
            );
        }

        return $this->asJson($data_arr,'[Success] display data...');
    }

    public function actionCreate(){
        $model['menuOptions'] = Menu::find()
                                    ->select(['name'])
                                    ->indexBy('id')
                                    ->where(['parent' => '0'])
                                    ->column();
        $model['iconOptions'] = Icons::find()
                                    ->select(['icon'])
                                    ->indexBy('icon')
                                    ->column();

        return $this->render('@app/modules/bo/views/manajemen/menus/form',$model);
    }

    public function actionSave(){
        $request = Yii::$app->request->post();

        $transaction = Yii::$app->db->beginTransaction();
        $hash = Yii::$app->getSecurity();
        
        $max = Menu::find()->max('sort_number');

        Yii::$app->timeZone = 'Asia/Jakarta';

        try {
            $menu = new Menu();
            $menu->name = $request['name'] ?? '';
            $menu->sort_number = intVal($max) + 1;
            $menu->path = !empty($request['path']) ? $request['path'] : '#';
            $menu->icon = $request['icon'] ?? '';
            $menu->parent = (empty($request['parent']) || $request === '') ? '0' : $request['parent'];
            $menu->created_at = date('Y-m-d H:i:s');

            if(!$menu->save()){
                throw Exception('Unable to save record.');
            }

            $callback = ['callback' => Url::toRoute(['menus/index'])];

            $transaction->commit();

            // log 
            Logs::save([
                'controller' => Yii::$app->controller->id,
                'topic' => 'CREATE RESOURCE',
                'activity' => 'user berhasil membuat menu',
                'status' => 'SUCCESS',
            ]);
            
        } catch (\Throwable $th) {
            $transaction->rollback();

            // log 
            Logs::save([
                'controller' => Yii::$app->controller->id,
                'topic' => 'CREATE RESOURCE',
                'activity' => 'user gagal membuat menu',
                'reason' => $th->getMessage(),
                'status' => 'ERROR'
            ]);

            return $this->asJsonError([$th->getMessage()],'Gagal menyimpan data.');
        }
        return $this->asJsonSuccess($callback,'Berhasil menyimpan data.');
    }

    public function actionEdit(){
        $id = Yii::$app->request->get()['id'];

        $model['menuOptions'] = Menu::find()
                                    ->select(['name'])
                                    ->indexBy('id')
                                    ->where(['parent' => '0'])
                                    ->column();
        $model['iconOptions'] = Icons::find()
                                    ->select(['icon'])
                                    ->indexBy('icon')
                                    ->column();
        $model['id'] = $id;
        $model['menu'] = Menu::findOne(['id' => $id]);
        return $this->render('@app/modules/bo/views/manajemen/menus/form',$model);
    }

    public function actionUpdate(){
        $request = Yii::$app->request->post();

        $transaction = Yii::$app->db->beginTransaction();
        $hash = Yii::$app->getSecurity();
        
        $max = Menu::find()->max('sort_number');

        Yii::$app->timeZone = 'Asia/Jakarta';

        try {
            $menu = Menu::find()->where(['id' => $request['id']])->one();
            $menu->name = $request['name'] ?? '';
            $menu->path = !empty($request['path']) ? $request['path'] : '#';
            $menu->icon = $request['icon'] ?? '';
            $menu->parent = (empty($request['parent']) || $request === '') ? '0' : $request['parent'];
            $menu->created_at = date('Y-m-d H:i:s');

            if(!$menu->save()){
                throw Exception('Unable to save record.');
            }

            $callback = ['callback' => Url::toRoute(['menus/index'])];

            $transaction->commit();

            // log 
            Logs::save([
                'controller' => Yii::$app->controller->id,
                'topic' => 'UPDATE RESOURCE',
                'activity' => 'user berhasil mengubah menu',
                'status' => 'SUCCESS',
            ]);
        } catch (\Throwable $th) {
            $transaction->rollback();

            // log 
            Logs::save([
                'controller' => Yii::$app->controller->id,
                'topic' => 'UPDATE RESOURCE',
                'activity' => 'user gagal mengubah menu',
                'reason' => $th->getMessage(),
                'status' => 'ERROR'
            ]);

            return $this->asJsonError([$th->getMessage()],'Gagal menyimpan data.');
        }
        return $this->asJsonSuccess($callback,'Berhasil menyimpan data.');
    }

    public function actionDelete(){
        $id = Yii::$app->request->get()['id'];
        
        $transaction = Yii::$app->db->beginTransaction();

        Yii::$app->timeZone = 'Asia/Jakarta';

        try {
            $model = Menu::findOne(['id' => $id]);
            $model->updated_at = date('Y-m-d H:i:s');
            $model->is_deleted = '1';
            if(!$model->save()){
                throw Exception('Unable to save record.');
            }

            $callback = ['callback' => Url::toRoute(['menus/index'])];

            $transaction->commit();

            // log 
            Logs::save([
                'controller' => Yii::$app->controller->id,
                'topic' => 'DELETE RESOURCE',
                'activity' => 'user berhasil menghapus menu',
                'status' => 'SUCCESS',
            ]);

        } catch (\Throwable $th) {
            $transaction->rollback();

            // log 
            Logs::save([
                'controller' => Yii::$app->controller->id,
                'topic' => 'DELETE RESOURCE',
                'activity' => 'user gagal menghapus menu',
                'reason' => $th->getMessage(),
                'status' => 'ERROR'
            ]);

            return $this->asJsonError([$th->getMessage()],'Gagal menghapus data.');
        }

        return $this->asJsonSuccess($callback,'Berhasil menghapus data.');
    }
}
