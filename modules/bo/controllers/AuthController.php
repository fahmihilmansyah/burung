<?php

namespace app\modules\bo\controllers;

use Yii;
use app\controllers\BaseController as Controller;
use yii\web\Response;
use yii\helpers\Url;
use app\helpers\MyLibs;
use yii\widgets\ActiveForm;
use yii\filters\AccessControl;
use app\modules\bo\models\User;
use app\modules\bo\models\UserLogin as Login;
use app\helpers\Logs;

/**
 * Eo Auth controller for the `eo` module
 */
class AuthController extends Controller
{
    public $layout = '@app/views/layouts/signin.php';

    /**
     * Login Display Section
     * ::Start
     */
    public function actionSignin(){
        $this->checkGuest();
        return $this->render('@app/modules/bo/views/auth/signin');
    }

    public function actionDoSignin(){
        $model = new Login();
        $model->setAttributes(Yii::$app->request->post());

        $authenticate = $model->authenticate();

        if(!$authenticate['status']){
            // log 
            Logs::save([
                'controller' => Yii::$app->controller->id,
                'topic' => 'authentication',
                'activity' => 'user gagal melakukan login',
                'reason' => $authenticate['message'],
                'status' => 'ERROR',
                'users' => 'Guest'
            ]);

            return $this->asJsonError(['callback' => null], $authenticate['message']);
        }
        
        Logs::save([
            'controller' => Yii::$app->controller->id,
            'topic' => 'authentication',
            'activity' => 'user berhasil melakukan login',
            'status' => 'SUCCESS',
        ]);

        return $this->asJsonSuccess(['callback' => Url::home(true).'bo'],'[Success] Save data...');
    }
    /**
     * Login Display Section
     * ::End
     */

    public function actionLogout(){
        
        // log 
        Logs::save([
            'controller' => Yii::$app->controller->id,
            'topic' => 'authentication',
            'activity' => 'user melogout akunnya',
            'status' => 'SUCCESS',
        ]);

        \Yii::$app->userbo->logout(true);
        $this->redirect(['auth/signin']);
    }

    public function checkGuest(){
        if (!\Yii::$app->userbo->isGuest) return $this->redirect(['default/index']);
    }
}