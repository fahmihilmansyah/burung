<?php

namespace app\modules\bo\controllers;

use Yii;
use yii\helpers\Url;
use app\helpers\MyLibs;
use app\controllers\BaseController as Controller;
use app\modules\bo\models\User;
use app\modules\bo\models\Roles;
use thamtech\uuid\helpers\UuidHelper;
use app\helpers\Logs;

/**
 * Default controller for the `bopanel` module
 */
class UsersController extends Controller
{
    public $layout = '@app/views/layouts/controlpanel.php';
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('@app/modules/bo/views/manajemen/users/index');
    }

    public function actionDrawTable(){
        $request = Yii::$app->request->get();

        $draw = $request['draw'];
        $start = $request['start'];
        $rowperpage = $request['length']; // Rows display per page
        $searchKey = $request['q'] ?? '';

        $columnIndex_arr = $request['order'];
        $columnName_arr = $request['columns'];
        $order_arr = $request['order'];
        $search_arr = $request['search'];

        $columnIndex = $columnIndex_arr[0]['column']; // Column index
        $columnName = $columnName_arr[$columnIndex]['data']; // Column name
        $columnSortOrder = $order_arr[0]['dir'] === 'asc' ? SORT_ASC : SORT_DESC; // asc or desc
        $searchValue = $search_arr['value']; // Search value

        // Total records
        $totalRecords = User::find()
                                ->where([])
                                ->count();
        $totalRecordswithFilter = User::find()
                                ->joinWith(['role'])
                                ->where(['like', 'admins.email', '%' . $searchValue . '%', false])
                                ->orWhere(['like', 'admins.name', '%' . $searchValue . '%', false])
                                ->orWhere(['like', 'roles.name', '%' . $searchValue . '%', false])
                                ->count();

        // Fetch records
        $records = User::find()
                    ->joinWith(['role'])
                    ->where(['like', 'admins.email', '%' . $searchValue . '%', false])
                    ->orWhere(['like', 'admins.name', '%' . $searchValue . '%', false])
                    ->orWhere(['like', 'roles.name', '%' . $searchValue . '%', false])
                    ->orderBy([$columnName => $columnSortOrder])
                    ->limit($rowperpage)
                    ->offset($start)
                    ->all();

        $data_arr = array();

        foreach($records as $record){
            $btn = [
                'edit' => [
                    'to' => Url::toRoute(['users/edit','id' => $record->id]),
                    'class' => 'btn btn-sm btn-warning',
                    'icon' => 'icofont icofont-ui-edit'
                ],
                'delete' => [
                    'to' => 'javascript:void(0)',
                    'class' => 'btn btn-sm btn-danger',
                    'icon' => 'icofont icofont-trash',
                    'onclick' => 'tableDelete(\''.$record->id.'\')',
                ]
            ];
            
            $created_at = !empty($record->created_at) ? date('d M Y',strtotime($record->created_at)) : '*';
            $updated_at = !empty($record->updated_at) ? date('d M Y',strtotime($record->updated_at)) : '*';

            $data_arr[] = array(
                'name' => $record->name ?? '*',
                'email' => $record->email ?? '*',
                'role' => $record->role->name ?? '*',
                'created_at' => $created_at,
                'updated_at' => $updated_at,
                'action' => $this->dtButton($btn)
            );
        }

        $response = array(
            'draw' => intval($draw),
            'iTotalRecords' => $totalRecords,
            'iTotalDisplayRecords' => $totalRecordswithFilter,
            'aaData' => $data_arr,
            'realData' => $records
        );

        return $this->asJsonDatatable($response,'Berhasil mendisplay data.');
    }

    public function actionCreate(){
        $model['roleOptions'] = Roles::find()
                                    ->select(['name'])
                                    ->indexBy('id')
                                    ->column();
        return $this->render('@app/modules/bo/views/manajemen/users/form',$model);
    }

    public function actionSave(){
        $request = Yii::$app->request->post();

        $transaction = Yii::$app->db->beginTransaction();
        $hash = Yii::$app->getSecurity();

        Yii::$app->timeZone = 'Asia/Jakarta';

        try {
            $passwordGenerated = MyLibs::digitRandom(6);

            $user = new User();
            $user->id = UuidHelper::uuid();
            $user->name = $request['name'] ?? '';
            $user->username = $request['email'] ?? '';
            $user->email = $request['email'] ?? '';
            $user->password = $hash->generatePasswordHash($passwordGenerated);
            $user->created_at = date('Y-m-d H:i:s');
            $user->role_id = $request['role_id'];

            if(!$user->save()){
                throw Exception('Unable to save record.');
            }

            $role = Roles::findOne(['id' => $request['role_id']]);

            $html = Yii::$app->mailer
                    ->render(
                        '@app/mail/otp', 
                        [
                            'nama_lengkap' =>  $request['name'],
                            'email' =>  $request['email'],
                            'role' =>  $role->name,
                            'password' =>  $passwordGenerated,
                        ], 
                        '@app/mail/layouts/html'
                    );

            $mailData['fromName'] = 'JOS Indonesia';
            $mailData['senderName'] = 'no-reply@jos.id';
            $mailData['to'] = $request['email'];
            $mailData['subject'] = 'Password Login Kamu.';
            $mailData['konten'] = $html;

            MyLibs::SendMail($mailData);

            $callback = ['callback' => Url::toRoute(['users/index'])];

            $transaction->commit();

            // log 
            Logs::save([
                'controller' => Yii::$app->controller->id,
                'topic' => 'CREATE RESOURCE',
                'activity' => 'berhasil membuat user',
                'status' => 'SUCCESS',
            ]);

        } catch (\Throwable $th) {
            $transaction->rollback();

            // log 
            Logs::save([
                'controller' => Yii::$app->controller->id,
                'topic' => 'CREATE RESOURCE',
                'activity' => 'user gagal membuat roles',
                'reason' => $th->getMessage(),
                'status' => 'ERROR'
            ]);

            return $this->asJsonError([$th->getMessage()],'Gagal menyimpan data.');
        }
        return $this->asJsonSuccess($callback,'Berhasil menyimpan data.');
    }

    public function actionEdit(){
        $id = Yii::$app->request->get()['id'];

        $model['roleOptions'] = Roles::find()
                                    ->select(['name'])
                                    ->indexBy('id')
                                    ->column();
        $model['id'] = $id;
        $model['user'] = User::findOne(['id' => $id]);
        return $this->render('@app/modules/bo/views/manajemen/users/form',$model);
    }

    public function actionUpdate(){
        $request = Yii::$app->request->post();

        $transaction = Yii::$app->db->beginTransaction();
        $hash = Yii::$app->getSecurity();

        Yii::$app->timeZone = 'Asia/Jakarta';

        try {
            $passwordGenerated = MyLibs::digitRandom(6);

            $user = User::findOne(['id' => $request['id']]);
            $user->name = $request['name'] ?? '';
            $user->role_id = $request['role_id'];
            $user->updated_at = date('Y-m-d H:i:s');

            if(!$user->save()){
                throw Exception('Unable to save record.');
            }

            $callback = ['callback' => Url::toRoute(['users/index'])];

            $transaction->commit();

            // log 
            Logs::save([
                'controller' => Yii::$app->controller->id,
                'topic' => 'UPDATE RESOURCE',
                'activity' => 'berhasil mengubah user',
                'status' => 'SUCCESS',
            ]);

        } catch (\Throwable $th) {
            $transaction->rollback();

            // log 
            Logs::save([
                'controller' => Yii::$app->controller->id,
                'topic' => 'UPDATE RESOURCE',
                'activity' => 'gagal mengubah roles',
                'reason' => $th->getMessage(),
                'status' => 'ERROR'
            ]);

            return $this->asJsonError([$th->getMessage()],'Gagal menyimpan data.');
        }
        return $this->asJsonSuccess($callback,'Berhasil menyimpan data.');
    }

    public function actionDelete(){
        $id = Yii::$app->request->get()['id'];
        
        $transaction = Yii::$app->db->beginTransaction();

        Yii::$app->timeZone = 'Asia/Jakarta';

        try {
            $model = User::findOne(['id' => $id]);
            $model->updated_at = date('Y-m-d H:i:s');
            $model->is_deleted = '1';
            if(!$model->save()){
                throw Exception('Unable to save record.');
            }

            $transaction->commit();

            // log 
            Logs::save([
                'controller' => Yii::$app->controller->id,
                'topic' => 'DELETE RESOURCE',
                'activity' => 'berhasil menghapus user',
                'status' => 'SUCCESS',
            ]);

        } catch (\Throwable $th) {
            $transaction->rollback();

            // log 
            Logs::save([
                'controller' => Yii::$app->controller->id,
                'topic' => 'DELETE RESOURCE',
                'activity' => 'user gagal menghapus user',
                'reason' => $th->getMessage(),
                'status' => 'ERROR'
            ]);

            return $this->asJsonError([$th->getMessage()],'Gagal menghapus data.');
        }

        return $this->asJsonSuccess($model,'Berhasil menghapus data.');
    }
}
