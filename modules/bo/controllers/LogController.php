<?php

namespace app\modules\bo\controllers;

use Yii;
use yii\helpers\Url;
use app\controllers\BaseController as Controller;
use app\models\Logs as LogModel;

/**
 * Default controller for the `bopanel` module
 */
class LogController extends Controller
{
    public $layout = '@app/views/layouts/controlpanel.php';
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('@app/modules/bo/views/default/logs');
    }

    public function actionDrawTable(){
        $request = Yii::$app->request->get();

        $draw = $request['draw'];
        $start = $request['start'];
        $rowperpage = $request['length']; // Rows display per page
        $searchKey = $request['q'] ?? '';

        $columnIndex_arr = $request['order'];
        $columnName_arr = $request['columns'];
        $order_arr = $request['order'];
        $search_arr = $request['search'];

        $columnIndex = $columnIndex_arr[0]['column']; // Column index
        $columnName = $columnName_arr[$columnIndex]['data']; // Column name
        $columnSortOrder = $order_arr[0]['dir'] === 'asc' ? SORT_ASC : SORT_DESC; // asc or desc
        $searchValue = $search_arr['value']; // Search value

        // Total records
        $totalRecords = LogModel::find()
                                ->where([])
                                ->count();
        $totalRecordswithFilter = LogModel::find()
                                ->where(['like', 'topic', '%' . $searchValue . '%', false])
                                ->count();

        // Fetch records
        $records = LogModel::find()
                    ->where(['like', 'topic', '%' . $searchValue . '%', false])
                    ->orderBy([$columnName => $columnSortOrder])
                    ->limit($rowperpage)
                    ->offset($start)
                    ->all();

        $data_arr = array();

        foreach($records as $record){
            $created_at = !empty($record->created_ad) ? date('d M Y',strtotime($record->created_ad)) : '*';
          
            $data_arr[] = array(
                'controller' => $record->controller ?? '*',
                'topic' => $record->topic ?? '*',
                'source_ip' => $record->source_ip?? '*',
                'browser_agent' => $record->browser_agent?? '*',
                'activity' => $record->activity?? '*',
                'reason' => !empty($record->reason) ? $record->reason : '*',
                'status' => $record->status?? '*',
                'user_id' => $record->user_id?? '*',
                'user_application' => $record->user_application?? '*',
                'created_at' => $created_at,
            );
        }

        $response = array(
            'draw' => intval($draw),
            'iTotalRecords' => $totalRecords,
            'iTotalDisplayRecords' => $totalRecordswithFilter,
            'aaData' => $data_arr,
            'realData' => $records
        );

        return $this->asJsonDatatable($response,'Berhasil mendisplay data.');
    }
}
