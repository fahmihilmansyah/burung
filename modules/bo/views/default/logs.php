<?php use yii\helpers\Url; ?>
<div class="page-header">
    <div class="row align-items-end">
        <div class="col-lg-8">
            <div class="page-header-title">
                <div class="d-inline">
                    <h4>Manajemen Logs - Daftar Logs</h4>
                    <span>Daftar data log.</span>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="page-header-breadcrumb">
                <ul class="breadcrumb-title">
                    <li class="breadcrumb-item">
                        <a href="<?= Url::toRoute(['default/index']); ?>"><i class="feather icon-home"></i> </a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="#!">Daftar data log</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="card">
    <!-- CARD HEADER -->
    <div class="card-block">
        <div class="dt-responsive table-responsive">
            <table id="dtLogs" class="table table-striped table-bordered nowrap mytable" load-url="<?= Url::toRoute(['log/draw-table']); ?>">
                <thead>
                    <tr>
                        <th>Controller</th>
                        <th>Topic</th>
                        <th>IP</th>
                        <th>Agent</th>
                        <th>Activity</th>
                        <th>Reason</th>
                        <th>Status</th>
                        <th>User ID</th>
                        <th>User Application</th>
                        <th>Created At</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>Controller</th>
                        <th>Topic</th>
                        <th>IP</th>
                        <th>Agent</th>
                        <th>Activity</th>
                        <th>Reason</th>
                        <th>Status</th>
                        <th>User ID</th>
                        <th>User Application</th>
                        <th>Created At</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>

<?php 
    $jsfiles = [
        'js/logs/index.js',
    ];

    foreach ($jsfiles as $key => $file) {
        $this->registerJsFile('@web/'.$file, ['position' => \yii\web\View::POS_END]);
    }
?>
