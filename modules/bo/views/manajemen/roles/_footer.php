<?php 
    $jsfiles = [
        'js/role/index.js',
    ];

    foreach ($jsfiles as $key => $file) {
        $this->registerJsFile('@web/'.$file, ['position' => \yii\web\View::POS_END]);
    }
?>
