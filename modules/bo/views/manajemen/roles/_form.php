<?php 
    $jsfiles = [
        'js/treetable/treeTable.js',
        'js/role/form.js',
    ];

    $cssfiles = [
        'js/treetable/tree-table.min.css',
    ];

    foreach ($jsfiles as $key => $file) {
        $this->registerJsFile('@web/'.$file, ['position' => \yii\web\View::POS_END]);
    }

    foreach ($cssfiles as $key => $file) {
        $this->registerCssFile('@web/'.$file, ['position' => \yii\web\View::POS_HEAD]);
    }
?>
