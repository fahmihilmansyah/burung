<?php 
    use yii\helpers\Url;
    use yii\widgets\ActiveForm;
    use yii\helpers\Html;
?>
<div class="page-header">
    <div class="row align-items-end">
        <div class="col-lg-8">
            <div class="page-header-title">
                <div class="d-inline">
                    <h4>Manajemen Roles - Add Role</h4>
                    <span>Form tambah data role business operational.</span>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="page-header-breadcrumb">
                <ul class="breadcrumb-title">
                    <li class="breadcrumb-item">
                        <a href="<?= Url::toRoute(['default/index']); ?>"><i class="feather icon-home"></i> </a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="<?= Url::toRoute(['roles/index']); ?>">Daftar data role</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="#!">Tambah data role</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- BEGIN: FORM -->
<div class="card">
    <div class="card-block">
        <?php 
            $form = ActiveForm::begin([
                'method' =>
                'POST', 'id' => 'formRole', 
                'options' => ['class' => 'form-horizontal'],
            ]); 
        ?>
        <div class="row">
            <div class="col-md-8 mx-auto">
                <?php 
                    if(isset($id)){
                ?>
                    <input id="hiddenId" type="hidden" name="id" value="<?= $id ?? ''; ?>" />
                <?php
                    }
                ?>
                <div class="form-group">
                    <label for="name" class="label-input">Role Name</label>
                    <input type="text" 
                            class="form-control" 
                            name="name" 
                            value="<?= $role->name ?? ''; ?>" 
                            placeholder="Input role name kamu disini"
                        />
                    <div class="error-wrapper"></div>
                </div>

                <div class="form-group">
                    <label for="email" class="label-input">Description</label>
                    <textarea type="text" 
                            class="form-control" 
                            name="description" 
                            value="<?= $role->description ?? ''; ?>"
                            placeholder="Input description kamu disini"
                    ><?= $role->description ?? ''; ?></textarea>
                    <div class="error-wrapper"></div>
                </div>
            </div>
        </div>
        <div class="spacer"></div>

        <div class="row">
            <div class="col-md-8 mx-auto">
                <button type="button" class="btn btn-sm btn-primary" id="checkall">Check All</button>
                <button type="button" class="btn btn-sm btn-danger" id="uncheckall">Uncheck All</button>
            </div>
        </div>

        <div class="row">
            <div class="col-md-8 mx-auto">
                <div class="dt-responsive table-responsive">
                    <table id="dtMenus" class="table table-striped table-bordered nowrap mytable" load-url="<?= Url::toRoute(['roles/draw-menus-role-table']); ?>">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Path</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>

                <div class="form-group" style="text-align:center">
                    <a 
                        href="<?= Url::toRoute(['roles/index']); ?>"
                        class="float-right btn btn-danger" 
                        style="font-size:18px; margin-left:10px;">
                        <i class="fa fa-remove"></i> Batal
                    </a>

                    <?php 
                        $action = Url::toRoute(['roles/save']);
                        if(isset($id)){
                            $action = Url::toRoute(['roles/update']);
                        }
                    ?>
                    <button 
                            id="rolesSave"
                            type="button" 
                            class="float-right btn btn-primary" 
                            style="font-size:18px;"
                            onclick="save('<?= $action; ?>')">
                        <i class="fa fa-save"></i> <span class="rolesSave">Simpan</span>
                    </button>
                </div>
            </div>
        </div>
        <?php ActiveForm::end() ?>
    </div>
</div>
<!-- END: FORM -->

<?= $this->context->renderPartial('@app/modules/bo/views/manajemen/roles/_form'); ?>