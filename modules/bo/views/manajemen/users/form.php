<?php 
    use yii\helpers\Url;
    use yii\widgets\ActiveForm;
    use yii\helpers\Html;
?>
<div class="page-header">
    <div class="row align-items-end">
        <div class="col-lg-8">
            <div class="page-header-title">
                <div class="d-inline">
                    <h4>Manajemen Users - Add User</h4>
                    <span>Form tambah data user business operational.</span>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="page-header-breadcrumb">
                <ul class="breadcrumb-title">
                    <li class="breadcrumb-item">
                        <a href="<?= Url::toRoute(['default/index']); ?>"><i class="feather icon-home"></i> </a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="<?= Url::toRoute(['users/index']); ?>">Daftar data user</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="#!">Tambah data user</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- BEGIN: FORM -->
<div class="card">
    <div class="card-block">
        <div class="row">
            <div class="col-md-8 mx-auto">
            <?php 
                $form = ActiveForm::begin([
                    'method' =>
                    'POST', 'id' => 'formUser', 
                    'options' => ['class' => 'form-horizontal'],
                ]); 
            ?>
            
            <?php 
                if(isset($id)){
            ?>
                <input id="hiddenId" type="hidden" name="id" value="<?= $id ?? ''; ?>" />
            <?php
                }
            ?>
            <div class="form-group">
                <label for="name" class="label-input">Nama Lengkap</label>
                <input type="text" 
                        class="form-control" 
                        name="name" 
                        value="<?= $user->name ?? ''; ?>" 
                        placeholder="Input nama lengkap kamu disini"
                    />
                <div class="error-wrapper"></div>
            </div>

            <div class="form-group">
                <label for="email" class="label-input">Email</label>
                <input type="text" 
                        class="form-control" 
                        name="email" 
                        value="<?= $user->email ?? ''; ?>"
                        placeholder="Input email kamu disini"
                        <?= isset($id) ? 'readonly' : ''; ?>
                    />
                <div class="error-wrapper"></div>
            </div>

            <div class="form-group">
                <label for="role">Role</label>
                <?php echo Html::dropDownList('role_id',
                                                $user->role_id ?? '', 
                                                $roleOptions, 
                                                [
                                                    'prompt' => '--- Pilih Role ---',
                                                    'class' => 'form-control roledropdown',
                                                ]); 
                                            ?>
                <div class="error-wrapper"></div>
            </div>

            <div class="form-group" style="text-align:center">
                <a 
                    href="<?= Url::toRoute(['users/index']); ?>"
                    class="float-right btn btn-danger" 
                    style="font-size:18px; margin-left:10px;">
                    <i class="fa fa-remove"></i> Batal
                </a>

                <?php 
                    $action = Url::toRoute(['users/save']);
                    if(isset($id)){
                        $action = Url::toRoute(['users/update']);
                    }
                ?>
                <button 
                        id="usersSave"
                        type="button" 
                        class="float-right btn btn-primary" 
                        style="font-size:18px;"
                        onclick="save('<?= $action; ?>')">
                    <i class="fa fa-save"></i> Simpan
                </button>
            </div>

            <?php ActiveForm::end() ?>
            </div>
        </div>
    </div>
</div>
<!-- END: FORM -->

<?= $this->context->renderPartial('@app/modules/bo/views/manajemen/users/_footer'); ?>