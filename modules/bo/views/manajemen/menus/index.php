<?php use yii\helpers\Url; ?>
<div class="page-header">
    <div class="row align-items-end">
        <div class="col-lg-8">
            <div class="page-header-title">
                <div class="d-inline">
                    <h4>Manajemen Menus - Daftar Menu</h4>
                    <span>Daftar data menu.</span>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="page-header-breadcrumb">
                <ul class="breadcrumb-title">
                    <li class="breadcrumb-item">
                        <a href="<?= Url::toRoute(['default/index']); ?>"><i class="feather icon-home"></i> </a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="#!">Daftar data menu</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="card">
    <!-- CARD HEADER -->
    <div class="card-block">
        <div>
            <a href="<?= Url::toRoute(['menus/create']); ?>" class="btn btn-mat btn-primary"><i class="icofont icofont-chart-flow"></i> Tambah Menu</a>
        </div>
        <div class="spacer"></div>
        <div class="dt-responsive table-responsive">
            <table id="dtMenus" class="table table-striped table-bordered nowrap mytable" load-url="<?= Url::toRoute(['menus/draw-table']); ?>">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Path</th>
                        <th>Icon</th>
                        <th>Created at</th>
                        <th>Updated at</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>Name</th>
                        <th>Path</th>
                        <th>Icon</th>
                        <th>Created at</th>
                        <th>Updated at</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>

<?= $this->context->renderPartial('@app/modules/bo/views/manajemen/menus/_footer'); ?>