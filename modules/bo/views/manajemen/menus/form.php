<?php 
    use yii\helpers\Url;
    use yii\widgets\ActiveForm;
    use yii\helpers\Html;
?>
<div class="page-header">
    <div class="row align-items-end">
        <div class="col-lg-8">
            <div class="page-header-title">
                <div class="d-inline">
                    <h4>Manajemen Menus - Add Menu</h4>
                    <span>Form tambah data menu.</span>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="page-header-breadcrumb">
                <ul class="breadcrumb-title">
                    <li class="breadcrumb-item">
                        <a href="<?= Url::toRoute(['default/index']); ?>"><i class="feather icon-home"></i> </a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="<?= Url::toRoute(['users/index']); ?>">Daftar data menu</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="#!">Tambah data menu</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- BEGIN: FORM -->
<div class="card">
    <div class="card-block">
        <div class="row">
            <div class="col-md-8 mx-auto">
            <?php 
                $form = ActiveForm::begin([
                    'method' =>
                    'POST', 'id' => 'formMenu', 
                    'options' => ['class' => 'form-horizontal'],
                ]); 
            ?>
            
            <?php 
                if(isset($id)){
            ?>
                <input id="hiddenId" type="hidden" name="id" value="<?= $id ?? ''; ?>" />
            <?php
                }
            ?>
            <div class="form-group">
                <label for="name" class="label-input">Nama Menu</label>
                <input type="text" 
                        class="form-control" 
                        name="name" 
                        value="<?= $menu->name ?? ''; ?>" 
                        placeholder="Input nama menu kamu disini"
                    />
                <div class="error-wrapper"></div>
            </div>

            <div class="form-group">
                <label for="email" class="label-input">Path</label>
                <input type="text" 
                        class="form-control" 
                        name="path" 
                        value="<?= $menu->path ?? ''; ?>"
                        placeholder="Input path menu kamu disini"
                    />
                <div class="error-wrapper"></div>
            </div>

            <div class="form-group">
                <label for="email" class="label-input pickicon">
                    Icon
                    <div id="display-icon" class="list-icon">
                        <i class="feather <?= isset($menu->icon) ? $menu->icon :  'icon-alert-octagon'; ?>"></i>
                    </div>
                </label>
                <?php echo Html::dropDownList('icon',
                                                $menu->icon ?? '', 
                                                $iconOptions, 
                                                [
                                                    'prompt' => 'Pilih Icon',
                                                    'class' => 'icondropdown',
                                                ]); 
                                            ?>
                <div class="error-wrapper"></div>
            </div>

            <div class="form-group">
                <label for="role">Menu Parent</label>
                <?php echo Html::dropDownList('parent',
                                                $menu->role_id ?? '', 
                                                $menuOptions, 
                                                [
                                                    'prompt' => 'Pilih Menu',
                                                    'class' => 'form-control roledropdown',
                                                ]); 
                                            ?>
                <div class="error-wrapper"></div>
            </div>

            <div class="form-group" style="text-align:center">
                <a 
                    href="<?= Url::toRoute(['menus/index']); ?>"
                    class="float-right btn btn-danger" 
                    style="font-size:18px; margin-left:10px;">
                    <i class="fa fa-remove"></i> Batal
                </a>

                <?php 
                    $action = Url::toRoute(['menus/save']);
                    if(isset($id)){
                        $action = Url::toRoute(['menus/update']);
                    }
                ?>
                <button 
                        id="menusSave"
                        type="button" 
                        class="float-right btn btn-primary" 
                        style="font-size:18px;"
                        onclick="save('<?= $action; ?>')">
                    <i class="fa fa-save"></i> Simpan
                </button>
            </div>

            <?php ActiveForm::end() ?>
            </div>
        </div>
    </div>
</div>
<!-- END: FORM -->

<?= $this->context->renderPartial('@app/modules/bo/views/manajemen/menus/_footer'); ?>