<?php 
    use yii\helpers\Url;
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;
?>
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <?php 
                $form = ActiveForm::begin([
                    'method' => 'POST',
                    'id' => 'formSignin',
                    'options' => ['class' => 'md-float-material form-material'],
                ]);
            ?>
                <div class="text-center">
                </div>
                <div class="auth-box card">
                    <div class="card-block">
                        <div class="row m-b-20">
                            <div class="col-md-12 signintitle">
                                <h5 class="login">Gacor Admin</h5>
                                <p>Masukan email dan password yang anda miliki.</p>
                            </div>
                        </div>
                        <div class="form-group form-primary">
                            <input type="text" name="email" class="form-control myinput" required="" placeholder="Your Email Address">
                            <span class="form-bar"></span>
                            <div class="error-wrapper"></div>
                        </div>
                        <div class="form-group form-primary">
                            <input type="password" name="password" class="form-control myinput" required="" placeholder="Password">
                            <span class="form-bar"></span>
                            <div class="error-wrapper"></div>
                        </div>
                        <div class="row m-t-25 text-left">
                            <div class="col-12">
                                <div class="checkbox-fade fade-in-primary d-">
                                    <label>
                                        <input type="checkbox" value="">
                                        <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i></span>
                                        <span class="text-inverse">Remember me</span>
                                    </label>
                                </div>
                                <div class="forgot-phone text-right f-right">
                                    <a href="auth-reset-password.htm" class="text-right f-w-600"> Forgot Password?</a>
                                </div>
                            </div>
                        </div>
                        <div class="row m-t-30">
                            <div class="col-md-12">
                                <button id="btnSignin" type="button" onclick="doSignin('<?= Url::toRoute(['auth/do-signin']); ?>','formSignin')" class="btn btn-primary btn-md btn-block waves-effect waves-light text-center m-b-20">Sign in</button>
                            </div>
                        </div>
                    </div>
                </div>
            <?php ActiveForm::end() ?>
        </div>
    </div>
</div>