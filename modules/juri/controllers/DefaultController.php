<?php

namespace app\modules\juri\controllers;

use Yii;
use app\controllers\BaseController as Controller;
use yii\filters\AccessControl;
use thamtech\uuid\helpers\UuidHelper;
use app\modules\eo\models\Event;
use app\modules\eo\models\EventPenilaian;
use app\modules\eo\models\Penilaian;
use app\modules\eo\models\Gantungan;
use app\modules\eo\models\TypePenilaian;
use app\modules\eo\models\TypeGantungan;
use app\modules\eo\models\JuriAssignment;

/**
 * Default controller for the `eo` module
 */
class DefaultController extends Controller
{
    public $layout = '@app/views/layouts/app.php';

    public $header;
    public $headerText;
    public $menu = '@app/modules/eo/views/default/menu/_mainmenu';

    public $backLink = 'default/event';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'user' => 'userjuri',
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
    * List Section
    * ::Start
    */
    public function actionIndex()
    {
        $this->header = '@app/modules/juri/views/default/menu/_backheader';
        $this->headerText = "Penilaian";
        $this->menu = '@app/modules/juri/views/default/menu/_mainmenu';
        $this->backLink = 'default/index';

        $juri_id = \Yii::$app->userjuri->identity->getId() ?? null;
        $eventIds = JuriAssignment::find()
                            ->select('event_id')
                            ->distinct()
                            ->where(['juri_id' => $juri_id])
                            ->all();

        $in = [];
        foreach ($eventIds as $key => $eventId) {
            $in[] = $eventId->event_id;
        }

          
        $model['events'] = Event::find()
                                ->where(['in','id', $in])
                                ->all();
        
        return $this->render('@app/modules/juri/views/penilaian/lists', $model);
    }

    public function actionListsGantungan(){
        $this->header = '@app/modules/juri/views/default/menu/_backheader';
        $this->headerText = "Penilaian";
        $this->menu = '@app/modules/juri/views/default/menu/_mainmenu';

        $this->backLink = 'default/index';
        
        $event_id = \Yii::$app->request->get()['id'] ?? null;
        $juri_id = \Yii::$app->userjuri->identity->getId() ?? null;

        $event = Event::find()->where(['id' => $event_id])->one();
        
        $tipeGantungan = TypeGantungan::find()->where(['id' => $event->type_gantungan_id])->one();
        
        $col = $tipeGantungan->jumlah_col ?? 0;
        $row = $tipeGantungan->jumlah_row ?? 0;
        $type_gnt = $tipeGantungan->type_gnt ?? 0;
        
        $length = $row * $col;
        $chunk = $col;
        $loops = $this->snakeLoop($length, $chunk, $type_gnt);
        
        $gantungans = [];

        if(!empty($loops)){
            foreach ($loops as $k1 => $loop) {
                foreach ($loop as $k2  => $value) {
                    $_gantungan = Gantungan::find()
                                            ->where([
                                                    'event_id' => $event_id,
                                                    'no_gnt' => $value,
                                            ]);

                    $gantunganData = $_gantungan->one();

                    if(!empty($gantunganData)){
                        $_assignment = JuriAssignment::find()
                                                        ->where([
                                                            'event_id' => $event_id,
                                                            'gnt_id' => $gantunganData->id,
                                                            'juri_id' => $juri_id
                                                        ]);

                        $canAssignValue = ($_assignment->count() > 0);
                                                        
                        $gantungans[$k1][$k2]['id'] = $gantunganData->id;
                        $gantungans[$k1][$k2]['no_gnt'] = $gantunganData->no_gnt;
                        $gantungans[$k1][$k2]['event_id'] = $gantunganData->event_id;

                        $totalPoint = Penilaian::find()
                                    ->where([
                                        'gantungan_id' => $gantunganData->id,
                                        'juri_id' => $juri_id
                                    ])
                                    ->sum('nilai');

                        $gantungans[$k1][$k2]['totalPoint'] = $totalPoint ?? 0;
                        $gantungans[$k1][$k2]['canassign'] = $canAssignValue;
                    }
                }
            }
        }
        
        $model['gantungans'] = $gantungans;
        $model['column'] = $col;
        $model['tipePenilaian'] = EventPenilaian::find()
                                            ->where(['event_id' => $event_id])
                                            ->all();
        
        return $this->render('@app/modules/juri/views/penilaian/index', $model);
    }

    public function actionNilai(){
        $request = Yii::$app->request->post();
        $juri_id = \Yii::$app->userjuri->identity->getId() ?? null;
        $event_id = $request['event'] ?? null;

        Yii::$app->timeZone = 'Asia/Jakarta';

        $transaction = Yii::$app->db->beginTransaction();
        $nilai = TypePenilaian::findOne(['id' => $request['tipePenilaian']]);

        $callback = [];

        try {
            $penilaian = new Penilaian();
            $id = UuidHelper::uuid();
            $penilaian->id = $id;
            $penilaian->gantungan_id = $request['gantungan'] ?? null;
            $penilaian->type_nilai = $request['tipePenilaian'] ?? null;
            $penilaian->nilai = $nilai->nilai ?? null;
            $penilaian->juri_id = $juri_id ?? null;
            $penilaian->event_id = $event_id;
            $penilaian->timestmp = strtotime(date('Y-m-d H:i:s'));
            $penilaian->created_ad = date('Y-m-d H:i:s');

            if(!$penilaian->save()){
                throw Exception('Unable to save record.');
            }
            $transaction->commit();

            $callback['gantungan_id'] = $request['gantungan'];
            $callback['totalpoint'] = Penilaian::find()
                                        ->where([
                                            'gantungan_id' => $request['gantungan'],
                                            'juri_id' => $juri_id
                                        ])
                                        ->sum('nilai');
        } catch (\Throwable $th) {
            $transaction->rollback();
            return $this->asJsonError([$th->getMessage()],'[Error] Save data...');
        }
        return $this->asJsonSuccess($callback,'[Success] Save data...');
    }
     /**
      * List Section
      * ::End
      */

    private function snakeLoop($snakeLength, $chunk, $type = 'ASC'){
        function createArrayLoop($number){
            $temp = [];
            for ($i=1; $i <= $number ; $i++) { 
                $temp[$i] = $i;
            }

            return $temp;
        }

        $array = createArrayLoop($snakeLength);
        $newArrays = array_chunk($array, $chunk);
        
        if($type == 'DESC'){
            foreach ($newArrays as $key => $newArray) {
                $newArrays[$key] = array_reverse($newArray, true);
            }
        } else if($type === 'ASCSLINE'){
            foreach ($newArrays as $key => $newArray) {
                if(($key + 1) % 2 == 0){
                    $newArrays[$key] = array_reverse($newArray, true);
                }
            }
        } else if($type === 'DESCSLINE'){
            foreach ($newArrays as $key => $newArray) {
                $newArrays[$key] = array_reverse($newArray, true);
            }
            
            foreach ($newArrays as $key => $newArray) {
                if(($key + 1) % 2 == 0){
                    $newArrays[$key] = array_reverse($newArray, true);
                }
            }
        }

        return $newArrays;
    }

    public function actionSummaryPenilaian(){
        $this->layout = '@app/views/layouts/top.php';
        $this->header = '@app/modules/juri/views/default/menu/_backheader';
        $this->headerText = "Perolehan Nilai";
        $this->backLink = 'default/index';

        $event_id = Yii::$app->request->get()['id'] ?? null;
        $query = 'SELECT
                        g.id,
                        g.no_gnt,
                        IFNULL(g_score.total_nilai, 0) as total_nilai,
                        g.event_id
                    FROM
                        gantungan g
                    LEFT JOIN (
                        SELECT
                            gantungan_id,
                            SUM(nilai) as total_nilai,
                            event_id
                        FROM
                            penilaian
                        GROUP BY
                            gantungan_id,
                            event_id) as g_score ON
                        g_score.gantungan_id = g.id
                    WHERE g.event_id=:event_id
                    ORDER BY
                        g_score.total_nilai DESC';
        $lists= Yii::$app->db
                        ->createCommand($query)
                        ->bindValue('event_id',$event_id)
                        ->queryAll();
          
        $data = [];
        foreach ($lists as $list) {
            $data[$list['id']]['id'] = $list['id'];
            $data[$list['id']]['no_gnt'] = $list['no_gnt'];
            $data[$list['id']]['total_nilai'] = $list['total_nilai'];
            $data[$list['id']]['event_id'] = $list['event_id'];   
            
            $tipePenilaian = EventPenilaian::find()->where(['event_id' => $list['event_id']])->all();

            $detailNilai = [];
            foreach ($tipePenilaian as $v) {
                $detailNilai[$v->type_penilaian_id]['type_nilai_id'] = $v->type_penilaian_id ?? '';
                $detailNilai[$v->type_penilaian_id]['nilai_name'] = $v->penilaian->nama_type ?? '';
                
                $point = $this->getDetailNilai($list['id'],$list['event_id'],$v->type_penilaian_id);
                $detailNilai[$v->type_penilaian_id]['point'] = !empty($point) ? $point['total_nilai'] : 0;
            }
            
            $data[$list['id']]['event_id'] = $detailNilai;   
        }

        $model['perolehans'] = $data;

        // echo '<pre>';
        // var_dump($model);
        // exit();

        return $this->render('@app/modules/juri/views/default/perolehan', $model);
    }

    private function getDetailNilai($id,$event_id, $type_nilai){
        $query = 'SELECT
            gantungan_id,
            SUM(penilaian.nilai) as total_nilai,
            event_id,
            type_nilai,
            tp.nama_type
        FROM
            penilaian
        JOIN type_penilaian tp ON
            tp.id = penilaian.type_nilai
        WHERE
            penilaian.gantungan_id =:gantungan_id
            AND penilaian.event_id =:event_id
            AND penilaian.type_nilai =:type_nilai
        GROUP BY
            gantungan_id,
            event_id,
            type_nilai';

        $detailPenilaian = Yii::$app->db
                        ->createCommand($query)
                        ->bindValues([
                            'gantungan_id' => $id,
                            'event_id' => $event_id,
                            'type_nilai' => $type_nilai
                        ])
                        ->queryOne();

        return $detailPenilaian;
    }
}