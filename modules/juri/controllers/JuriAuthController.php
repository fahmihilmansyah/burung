<?php

namespace app\modules\juri\controllers;

use Yii;
use app\controllers\BaseController as Controller;
use yii\web\Response;
use yii\helpers\Url;
use app\helpers\MyLibs;
use yii\widgets\ActiveForm;
use app\modules\juri\models\UserJuri;
use app\modules\juri\models\UserJuriLogin as Login;
use yii\filters\AccessControl;

/**
 * Eo Auth controller for the `eo` module
 */
class JuriAuthController extends Controller
{
    public $layout = '@app/views/layouts/app.php';

    public $header;
    public $headerText;
    public $menu = '@app/modules/eo/views/default/menu/_mainmenu';

    public $backLink = 'default/juri';

    /**
     * Login Display Section
     * ::Start
     */

    //  Login Page
    public function actionLogin()
    {
        $this->checkGuest();

        $this->layout = '@app/views/layouts/plain.php';
        
        return $this->render('@app/modules/juri/views/auth/login');
    }

    public function actionDoSignin(){
        $this->checkGuest();
        
        $model = new Login();
        $model->setAttributes(Yii::$app->request->post());

        $authenticate = $model->authenticate();

        if(!$authenticate['status']){
            return $this->asJsonError(['callback' => null], $authenticate['message']);
        }
       
        return $this->asJsonSuccess(['callback' => Url::home(true).'juri'],'[Success] Save data...');
    }
    /**
     * Login Display Section
     * ::End
     */

    public function actionLogout(){
        $this->checkGuest();

        \Yii::$app->userjuri->logout(true);

        $this->redirect(['juri-auth/login']);
    }

    public function actionNotFound()
    {
        $this->layout = '@app/views/layouts/top.php';
        $this->header = '//default/_registerheader';
        $this->headerText = 'Halaman Tidak Ditemukan';

        return $this->render('notfound');
    }

    public function checkGuest(){
        if (!\Yii::$app->userjuri->isGuest) return $this->redirect(['default/index']);
    }
}