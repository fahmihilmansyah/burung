<?php

namespace app\modules\juri\models;
use Yii;
use yii\base\Model;
use app\modules\juri\models\UserJuri;

class UserJuriLogin extends Model
{
	public $username;
	public $password;
	public $rememberMe = true;

	public $userData = false;
	
	public function rules()
    {
        return [
            // username and password are both required
            [['username', 'password'], 'required'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'juri';
    }

	public function authenticate()
    {
		$result = [];
		
        if ($this->validate()) {
			$dataUser = $this->getUser();

			if(!$dataUser){
				$result['status'] = false;
				$result['message'] = 'Anda tidak terdaftar pada sistem kami.';

				return $result;
			}

			if(!UserJuri::validateLoginCode($this->username, $this->password)){
				$result['status'] = false;
				$result['message'] = 'Password yang anda masukan salah.';

				return $result;
			}

            Yii::$app->userjuri->login($this->getUser(), ($this->rememberMe ? 3600*24*30 : 0));

			$result['status'] = true;
			$result['message'] = 'Login berhasil.';
			return $result;
        }

		$result['status'] = false;
		$result['message'] = 'Login gagal, inputan tidak diisi.';

        return $result;
    }

	public function getUser()
	{
		return !$this->userData ? UserJuri::findByEmailAndCode($this->username, $this->password): false;
	}
}