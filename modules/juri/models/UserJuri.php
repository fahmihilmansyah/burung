<?php

namespace app\modules\juri\models;
use yii\base\NotSupportedException;
use yii\db\ActiveRecord;
use yii\helpers\Security;
use yii\web\IdentityInterface;
use yii\behaviors\TimestampBehavior;

class UserJuri extends \yii\db\ActiveRecord  implements IdentityInterface
{
    public $auth_key;
    public $kode_juri;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'juri';
    }
 
    public static function findIdentity($id){
        return static::findOne($id);
    }

    public static function findIdentityByAccessToken($token, $type = null){
        throw new NotSupportedException('Not have access token field');
    }

    public function getId(){
        return $this->id;
    }

    public function getEventId(){
        return $this->event_id;
    }

    public function getAuthKey(){
        return $this->auth_key;
    }

    public function validateAuthKey($authKey){
        return $this->getAuthKey() === $authKey;
    }

    public static function findByUsername($username){
        return self::findOne(['username'=>$username,'is_active' => 1]);
    }

    public static function findByEmailAndCode($email, $code){
        return self::findOne(['email_juri'=>$email,'kode_juri' => $code]);
    }

    public function validatePassword($password){
        return Yii::$app->getSecurity()->validatePassword($password, $this->password);
    }

    public function validateLoginCode($username, $loginCode){
        $result = self::find()
                        ->where([
                            'email_juri' => $username, 
                            'kode_juri' => $loginCode
                        ])
                        ->count();

        if($result > 0){
            return true;
        }
        return false;
    }
}