<?php 
    use yii\helpers\Url;
    use app\modules\eo\models\JuriAssignment;
    use app\modules\eo\models\EventPenilaian;
    
    $this->title = 'GacorApp';
?>
<div class="AppEvent">
    <?php if (Yii::$app->session->hasFlash('success')): ?>
        <div class="alert alert-success alert-dismissable">
            <h4 style="font-size:11px"><i class="uil uil-check"></i>Success!</h4>
            <?= Yii::$app->session->getFlash('success') ?>
        </div>
    <?php endif; ?>

    <?php if (Yii::$app->session->hasFlash('error')): ?>
        <div class="alert alert-danger alert-dismissable">
            <h4 style="font-size:11px"><i class="uil uil-check"></i>Error!</h4>
            <?= Yii::$app->session->getFlash('error') ?>
        </div>
    <?php endif; ?>

    <?php 
        if(!empty($perolehans)){
            $i = 1;
            foreach ($perolehans as $key => $perolehan) {
    ?>
            <div class="AppGntInformation">
                <div class="body">
                    <div class="row">
                        <div class="col-xs-12">
                            <h5>#<?= $i?></h5>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6 header">
                            <p>Nomor Gantungan</p>
                            <h5><?= $perolehan['no_gnt']?? ''; ?></h5>
                        </div>
                        <div class="col-xs-6">
                            <div class="pointText">
                                <span>Total Point: <?= $perolehan['total_nilai']?? '0'; ?></span>
                            </div>
                            <?php 
                                foreach ($perolehan['event_id'] as $v) {
                            ?>
                                <div class="pointText">
                                    <span><?= $v['nilai_name']?? ''; ?> : <?= $v['point']?? '0'; ?></span>
                                </div>
                            <?php
                                }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
    <?php
                $i++;
            }
        }else{
    ?>
           <div class="AppCard">
                No Data
            </div>
    <?php 
        }
    ?>
</div>

<script>
</script>