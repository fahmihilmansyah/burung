<?php 
    use yii\helpers\Url;
    
    $this->title = 'GacorApp';
?>

<div class="AppSection">
    <div class="container-fluid">
        <div class="AppApplication paddingcard">
            <div class="AppApplication__list">
                <a href="<?= Url::toRoute(['default/event']); ?>">
                    <i class="uil uil-rocket"></i>
                    <p>Event</p>
                </a>
                <a href="<?= Url::toRoute(['juri/juri']); ?>">
                    <i class="uil uil-user-check"></i>
                    <p>Juri</p>
                </a>
                <a href="">
                    <i class="uil uil-map"></i>
                    <p>Other Menu</p>
                </a>
            </div>
        </div>
    </div>
</div>