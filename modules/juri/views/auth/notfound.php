<?php 
use yii\helpers\Url;
?>
<div class="AppNotfound">
    <div class="AppNotfound__box">
    
        <?php $img = Url::to('@web/images/notfound.svg');     ?>
        <img src="<?= $img; ?>" alt="notfound"/>
    </div>
    <p>Maaf, Halaman yang anda cari tidak kami temukan.</p>
</div>