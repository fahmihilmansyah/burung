<?php

/* @var $this yii\web\View */
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
$this->title = 'Gacor App - Halaman Login Juri';
?>

<!-- INFORMATION CARD -->
<div class="AppLogin">
    <?php $img = Url::to('@web/images/birdlogo.png');     ?>

    <div class="AppLogin__image">
        <img src="<?= $img; ?>" alt="image">
    </div>

    <h5>Login</h5>
    <h2 style="text-align:center;margin-bottom:15px;">Juri Penilaian</h2>
    <h4>Untuk masuk kedalam aplikasi, <br/> masukan Email & Password kamu.</h4>
    <div class="AppLogin__form">
        <?php 
            $form = ActiveForm::begin([
                'method' => 'POST',
                'id' => 'formLoginJuri',
                'options' => ['class' => 'form-horizontal'],
            ]);
        ?>
            <div>
                <div class="AppInputGroup">
                    <i class="uil uil-envelope"></i>
                    <input type="text" class="form-control AppInputText" placeholder="Ketikan email kamu disini" name="username" >
                    <div class="error-wrapper"></div>
                </div>
            </div>
            <br/>
            <div>
                <div class="AppInputGroup">
                    <i class="uil uil-padlock"></i>
                    <input type="password" class="form-control AppInputText" placeholder="Ketikan password kamu disini" name="password">
                    <div class="error-wrapper"></div>
                </div>
            </div>
            <br/>
            <div>
                <label>
                    <input type="checkbox" name="rememberme"><span>Ingat saya</span>
                </label>
            </div>
            <div style="text-align:center; padding:10px 0 10px 0;">
                <button type="button" class="btn AppButton--primary AppButton--login" onclick="doSignin('<?php echo Url::toRoute(['juri-auth/do-signin']); ?>','formLoginJuri')">
                    <i class="uil uil-message"></i> Login
                </button>
            </div>
        <?php ActiveForm::end() ?>
    </div>
    <br/>
</div>

<?php $this->registerJsFile('js/login.js', ['depends' => [yii\web\JqueryAsset::className()]]); ?> 