<?php 
    use yii\helpers\Url; 
?>
<div class="container-fluid">
    <div class=AppGantungan>
        <div class="spacer"></div>
        <h5>Daftar Gantungan</h5>
        <div id="gantungan" class="gantungan" column="<?= $column; ?>">
            <?php 
                foreach ($gantungans as $key => $gantungan) {
                    echo '<div class="owl-carousel">';
                    foreach ($gantungan as $key => $record) {
                        if($record['canassign']){
            ?>
                    <div class="gantunganCol <?= ($record['canassign']) ? 'assignValue' : ''; ?>" gntId="<?= $record['id']; ?>" evId="<?= $record['event_id']; ?>">
                        <p><?= $record['no_gnt']; ?></p>
                        <div class="totalpoint">
                            <span>total point</span>
                            <span id="point-<?= $record['id']; ?>" class="point"><?= $record['totalPoint']; ?></span>
                        </div>
                    </div>
            <?php
                        }
                    }
                    echo '</div>';
                }
            ?>
        </div>
        <div class="modalGantungan">
            <div class="modalGantunganWrapper">
                <div class="gantunganTarget">
                    <?php 
                        if($tipePenilaian){
                            foreach ($tipePenilaian as $key => $value) {
                    ?>
                            <div class="btn" onclick="sendPenilaian(this, '<?= Url::toRoute(['default/nilai']); ?>')">
                                <input type="hidden" name="tpid" value="<?= $value->type_penilaian_id; ?>">
                                <?= $value->penilaian->nama_type??'-'; ?>
                            </div>
                    <?php
                            }
                        }else{
                            echo "<p>Jenis penilaian belum diatur</p>";
                        }
                    ?>
                </div>
                <div class="modalGantunganClose" onclick="closePreview()"><i class="uil uil-times"></i></div>
            </div>
        </div>
    </div>
</div>