<?php 
    use yii\helpers\Url;
    use app\modules\eo\models\JuriAssignment;
    use app\modules\eo\models\EventPenilaian;
    
    $this->title = 'GacorApp';
?>

<div class="AppEvent">
    <?php if (Yii::$app->session->hasFlash('success')): ?>
        <div class="alert alert-success alert-dismissable">
            <h4 style="font-size:11px"><i class="uil uil-check"></i>Success!</h4>
            <?= Yii::$app->session->getFlash('success') ?>
        </div>
    <?php endif; ?>

    <?php if (Yii::$app->session->hasFlash('error')): ?>
        <div class="alert alert-danger alert-dismissable">
            <h4 style="font-size:11px"><i class="uil uil-check"></i>Error!</h4>
            <?= Yii::$app->session->getFlash('error') ?>
        </div>
    <?php endif; ?>

    <?php 
        if(!empty($events)){
            foreach ($events as $key => $event) {
    ?>
            <div class="AppCard">
                <div class="body">
                    <div class="row">
                        <div class="col-xs-12">
                            <h5><?= $event->nama_event ?? ''; ?></h5>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <h4>
                                Tangal Event<br/>

                                <?php 
                                    $tgl_event = isset($event->tgl_event) ? date('d M Y',strtotime($event->tgl_event)) : '';
                                ?>
                                <span><?= $tgl_event; ?></span>
                            </h4>
                        </div>
                        <div class="col-xs-6">
                            <h4>Waktu<br/><span><?= $event->waktu ?? ''; ?></span></h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <h4>
                                Jenis Burung<br/>
                                <span>
                                    <?= $event->jenisBurung->name ?? ''; ?>
                                </span>
                            </h4>
                        </div>
                        <div class="col-xs-6">
                            <h4>
                                Kuota Peserta<br/>
                                <span>
                                    <?php 
                                        $jml_row = $event->tipeGantungan->jumlah_row ?? 0;
                                        $jml_col = $event->tipeGantungan->jumlah_col ?? 0;
                                        $kuota = intVal($jml_row) * intVal($jml_col); 
                                    ?>
                                    <?= $kuota ?? ''; ?>
                                </span>
                            </h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-4">
                            <h4>
                                Penilaian Khusus<br/>
                                <span>
                                    <?= ($event->is_khusus === 1) ? 'Ya' : 'Tidak'; ?>
                                </span>
                            </h4>
                        </div>
                    </div>
                </div>
                <div class="footer">
                <?php 
                    $actionJuri = ['add-juri/general','id' => $event->id];
                    if($event->is_khusus === 1)
                        $actionJuri = ['add-juri/spesific','id' => $event->id];
                ?>
                    <a href="<?= Url::toRoute(['penilaian-manual/lists-gantungan', 'id' => $event->id]); ?>" class="btn btn-success">
                        <i class="uil uil-edit"></i> Manual Input
                    </a>
                    <a href="<?= Url::toRoute(['default/lists-gantungan', 'id' => $event->id]); ?>" class="btn btn-primary">
                        <i class="uil uil-edit"></i> Manual Touch
                    </a>
                    <a href="<?= Url::toRoute(['default/summary-penilaian', 'id' => $event->id]); ?>" class="btn btn-warning">
                        <i class="uil uil-edit"></i> Perolehan Nilai
                    </a>
                </div>
            </div>
    <?php
            }
        }else{
    ?>
           <div class="AppCard">
                No Data
            </div>
    <?php 
        }
    ?>
</div>

<script>
</script>