<?php 
    use yii\helpers\Url; 
    use yii\widgets\ActiveForm;
    use yii\helpers\Html;
?>
<div class="container-fluid">
    <div class=AppGantunganManual>
        <div class="spacer"></div>
        <h5>Daftar Gantungan</h5>
        <div id="gantungan" class="gantungan" column="<?= $column; ?>">
            <?php 
                foreach ($gantungans as $key => $gantungan) {
                    echo '<div class="owl-carousel">';
                    foreach ($gantungan as $key => $record) {
                        if($record['canassign']){
            ?>
                    <div class="gantunganCol <?= ($record['canassign']) ? 'assignValue' : ''; ?>" gntId="<?= $record['id']; ?>" evId="<?= $record['event_id']; ?>">
                        <p><?= $record['no_gnt']; ?></p>
                        <div class="totalpoint">
                            <span>total point</span>
                            <span id="point-<?= $record['id']; ?>" class="point"><?= $record['totalPoint']; ?></span>
                        </div>
                    </div>
            <?php
                        }
                    }
                    echo '</div>';
                }
            ?>
        </div>
        <?php 
            $action = Url::toRoute(['penilaian-manual/nilai']); 
            $form = ActiveForm::begin([
                'action' => $action,
                'method' => 'POST', 
                'id' => 'formNilai', 
                'options' => ['class' => 'form-horizontal'],
            ]); 
        ?>
        <div class="modalGantungan">
            <div class="modalGantunganWrapper">
                <div class="gantunganTarget">
                    <?php 
                        if($tipePenilaian){
                            foreach ($tipePenilaian as $key => $value) {
                    ?>
                            <div class="btn">
                                <input type="hidden" name="tpid[<?= $value->type_penilaian_id; ?>][tipePenilaian]" value="<?= $value->type_penilaian_id; ?>">
                                <?= $value->penilaian->nama_type??'-'; ?> 
                                <br/> 
                                <input class="form-control" type="text" name="tpid[<?= $value->type_penilaian_id; ?>][value]" value="0" style="text-align:center"/>
                            </div>
                    <?php
                            }
                        }else{
                            echo "<p>Jenis penilaian belum diatur</p>";
                        }
                    ?>

                    <div class="btn btn-primary btn-simpan" onclick="saveNilai()">
                        Simpan
                    </div>
                </div>
                <div class="modalGantunganClose" onclick="closePreview()"><i class="uil uil-times"></i></div>
            </div>
        </div>
        <?php ActiveForm::end() ?>
    </div>
</div>