<?php

namespace app\helpers;

use Yii;
use app\models\Logs as LogModel;
use thamtech\uuid\helpers\UuidHelper;

class Logs
{
    static function save($data){
        $agent =  Yii::$app->request->getUserAgent();
        $ip =  Yii::$app->request->getUserIP();
        $identity = !empty($data['users']) ? $data['users'] : json_encode(Yii::$app->userbo->identity->attributes);
        $activity = $data['activity'] ?? '';
        $reason = $data['reason'] ?? '';
        $controller = $data['controller'] ?? '';
        $topic = $data['topic'] ?? '';
        $status = $data['status'] ?? '';
        $userId = !empty($data['loginid']) ? $data['loginid'] : (!empty(Yii::$app->userbo->identity) ? Yii::$app->userbo->identity->getId() : '');
        $application = $data['application'] ?? 'JOSBOS';

        $transaction = Yii::$app->db->beginTransaction();
        Yii::$app->timeZone = 'Asia/Jakarta';
        try {
            $log = new LogModel();
            $log->id = UuidHelper::uuid();
            $log->controller = $controller;
            $log->topic = $topic;
            $log->source_ip = $ip;
            $log->browser_agent = $agent;
            $log->activity = $activity;
            $log->reason = $reason;
            $log->status = $status;
            $log->created_ad = date('Y-m-d H:i:s');
            $log->login_data = $identity;
            $log->user_id = $userId;
            $log->user_application = $application;
            $log->save();

            $transaction->commit();
        } catch (\Throwable $th) {
            //throw $th;
            echo '<pre>';
            var_dump($th->getMessage());
            exit();
            $transaction->rollback();
            return false;
        }

        return true;
    }
}
