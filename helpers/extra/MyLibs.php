<?php
/**
 * Created by PhpStorm.
 * Project : mymumuapps
 * User: fahmihilmansyah
 * Date: 30/10/20
 * Email : fahmi.hilmansyah@gmail.com
 * Telp : 0817170820
 * Time: 05.58
 * Dilarang Keras Mengubah atau Mendistribusikan ulang code ini tanpa sepengetahuan.
 */
  namespace app\helpers\extra;
  use app\models\ConfigApi;
  use app\models\LogPpob;
  use app\models\TransaksiIpg;
  use yii\helpers\Url;

  class Mylibs{

//      protected static $urlpa= 'http://localhost/paymentgateway/public/';
      protected static $urlpa= 'http://core.mumuapps.id/';
//      protected static $mkey= 'SECRETKEY12839';
      protected static $mkey= 'DDTMSJDSCRTKY#2021!';
      protected static $appkey= 'DDTMSJTKN2021!123';
      protected static $appkeywalet= 'KEYDDTMSJD2021!';
      protected static $appkeyppob= 'SECMSJD@123#KEY';
      protected static $apptokenppob= 'MSJDAPPS2021';
      public static function uuidv4()
      {
          return time() . "-" . sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',

                  // 32 bits for "time_low"
                  mt_rand(0, 0xffff), mt_rand(0, 0xffff),

                  // 16 bits for "time_mid"
                  mt_rand(0, 0xffff),

                  // 16 bits for "time_hi_and_version",
                  // four most significant bits holds version number 4
                  mt_rand(0, 0x0fff) | 0x4000,

                  // 16 bits, 8 bits for "clk_seq_hi_res",
                  // 8 bits for "clk_seq_low",
                  // two most significant bits holds zero and one for variant DCE1.1
                  mt_rand(0, 0x3fff) | 0x8000,

                  // 48 bits for "node"
                  mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
              );
      }
      static function _signaturecheck($data=[]){
          $result_code = $data['result_code']??'';
          $result_desc = $data['result_desc']??'';
          $invoice = $data['invoice']??'';
          $amount = $data['amount']??'';
          $pay_date = $data['pay_date']??'';
          $ref_no = $data['ref_no']??'';
          $bill = $data['bill']??'';
          $sof_id = $data['sof_id']??'';
          $signature = hash_hmac('sha256', $result_code . '|' . $result_desc . '|' . $invoice . '|' . $amount . '|' . $pay_date . '|' . $ref_no . '|' . $bill . '|' . $sof_id, self::$mkey);
          return $signature;
      }
      static function _curl_exec_ipg($arrParam)
      {
//          $cquery = new Fhhlib();
//          $cariipg = $cquery->custom_query('nb_partner_ipg', ['where' => ['id' => $cquery->db->escape('MUPAYS')]])->result();
          $urlfinpay = self::$urlpa.'trx/request';
          $signature = hash_hmac('sha256', $arrParam['invoice'] . '|' . $arrParam['amount'] . '|' . $arrParam['cust_email'] . '|' . $arrParam['cust_msisdn']  . '|' .$arrParam['sof_id'] , self::$mkey);
          $arrParam['signature'] = $signature;
          $postfields = http_build_query($arrParam);
          $ch = curl_init();
          curl_setopt($ch, CURLOPT_POST, 1);
          curl_setopt($ch, CURLOPT_URL, $urlfinpay);
          curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($ch, CURLOPT_HTTPHEADER, array(
              'Pauthorization: '.self::$appkey
          ));
          $response = curl_exec($ch);
//          print_r($response);exit;
          $err = curl_error($ch);
          curl_close($ch);
          if ($err) {
              echo "cURL Error #:" . $err;
          } else {
              $insrt = new  TransaksiIpg();
              $insrt->id = self::uuidv4();
              $insrt->request_json = json_encode($arrParam);
              $insrt->response_json = $response;
              $insrt->target_table = 'transaksi';
              $insrt->status = 'WAITING';
              $insrt->trxid = $arrParam['invoice'];
              $insrt->created_ad = date('Y-m-d H:i:s');
              $insrt->save();
              $response = ($response);
//            if($response['status_code'] == '00'){
//                return $response['landing_page'];
//            }else{
              return $response;
//            }
              print_r($response);
          }
      }
      public static function changepassword($password='',$tokenlogin=''){
          if(empty($password) || empty($tokenlogin)){
              return false;
          }
          $heaer = ['token-login: ' . $tokenlogin];
         $exe = self::_curl_exexc_walet("mupays/partner/changepass",['password'=>$password],'POST',$heaer);
         $exe = json_decode($exe,true);
         if($exe['rc'] != '0000'){
             return false;
         }
         return true;
      }
      public static function SendMail($data = array())
      {
          try {
              $lsdata['config']['type'] = 'smtp';
              $lsdata['config']['host'] = 'smtp.gmail.com';
              $lsdata['config']['username'] = 'no-reply@ddtekno.com';
              $lsdata['config']['password'] = 'ddtekno92528';
              $lsdata['config']['port'] = '587';
              $lsdata['config']['encryption'] = 'tls';
              $lsdata['fromName'] = $data["senderName"];
              $lsdata['to'] = $data["emailTo"];
              $lsdata['subject'] = $data["subject"];
              $lsdata['html'] = $data["konten"];
              Mylibs::sendemailbyurl($lsdata);
          }catch(\Exception $e){
              print_r($e->getMessage());exit;
          }
      }
      static function sendemailbyurl($postData){
          try {

              $ch = curl_init();

              $curl = curl_init();

              curl_setopt_array($curl, array(
                  CURLOPT_URL => "http://194.31.53.26/kirimemail/public/kirim2",
                  CURLOPT_RETURNTRANSFER => true,
                  CURLOPT_FOLLOWLOCATION => true,
                  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                  CURLOPT_CUSTOMREQUEST => "POST",
                  CURLOPT_POSTFIELDS =>json_encode($postData),
                  CURLOPT_HTTPHEADER => array(
                      "Content-Type: application/json"
                  ),
              ));

              $response = curl_exec($curl);

              curl_close($curl);
//            echo $response;
              return $response;
          }catch (\Exception $e){
              print_r($e->getMessage());
          }

      }
      public static function existsImage($path, $img)
      {
          $dPath = \Yii::getAlias('@webroot/uploads') ;;
          $rpath = $dPath . $path . $img;
          $url_image = Url::to(['/template/img/logomumu-dark.png'],true);
          if(!empty($img))
          {
              if(file_exists($rpath))
              {
                  $url_image = Url::to(['/uploads/'.$path.'/'.$img]);
              }
          }

          return $url_image;
      }
      static function _koneksi_ppob($data = [],$type='INQ',$no_transaksi='')
      {

          /*$signature = hash_hmac('sha256', $data['trxid'] . '|' .
              $data['kode_produk'] . '|' .
              $data['trx_type'] . '|' .
              $data['billNumber'], self::$appkeyppob);*/
          $timestpm = time();
          $signature = hash_hmac('sha256', $data['trxid'] . '|' .
              $data['kode_produk'] . '|' .
              $data['trx_type'] . '|' .
              $data['billNumber'].'|'.
              $timestpm
              , self::$appkeyppob);
          $HEADER = array('ptokenkey: '.self::$apptokenppob, 'signature: '.$signature);

          $curl = curl_init();
          curl_setopt_array($curl, array(
              CURLOPT_URL => "http://core.mumuapps.id/ppob/b2b/transaksi",
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 0,
              CURLOPT_FOLLOWLOCATION => true,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "POST",
              CURLOPT_POSTFIELDS => $data,
              CURLOPT_HTTPHEADER=>$HEADER
          ));
          $response = curl_exec($curl);
          curl_close($curl);
          $logppob = new LogPpob();
          $logppob->id = self::uuidv4();
          $logppob->no_transaksi = $no_transaksi;
          $logppob->req_json = json_encode($data);
          $logppob->res_json = $response;
          $logppob->created_ad = date('Y-m-d H:i:s');
          $logppob->type = $type;
          $logppob->save();
          return $response;
      }

      static function _curl_exexc_walet($slugurl = '', $postdata = '', $METHOD = 'POST', $HEADER_ADD =[])
      {
          $HEADER = array('TOKEN-KEY: '.self::$appkey, 'APP-KEY: '.self::$appkeywalet);
          if (!empty($HEADER_ADD)){
              foreach ($HEADER_ADD as $r){
                  $HEADER[]=$r;
              }
          }
//        $postdata = http_build_query($data);
          // create curl resource
          $ch = curl_init();

          // set url
          curl_setopt($ch, CURLOPT_URL, self::$urlpa.$slugurl);
          //return the transfer as a string
          curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $METHOD);
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
          curl_setopt($ch, CURLOPT_HTTPHEADER, $HEADER);

          // $output contains the output string
          $output = curl_exec($ch);
          // close curl resource to free up system resources
          curl_close($ch);
          return $output;
      }
      static function _curl_exexc_walet_custom($slugurl = '', $postdata = '', $METHOD = 'POST', $HEADER_ADD =[],$CONFIGTYPE='')
      {
        //   DOMPETMU_MERCHANT
          $cariconfigapi = ConfigApi::findOne(['kode_api'=>$CONFIGTYPE]);
          $tokenkey = $cariconfigapi->token_key;
          $appkey = $cariconfigapi->secret_key;
          $HEADER = array('TOKEN-KEY: '.$tokenkey, 'APP-KEY: '.$appkey);
          if (!empty($HEADER_ADD)){
              foreach ($HEADER_ADD as $r){
                  $HEADER[]=$r;
              }
          }
//        $postdata = http_build_query($data);
          // create curl resource
          $ch = curl_init();

          // set url
          curl_setopt($ch, CURLOPT_URL, self::$urlpa.$slugurl);
          //return the transfer as a string
          curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $METHOD);
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
          curl_setopt($ch, CURLOPT_HTTPHEADER, $HEADER);

          // $output contains the output string
          $output = curl_exec($ch);
          // close curl resource to free up system resources
          curl_close($ch);
          return $output;
      }
      static function curlget($urlsop='')
      {
          $curl = curl_init();

          curl_setopt_array($curl, array(
              CURLOPT_URL => $urlsop,
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_FOLLOWLOCATION => true,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "GET",
          ));

          $response = curl_exec($curl);

          curl_close($curl);
          return $response;
      }

      static function LoginWallet($email,$pwd){
          $gettokenwalletlogin = Mylibs::_curl_exexc_walet("mupays/partner/gettokenweb", [], 'GET',[]);
          $gettokenwalletlogin = json_decode($gettokenwalletlogin,true);
          $tokenwalletlogin = $gettokenwalletlogin['data']['token_akses'];
          $username = $email;
          $password = $pwd;
          $datapost = array('username' => $username, 'password' => $password);
          $dataheader = array('token-partner:'.$tokenwalletlogin);
          $loginwallet = Mylibs::_curl_exexc_walet('mupays/partner/login',$datapost, 'POST',$dataheader);
          $resjsn = json_decode($loginwallet, true);
          return $resjsn;
      }
      static function LoginWalletCustom($email,$pwd){
          $cekemailperson = Person::findOne(['email'=>$email]);
          $confumum = 'DOMPETMU_UMUM';
          if (!empty($cekemailperson)){
              if($cekemailperson->personauth->oauth_mupays_type == 'DOMPETMU_MERCHANT'){
                  $confumum = 'DOMPETMU_MERCHANT';
              }
          }
          $gettokenwalletlogin = Mylibs::_curl_exexc_walet_custom("mupays/partner/gettokenweb", [], 'GET',[],$confumum);
          $gettokenwalletlogin = json_decode($gettokenwalletlogin,true);
          $tokenwalletlogin = $gettokenwalletlogin['data']['token_akses'];
          $username = $email;
          $password = $pwd;
          $datapost = array('username' => $username, 'password' => $password);
          $dataheader = array('token-partner:'.$tokenwalletlogin);
          $loginwallet = Mylibs::_curl_exexc_walet_custom('mupays/partner/login',$datapost, 'POST',$dataheader,$confumum);
          $resjsn = json_decode($loginwallet, true);
          $resjsn['type_config']=$confumum;
          return $resjsn;
      }
  }