function doSignin(action, formid = 'formLogin') {
  const options = {
    rules: {
      email: {
        required: true,
        email: true,
      },
      password: 'required',
    },
    messages: {
      email: {
        required: 'Email kamu belum diisi',
        email: 'Format email kamu tidak sesuai',
      },
      password: 'Password kamu belum diisi',
    },
  };

  const form = `#${formid}`;
  $(form).validate(options);

  if ($(form).valid()) {
    $.ajax({
      url: action,
      type: 'post',
      data: $(form).serialize(),
      beforeSend: function () {
        $('#btnSignin').prop('disabled', true);
        $('#btnSignin').html('Submited...');
      },
      success: function (response) {
        if (response.meta.code === 200) {
          location.href = response.data.callback;
        } else {
          new PNotify({
            title: 'Peringatan',
            text: response.meta.message,
            type: 'danger',
            addclass: 'alert alert-styled-left text-center',
          });
        }
        $('#btnSignin').prop('disabled', false);
        $('#btnSignin').html('Sign in');
      },
      error: function (jqXHR, textStatus, errorThrown) {},
    });
  }
}
