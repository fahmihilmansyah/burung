var datatable;

function tableInit() {
  datatable = $('#dtRoles').DataTable({
    processing: true,
    serverSide: true,
    autoWidth: false,
    ajax: $('#dtRoles').attr('load-url'),
    columns: [
      { data: 'name' },
      { data: 'description' },
      { data: 'created_at', className: 'text-center' },
      { data: 'updated_at', className: 'text-center' },
      { data: 'action', className: 'text-center' },
    ],
    columnDefs: [
      {
        targets: 1,
        orderable: false,
      },
      {
        targets: 4,
        orderable: false,
      },
    ],
  });
}

tableInit();

function tableDelete(id) {
  swal(
    {
      type: 'warning',
      title: 'Yakin menghapus data ini?',
      showCancelButton: true,
      confirmButtonText: 'Ya, hapus.',
      cancelButtonText: 'Batal',
    },
    function (isConfirm) {
      if (isConfirm) {
        $.ajax({
          url: `${host}/jospanel/roles/delete?id=${id}`,
          type: 'get',
          success: function (response) {
            if (response.meta.code === 200) {
              new PNotify({
                title: 'Informasi',
                text: response.meta.message,
                type: 'success',
                addclass: 'alert alert-styled-left text-center',
              });
            } else {
              new PNotify({
                title: 'Peringatan',
                text: response.meta.message,
                type: 'danger',
                addclass: 'alert alert-styled-left text-center',
              });
            }

            location.href = response.data.callback;
          },
          error: function (jqXHR, textStatus, errorThrown) {},
        });
      }
    }
  );
}
