var datatable;
var menuData;

async function getMenuData(data = {}) {
  function getData() {
    var id = $('#hiddenId').val();
    if (id !== '') data.id = id;
    return $.ajax({
      url: $('#dtMenus').attr('load-url'),
      type: 'GET',
      data: data,
    });
  }

  try {
    const res = await getData();
    menuData = res;
  } catch (error) {
    menuData = [];
  }

  tableInit(menuData);

  $(document).ready(function () {
    $('.parent').each(function () {
      $(this).on('click', function () {
        var references = $(this).attr('parentId');
        if ($(this).is(':checked')) {
          $('.child').each(function () {
            if ($(this).attr('references') === references) {
              $(this).prop('checked', true);
            }
          });
        } else {
          $('.child').each(function () {
            if ($(this).attr('references') === references) {
              $(this).prop('checked', false);
            }
          });
        }
      });
    });

    $('#checkall').on('click', function () {
      $('input[type="checkbox"]').prop('checked', true);
    });

    $('#uncheckall').on('click', function () {
      $('input[type="checkbox"]').prop('checked', false);
    });
  });
}

function redDraw(menuData) {
  datatable.clear();
  datatable.rows.add(menuData);
  datatable.draw();
}

function tableInit(menuData) {
  datatable = $('#dtMenus').treeTable({
    data: menuData,
    bLengthChange: false,
    bPaginate: false,
    bFilter: false,
    columns: [
      { data: 'name' },
      { data: 'path', className: 'text-center' },
      { data: 'action', className: 'text-center' },
    ],
  });
}

getMenuData();

function save(action) {
  const options = {
    rules: {
      name: 'required',
      description: 'required',
    },
    messages: {
      name: 'Isi role kamu',
      description: 'Isi deskripsi role kamu',
    },
  };

  const form = `#formRole`;
  $(form).validate(options);

  if ($(form).valid()) {
    $.ajax({
      url: action,
      type: 'post',
      data: $(form).serialize(),
      beforeSend: function () {
        $('#rolesSave').prop('disabled', true);
        $('.rolesSave').html('Process...');
      },
      success: function (response) {
        $('#rolesSave').prop('disabled', false);
        $('.rolesSave').html('Simpan');
        swal(
          {
            title: 'Informasi',
            text: 'Proses penyimpanan berhasil.',
            type: 'success',
            showCancelButton: false,
            confirmButtonClass: 'btn-danger',
            confirmButtonText: 'Ok',
          },
          function (isConfirm) {
            if (isConfirm) {
              location.href = response.data.callback;
            }
          }
        );
      },
      error: function (jqXHR, textStatus, errorThrown) {},
    });
  }
}
