$(document).ready(function () {
  $('.parentCheck').each(function () {
    $(this).on('click', function () {
      var references = $(this).attr('parentId');
      if ($(this).is(':checked')) {
        $('.childCheck').each(function () {
          if ($(this).attr('references') === references) {
            $(this).prop('checked', true);
          }
        });
      } else {
        $('.childCheck').each(function () {
          if ($(this).attr('references') === references) {
            $(this).prop('checked', false);
          }
        });
      }
    });
  });

  $('#checkall').on('click', function () {
    $('input[type="checkbox"]').prop('checked', true);
  });

  $('#uncheckall').on('click', function () {
    $('input[type="checkbox"]').prop('checked', false);
  });
});

function doSaveAssign(action) {
  const form = `#assignForm`;

  const options = {
    rules: {
      juri_id: {
        required: true,
      },
    },
    messages: {
      juri_id: {
        required: 'isi juri',
      },
    },
  };

  $(form).validate(options);

  if ($(form).valid()) {
    loadingType = 'fullScreenLoading';
    $.ajax({
      url: $(form).attr('action'),
      type: 'post',
      data: $(form).serialize(),
      success: function (response) {
        if (response.meta.code === 400) {
          Snackbar.show({
            text: response.meta.message,
            actionText: 'DISMISS',
            pos: 'bottom-center',
            actionTextColor: '#dc3545',
          });
          return true;
        } else {
          location.href = response.data.callback;
        }
      },
      error: function (jqXHR, textStatus, errorThrown) {},
    });
  }
}
