function submit(formId) {
  var $form = $('#' + formId),
    data = $form.data('yiiActiveForm');

  $.each(data.attributes, function () {
    this.status = 3;
  });
  $form.yiiActiveForm('validate');
  if ($form.find('.has-error').length == 0) {
    $form.submit();
    return true;
  }
  return false;
}

function generateKode() {
  let generate = makeid(10);

  $('#kode_juri').val(generate);

  console.log(generate);
}

function makeid(length) {
  var result = '';
  var characters =
    'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  var charactersLength = characters.length;
  for (var i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}
