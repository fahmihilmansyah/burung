function doHari(action) {
  const options = {
    rules: {
      nama_pemilik: 'required',
      notlp: 'required',
      alamat: 'required',
    },
    messages: {
      nama_pemilik: 'Isi nama pemilik kamu',
      notlp: 'Isi no telpon kamu',
      alamat: 'Isi alamat kamu',
    },
  };

  const form = `#formHari`;
  $(form).validate(options);

  if ($(form).valid()) {
    $.ajax({
      url: action,
      type: 'post',
      data: $(form).serialize(),
      success: function (response) {
        swal
          .fire({
            title: 'Informasi',
            text: 'Proses penyimpanan berhasil.',
            type: 'success',
            showCancelButton: false,
            confirmButtonClass: 'btn-danger',
            confirmButtonText: 'Ok',
          })
          .then((result) => {
            if (result) {
              location.href = response.data.callback;
            }
          });
      },
      error: function (jqXHR, textStatus, errorThrown) {},
    });
  }
}
