var datatable;

function tableInit() {
  datatable = $('#dtUsers').DataTable({
    processing: true,
    serverSide: true,
    autoWidth: false,
    ajax: $('#dtUsers').attr('load-url'),
    columns: [
      { data: 'name' },
      { data: 'email' },
      { data: 'role' },
      { data: 'created_at', className: 'text-center' },
      { data: 'updated_at', className: 'text-center' },
      { data: 'action', className: 'text-center' },
    ],
    columnDefs: [
      {
        targets: 2,
        orderable: false,
      },
      {
        targets: 5,
        orderable: false,
      },
    ],
  });
}

tableInit();

function tableDelete(id) {
  swal(
    {
      type: 'warning',
      title: 'Yakin menghapus data ini?',
      showCancelButton: true,
      confirmButtonText: 'Ya, hapus.',
      cancelButtonText: 'Batal',
    },
    function (isConfirm) {
      if (isConfirm) {
        $.ajax({
          url: `${host}/jospanel/users/delete?id=${id}`,
          type: 'get',
          success: function (response) {
            if (response.meta.code === 200) {
              new PNotify({
                title: 'Informasi',
                text: response.meta.message,
                type: 'success',
                addclass: 'alert alert-styled-left text-center',
              });
            } else {
              new PNotify({
                title: 'Peringatan',
                text: response.meta.message,
                type: 'danger',
                addclass: 'alert alert-styled-left text-center',
              });
            }
            datatable.clear().draw();
          },
          error: function (jqXHR, textStatus, errorThrown) {},
        });
      }
    }
  );
}

function save(action) {
  const options = {
    rules: {
      name: 'required',
      role_id: 'required',
      email: {
        required: true,
        email: true,
      },
    },
    messages: {
      name: 'Isi nama lengkap kamu',
      role_id: 'Isi role kamu',
      email: {
        required: 'Isi email kamu',
        email: 'Format email kamu tidak sesuai',
      },
    },
  };

  const form = `#formUser`;
  $(form).validate(options);

  if ($(form).valid()) {
    $.ajax({
      url: action,
      type: 'post',
      data: $(form).serialize(),
      success: function (response) {
        swal(
          {
            title: 'Informasi',
            text: 'Proses penyimpanan berhasil.',
            type: 'success',
            showCancelButton: false,
            confirmButtonClass: 'btn-danger',
            confirmButtonText: 'Ok',
          },
          function (isConfirm) {
            if (isConfirm) {
              location.href = response.data.callback;
            }
          }
        );
      },
      error: function (jqXHR, textStatus, errorThrown) {},
    });
  }
}
