function doSignin(action, formid = 'formLogin') {
  const options = {
    rules: {
      username: {
        required: true,
        email: true,
      },
      password: 'required',
    },
    messages: {
      username: {
        required: 'Email kamu belum diisi',
        email: 'Format email kamu tidak sesuai',
      },
      password: 'Password kamu belum diisi',
    },
  };

  const form = `#${formid}`;
  $(form).validate(options);

  if ($(form).valid()) {
    loadingType = 'fullScreenLoading';
    console.log(action);
    $.ajax({
      url: action,
      type: 'post',
      data: $(form).serialize(),
      success: function (response) {
        if (response.meta.code === 200) {
          location.href = response.data.callback;
        } else {
          Snackbar.show({
            text: response.meta.message,
            actionText: 'DISMISS',
            pos: 'bottom-center',
            actionTextColor: '#dc3545',
          });
        }
      },
      error: function (jqXHR, textStatus, errorThrown) {},
    });
  }
}
