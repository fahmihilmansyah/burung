function doVerified(action) {
  swal(
    {
      type: 'warning',
      title: 'Apa anda yakin akan memverifikasi data ini?',
      showCancelButton: true,
      confirmButtonText: 'Ya.',
      cancelButtonText: 'Batal',
    },
    function (isConfirm) {
      if (isConfirm) {
        const form = `#formVerified`;

        $.ajax({
          url: action,
          type: 'post',
          data: $(form).serialize(),
          beforeSend: function () {
            $('#btnSave').prop('disabled', true);
            $('.btnSave').html('Process...');
          },
          success: function (response) {
            $('#btnSave').prop('disabled', false);
            $('.btnSave').html('Simpan');

            if (response.meta.code !== 200) {
              swal({
                title: 'Informasi',
                text: response.meta.message,
                type: 'error',
                showCancelButton: false,
                confirmButtonClass: 'btn-danger',
                confirmButtonText: 'Ok',
              });
            } else {
              swal(
                {
                  title: 'Informasi',
                  text: 'Proses penyimpanan berhasil.',
                  type: 'success',
                  showCancelButton: false,
                  confirmButtonClass: 'btn-danger',
                  confirmButtonText: 'Ok',
                },
                function (isConfirm) {
                  if (isConfirm) {
                    location.href = response.data.callback;
                  }
                }
              );
            }
          },
          error: function (jqXHR, textStatus, errorThrown) {},
        });
      }
    }
  );
}
