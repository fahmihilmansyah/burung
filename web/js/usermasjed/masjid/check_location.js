var map;
var lat = parseFloat($('#lat').val());
var lng = parseFloat($('#lng').val());
var masjidData;

function find(latLng) {
  var request = {
    types: ['mosque'],
    location: latLng,
    radius: 15000,
  };

  places = new google.maps.places.PlacesService(map);
  places.nearbySearch(request, callback);
}

function initMap() {
  map = new google.maps.Map(document.getElementById('map'), {
    zoom: 17,
    center: { lat: lat, lng: lng },
  });

  initialLocation = new google.maps.LatLng(lat, lng);
  map.setCenter(initialLocation);

  // creates a marker of user's location
  var marker = new google.maps.Marker({
    position: initialLocation,
    map: map,
    draggable: true,
    title: 'Your Location',
    optimized: false,
    zIndex: 99999999,
  });

  new google.maps.event.addListener(marker, 'dragend', function () {
    $('#lat').val(marker.position.lat());
    $('#lng').val(marker.position.lng());
    $('#mapModal').modal('hide');
  });

  find(marker.getPosition());

  // Create the search box and link it to the UI element.
  var searchInput = document.getElementById('pac-input');
  var searchBox = new google.maps.places.SearchBox(searchInput);

  // Bias the SearchBox results towards current map's viewport.
  map.addListener('bounds_changed', function () {
    searchBox.setBounds(map.getBounds());
  });

  // Listen for the event fired when the user selects a prediction and retrieve
  // more details for that place.
  searchBox.addListener('places_changed', function () {
    displaySearchResults(map, searchBox);
  });
}

function callback(results, status, pagination) {
  if (status !== 'OK') return;

  createMarkers(results);
}

function createMarkers(places) {
  var bounds = new google.maps.LatLngBounds();
  for (var i = 0, place; (place = places[i]); i++) {
    var image = {
      url: place.icon,
      size: new google.maps.Size(71, 71),
      origin: new google.maps.Point(0, 0),
      anchor: new google.maps.Point(17, 34),
      scaledSize: new google.maps.Size(25, 25),
    };

    var marker = new google.maps.Marker({
      map: map,
      icon: image,
      title: place.name,
      position: place.geometry.location,
    });

    marker.placeID = places[i].place_id;
    marker.data = places[i];

    attachClickEvent(marker);

    bounds.extend(place.geometry.location);

    const data = {
      gmaps_id: place.place_id,
      nama_masjid: place.name,
      alamat_masjid: place.vicinity,
      lat: place.geometry.location.lat(),
      lng: place.geometry.location.lng(),
    };
    saveMapsData(data);
  }
  map.fitBounds(bounds);
  map.setZoom(17);
  map.setCenter(new google.maps.LatLng(lat, lng));
}

function attachClickEvent(marker) {
  google.maps.event.addListener(marker, 'click', function () {
    // the reference to the marker will be saved in the closure
    // console.log(marker.data.geometry.location.lat());
    $('#lat').val(marker.data.geometry.location.lat());
    $('#lng').val(marker.data.geometry.location.lng());
    $('#mapModal').modal('hide');
  });
}

function displaySearchResults(map, searchBox) {
  var places = searchBox.getPlaces();

  const newLat = places[0].geometry.location.lat();
  const newLang = places[0].geometry.location.lng();

  map = new google.maps.Map(document.getElementById('map'), {
    zoom: 17,
    center: { lat: newLat, lng: newLang },
  });

  initialLocation = new google.maps.LatLng(newLat, newLang);
  map.setCenter(initialLocation);
  // creates a marker of user's location
  var marker = new google.maps.Marker({
    position: initialLocation,
    map: map,
    draggable: true,
    title: 'Your Location',
    optimized: false,
    zIndex: 99999999,
  });

  var request = {
    types: ['mosque'],
    location: marker.getPosition(),
    radius: 15000,
  };

  nearbiest = new google.maps.places.PlacesService(map);
  nearbiest.nearbySearch(request, function (results, status, pagination) {
    if (status !== 'OK') return;
    var bounds = new google.maps.LatLngBounds();
    for (var i = 0, place; (place = results[i]); i++) {
      var image = {
        url: place.icon,
        size: new google.maps.Size(71, 71),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(17, 34),
        scaledSize: new google.maps.Size(25, 25),
      };

      var marker = new google.maps.Marker({
        map: map,
        icon: image,
        title: place.name,
        position: place.geometry.location,
      });

      marker.placeID = results[i].place_id;
      marker.data = results[i];

      attachClickEvent(marker);

      const data = {
        gmaps_id: place.place_id,
        nama_masjid: place.name,
        alamat_masjid: place.vicinity,
        lat: place.geometry.location.lat(),
        lng: place.geometry.location.lng(),
      };
      saveMapsData(data);

      bounds.extend(place.geometry.location);
    }
    map.fitBounds(bounds);
    map.setZoom(17);
    map.setCenter(new google.maps.LatLng(newLat, newLang));

    $('#lat').val(newLat);
    $('#lng').val(newLang);
  });

  new google.maps.event.addListener(marker, 'dragend', function () {
    $('#lat').val(marker.position.lat());
    $('#lng').val(marker.position.lng());
    $('#mapModal').modal('hide');
  });
}

function saveMapsData(data) {
  $.ajax({
    url: $('#urlSaveMap').val(),
    type: 'get',
    data: data,
    success: function (response) {
      console.log(response);
    },
    error: function (jqXHR, textStatus, errorThrown) {},
  });
}
