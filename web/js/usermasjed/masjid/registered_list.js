var tableRegisteredMasjed;

function registeredMasjedInit() {
  tableRegisteredMasjed = $('#dtRegisteredMasjed').DataTable({
    processing: true,
    serverSide: true,
    autoWidth: false,
    ajax: $('#dtRegisteredMasjed').attr('load-url'),
    columns: [
      { data: 'action', className: 'text-center' },
      { data: 'is_verifikasi', className: 'text-center' },
      { data: 'nama_masjid' },
      { data: 'nama_pengurus' },
      { data: 'email_pengurus' },
      { data: 'alamat_masjid' },
      { data: 'created_ad', className: 'text-center' },
      { data: 'updated_ad', className: 'text-center' },
    ],
    order: [[5, 'desc']],
  });
}

registeredMasjedInit();
