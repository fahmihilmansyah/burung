$(document).ready(function () {
  showPie();
});

function showPie() {
  let piechart = $(`#piechart`);

  $.ajax({
    url: piechart.attr('load-url'),
    type: 'get',
    success: function (response) {
      merchantMasjidPieChart(response.data);
    },
    error: function (jqXHR, textStatus, errorThrown) {},
  });
}

function merchantMasjidPieChart(data) {
  const dummy = [
    {
      name: 'Chrome',
      y: 61.41,
      sliced: true,
      selected: true,
    },
    {
      name: 'Internet Explorer',
      y: 11.84,
    },
    {
      name: 'Firefox',
      y: 10.85,
    },
    {
      name: 'Other',
      y: 2.61,
    },
  ];
  Highcharts.chart('piechart', {
    chart: {
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false,
      type: 'pie',
    },
    title: {
      text: 'Data Masjid Terdaftar 2021',
    },
    tooltip: {
      pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>',
    },
    accessibility: {
      point: {
        valueSuffix: '%',
      },
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: 'pointer',
        dataLabels: {
          enabled: true,
          format: '<b>{point.name}</b>: {point.percentage:.1f} %',
        },
      },
    },
    series: [
      {
        name: 'Users',
        colorByPoint: true,
        data: data ?? dummy,
      },
    ],
  });
}
