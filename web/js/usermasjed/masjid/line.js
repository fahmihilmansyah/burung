$(document).ready(function () {
  showLine();
});

function showLine() {
  let piechart = $(`#linechart`);

  $.ajax({
    url: piechart.attr('load-url'),
    type: 'get',
    data: {
      date: Date.now(),
    },
    success: function (response) {
      merchatMasjidLineChart(response.data);
    },
    error: function (jqXHR, textStatus, errorThrown) {},
  });
}

function merchatMasjidLineChart(data) {
  Highcharts.setOptions({
    global: {
      useUTC: false,
    },
  });
  Highcharts.chart('linechart', {
    title: {
      text: 'Data Masjid Terdaftar 2021',
    },

    subtitle: {
      text: 'Source: thesolarfoundation.com',
    },

    yAxis: {
      title: {
        text: 'Number of Masjid',
      },
    },
    legend: {
      layout: 'vertical',
      align: 'right',
      verticalAlign: 'middle',
    },
    xAxis: {
      categories: data?.categories,
    },

    series: data?.chartData,

    responsive: {
      rules: [
        {
          condition: {
            maxWidth: 500,
          },
          chartOptions: {
            legend: {
              layout: 'horizontal',
              align: 'center',
              verticalAlign: 'bottom',
            },
          },
        },
      ],
    },
  });
}
