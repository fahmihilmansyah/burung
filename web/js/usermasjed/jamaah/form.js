$('.select2').select2();
$('#dropper-format').dateDropper({
  dropWidth: 200,
  format: 'd F Y',
  dropPrimaryColor: '#1abc9c',
  dropBorder: '1px solid #1abc9c',
});

function saveJamaah() {
  const options = {
    rules: {
      fullname: 'required',
      no_ktp: 'required',
      email: {
        required: true,
        email: true,
      },
      no_hp: 'required',
      jenis_kelamin: 'required',
      tempat_lahir: 'required',
      tgl_lahir: 'required',
      pendidikan_id: 'required',
      agama_id: 'required',
      alamat: 'required',
      prov_id: 'required',
      kabkota_id: 'required',
      kec_id: 'required',
    },
    messages: {
      fullname: 'Isi nama lengkap',
      no_ktp: 'Isi nomor ktp',
      email: {
        required: 'Isi email',
        email: 'Format email salah',
      },
      no_hp: 'Isi nomor hp',
      jenis_kelamin: 'Isi jenis kelamin',
      tempat_lahir: 'Isi tempat lahir',
      tgl_lahir: 'Isi tanggal lahir',
      pendidikan_id: 'Isi pendidikan',
      alamat: 'Isi alamat',
      agama_id: 'Isi agama',
      prov_id: 'Isi provinsi',
      kabkota_id: 'Isi kabupaten',
      kec_id: 'Isi kecamatan',
    },
  };

  const form = `#formJamaah`;
  $(form).validate(options);

  if ($(form).valid()) {
    $.ajax({
      url: action,
      type: 'post',
      data: $(form).serialize(),
      beforeSend: function () {
        $('#btnSave').prop('disabled', true);
        $('.btnSave').html('Process...');
      },
      success: function (response) {
        $('#btnSave').prop('disabled', false);
        $('.btnSave').html('Simpan');
        swal(
          {
            title: 'Informasi',
            text: 'Proses penyimpanan berhasil.',
            type: 'success',
            showCancelButton: false,
            confirmButtonClass: 'btn-danger',
            confirmButtonText: 'Ok',
          },
          function (isConfirm) {
            if (isConfirm) {
              location.href = response.data.callback;
            }
          }
        );
      },
      error: function (jqXHR, textStatus, errorThrown) {},
    });
  }
}

$('#color-accordion').accordion({
  heightStyle: 'content',
  icons: {
    header: 'zmdi zmdi-chevron-down',
    activeHeader: 'zmdi zmdi-chevron-up',
  },
});

var loadFile = function (event, idTarget) {
  console.log(event);
  var output = document.getElementById(idTarget);
  output.src = URL.createObjectURL(event.target.files[0]);
  output.onload = function () {
    URL.revokeObjectURL(output.src);
  };
};
