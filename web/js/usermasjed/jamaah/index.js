var tablejamaah;

function jamaahInit() {
  tablejamaah = $('#dtJamaah').DataTable({
    processing: true,
    serverSide: true,
    autoWidth: false,
    ajax: $('#dtJamaah').attr('load-url'),
    columns: [
      { data: 'fullname' },
      { data: 'email' },
      { data: 'no_hp', className: 'text-center' },
      { data: 'created_at', className: 'text-center' },
      { data: 'updated_at', className: 'text-center' },
      { data: 'is_active', className: 'text-center' },
      { data: 'action', className: 'text-center' },
    ],
  });
}

jamaahInit();
