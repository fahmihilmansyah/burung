var penilaian = [];
var gantungan;

function deleteData(url) {
  Swal.fire({
    title: 'Are you sure?',
    text: "You won't be able to revert this!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Yes, delete it!',
  }).then((result) => {
    if (result.isConfirmed) {
      location.href = url;
    }
  });
}

/** Preview Penilaian */
function preview(formname = 'eventform') {
  let row = parseInt($('#jml_row').val());
  let column = parseInt($('#jml_col').val());
  let filter = $(`#${formname}-type_gnt`).val();

  if (row === '' || column === '' || filter === '') {
    Swal.fire(
      'Error',
      'Jumlah Row, Column, dan Tipe Gantungan Belum diberikan nilai!',
      'error'
    );
    return false;
  }
  $('.preview').fadeIn();
  generateContent({ row, column, filter });
  $('.owl-carousel').owlCarousel({
    items: column,
  });
}

function generateContent({ row, column, filter }) {
  /**
   * create array chunk
   * @param {array} arr
   * @param {int} n
   */
  function* chunks(arr, n) {
    for (let i = 0; i < arr.length; i += n) {
      yield arr.slice(i, i + n);
    }
  }

  /**
   * filter array chunk
   * @param {array} array
   * @param {string} filteredType
   * @returns array
   */
  function filtered(array, filteredType) {
    /* reverse asc or desc normal*/
    let reversed = array.map(function (item) {
      return filteredType === 'ASC' ? item : item.reverse();
    });

    if (filteredType == 'ASCSLINE') {
      /* reverse asc sline*/
      reversed = array.map(function (item, index) {
        if (index % 2 === 0) {
          return item.reverse();
        }
        return item;
      });
    }

    if (filteredType == 'DESCSLINE') {
      /* reverse desc sline*/
      reversed = array
        .map(function (item) {
          return item.reverse();
        })
        .map(function (item, index) {
          if (index % 2 === 0) {
            return item.reverse();
          }
          return item;
        });
    }

    return reversed;
  }

  const NArrays = [
    ...chunks(
      [...Array(row * column).keys()].map((v, i) => i + 1),
      column
    ),
  ];
  let filteredArray = filtered(NArrays, filter);
  let table = document.getElementById('previewTarget');

  let tableContent = '<div class="gantungan">';
  filteredArray.forEach((rows) => {
    tableContent += `<div class="owl-carousel">`;
    rows.forEach((column) => {
      tableContent += `<div class="gantunganCol"><p>${column}</p></div>`;
    });
    tableContent += `</div>`;
  });
  tableContent += `</div>`;

  table.innerHTML = tableContent;
}

function closePreview() {
  $('.preview').hide();
}

let alert = document.getElementsByClassName('alert-success')[0];

if (alert !== undefined) {
  setTimeout(() => {
    alert.style.display = 'none';
  }, 3000);
}

function saveEvent(action) {
  const options = {
    rules: {
      nama_event: 'required',
      type_gantungan_id: 'required',
      harga_tiket: 'required',
      tgl_pelaksanaan: 'required',
      waktu: 'required',
      jenis_burung_id: 'required',
    },
    messages: {
      nama_event: 'Isi nama event',
      type_gantungan_id: 'Isi tipe gantungan',
      harga_tiket: 'Isi harga tiket',
      tgl_pelaksanaan: 'Isi tanggal pelaksanaan',
      waktu: 'Isi waktu',
      jenis_burung_id: 'Isi jenis burung',
    },
  };

  const form = `#eventForm`;
  $(form).validate(options);

  if ($(form).valid()) {
    $.ajax({
      url: action,
      type: 'post',
      data: $(form).serialize(),
      success: function (response) {
        swal
          .fire({
            title: 'Informasi',
            text: 'Proses penyimpanan berhasil.',
            type: 'success',
            showCancelButton: false,
            confirmButtonClass: 'btn-danger',
            confirmButtonText: 'Ok',
          })
          .then((result) => {
            if (result) {
              location.href = response.data.callback;
            }
          });
      },
      error: function (jqXHR, textStatus, errorThrown) {},
    });
  }
}

var enableDay = $('#jadwapelaksanaan').val();

$('#tanggal_pelaksanaan').datepicker({
  beforeShowDay: function (date) {
    inEnable = enableDay !== '' ? enableDay.split(',') : [];
    var day = String(date.getDay());
    if (enableDay.includes(day)) return [true, ''];
    else return [false, ''];
  },
});
$('#btnCalendar').click(function () {
  $('#tanggal_pelaksanaan').focus();
});
