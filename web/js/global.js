const $loading = $('#loading').hide();
var loadingType;
var host = window.location.origin;

function loading(target, type) {
  if (type === 'hide') {
    $(`#${target}`).hide();
  } else {
    $(`#${target}`).show();
  }
}

$(document)
  .ajaxStart(function () {
    if (loadingType === 'fullScreenLoading') {
      $loading.show();
    }
  })
  .ajaxStop(function () {
    if (loadingType === 'fullScreenLoading') {
      $loading.hide();
    }
  });

$.validator.setDefaults({
  errorPlacement: function (error, element) {
    let label;
    if (element.parent().attr('class') === 'containerRadio') {
      label = element.parent().parent().find('div.error-wrapper')[0];
    } else {
      label = element.parent().find('div.error-wrapper')[0];
    }
    error.appendTo(label);
  },
  invalidHandler: function (e, validator) {
    Snackbar.show({
      text: 'Oops.. Terjadi Kesalahan!',
      actionText: 'DISMISS',
      pos: 'bottom-center',
      actionTextColor: '#dc3545',
    });
  },
});

$('.AppListData__cardWrapper').each(function () {
  $(this).click(function (e) {
    var action = $(this).find('.action');
    action.removeClass('action-none');
  });
});

$('.batal').each(function () {
  $(this).click(function (e) {
    var action = $(this).parent();
    action.addClass('action-none');
    e.stopPropagation();
  });
});

function loadChild(self) {
  const component = $(`#${self}`);
  const loadChildUrl = component.attr('loadChildUrl');
  const loadTarget = component.attr('loadTarget');
  const loadType = component.attr('loadType');
  const childTarget = $(`#${loadTarget}`);

  $.ajax({
    url: loadChildUrl,
    type: 'get',
    data: {
      parent: loadType,
      value: component.val(),
      placeholder: `Pilih ${loadTarget}`,
    },
    beforeSend: function () {
      childTarget
        .parent()
        .find('label')
        .append(`<span class="selectloading">Load data ...</span>`);
    },
    success: function (data) {
      childTarget.select2('destroy').empty().select2({ data });
      $('.selectloading').remove();
    },
    error: function (jqXHR, textStatus, errorThrown) {},
  });
}
