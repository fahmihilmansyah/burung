var datatable;

function tableInit() {
  datatable = $('#dtLogs').DataTable({
    processing: true,
    serverSide: true,
    autoWidth: false,
    ajax: $('#dtLogs').attr('load-url'),
    columns: [
      { data: 'controller' },
      { data: 'topic', className: 'text-center' },
      { data: 'source_ip' },
      { data: 'browser_agent' },
      { data: 'activity' },
      { data: 'reason', className: 'text-center' },
      { data: 'status', className: 'text-center' },
      { data: 'user_id', className: 'text-center' },
      { data: 'user_application' },
      { data: 'created_at' },
    ],
    columnDefs: [
      {
        targets: 1,
        orderable: false,
      },
      {
        targets: 4,
        orderable: false,
      },
    ],
  });
}

tableInit();
