var datatable;
var menuData;

async function getMenuData() {
  function getData() {
    return $.ajax({
      url: $('#dtMenus').attr('load-url'),
      type: 'GET',
    });
  }

  try {
    const res = await getData();
    menuData = res;
  } catch (error) {
    menuData = [];
  }

  tableInit(menuData);
}

function redDraw(menuData) {
  datatable.clear();
  datatable.rows.add(menuData);
  datatable.draw();
}

function tableInit(menuData) {
  datatable = $('#dtMenus').treeTable({
    data: menuData,
    bLengthChange: false,
    bPaginate: false,
    autoWidth: false,
    columns: [
      { data: 'name' },
      { data: 'path', className: 'text-center' },
      { data: 'icon', className: 'text-right' },
      { data: 'created_at', className: 'text-right' },
      { data: 'updated_at', className: 'text-right' },
      { data: 'action', className: 'text-center' },
    ],
  });
}

getMenuData();

function tableDelete(id) {
  swal(
    {
      type: 'warning',
      title: 'Yakin menghapus data ini?',
      showCancelButton: true,
      confirmButtonText: 'Ya, hapus.',
      cancelButtonText: 'Batal',
    },
    function (isConfirm) {
      if (isConfirm) {
        $.ajax({
          url: `${host}/jospanel/menus/delete?id=${id}`,
          type: 'get',
          success: function (response) {
            if (response.meta.code === 200) {
              new PNotify({
                title: 'Informasi',
                text: response.meta.message,
                type: 'success',
                addclass: 'alert alert-styled-left text-center',
              });
            } else {
              new PNotify({
                title: 'Peringatan',
                text: response.meta.message,
                type: 'danger',
                addclass: 'alert alert-styled-left text-center',
              });
            }

            location.href = response.data.callback;
          },
          error: function (jqXHR, textStatus, errorThrown) {},
        });
      }
    }
  );
}

function save(action) {
  const options = {
    rules: {
      name: 'required',
      path: 'required',
    },
    messages: {
      name: 'Isi nama menu kamu',
      path: 'Isi path kamu',
    },
  };

  const form = `#formMenu`;
  $(form).validate(options);

  if ($(form).valid()) {
    $.ajax({
      url: action,
      type: 'post',
      data: $(form).serialize(),
      success: function (response) {
        swal(
          {
            title: 'Informasi',
            text: 'Proses penyimpanan berhasil.',
            type: 'success',
            showCancelButton: false,
            confirmButtonClass: 'btn-danger',
            confirmButtonText: 'Ok',
          },
          function (isConfirm) {
            if (isConfirm) {
              location.href = response.data.callback;
            }
          }
        );
      },
      error: function (jqXHR, textStatus, errorThrown) {},
    });
  }
}

$('.icondropdown').on('change', function () {
  $('#display-icon').html(
    `<i class="feather ${$(this).find(':selected').val()}"></i>`
  );
});

$('.icondropdown').select2();
$('.roledropdown').select2();
