var otpNumber = {
  first: '',
  second: '',
  third: '',
  fourth: '',
  fifth: '',
  sixth: '',
};

function OTPInput() {
  const inputs = document.querySelectorAll('#otp > *[id]');
  for (let i = 0; i < inputs.length; i++) {
    inputs[i].addEventListener('keydown', function (event) {
      if (event.key === 'Backspace') {
        inputs[i].value = '';
        if (i !== 0) inputs[i - 1].focus();
      } else {
        if (i === inputs.length - 1 && inputs[i].value !== '') {
          return true;
        } else if (event.keyCode > 47 && event.keyCode < 58) {
          inputs[i].value = event.key;
          if (i !== inputs.length - 1) inputs[i + 1].focus();
          event.preventDefault();
        } else if (event.keyCode > 64 && event.keyCode < 91) {
          inputs[i].value = String.fromCharCode(event.keyCode);
          if (i !== inputs.length - 1) inputs[i + 1].focus();
          event.preventDefault();
        }
      }
    });
  }
}
OTPInput();

function submitOtp(action, otpid) {
  var first = $(`#first`).val();
  var second = $(`#second`).val();
  var third = $(`#third`).val();
  var fourth = $(`#fourth`).val();
  var fifth = $(`#fifth`).val();
  var sixth = $(`#sixth`).val();

  if (
    first === '' ||
    second === '' ||
    third === '' ||
    fourth === '' ||
    fifth === '' ||
    sixth === ''
  ) {
    console.log('error');
    return false;
  }

  const value = `${first}${second}${third}${fourth}${fifth}${sixth}`;

  $.ajax({
    url: action,
    type: 'post',
    data: { otpcode: value, otpid },
    beforeSend: function () {
      $('#btnKonfirmasi').attr('disabled', 'disabled');
    },
    success: function (response) {
      $('#btnKonfirmasi').removeAttr('disabled');

      if (response.meta.code === 400) {
        $('#targetMessage').html(`<p>${response.meta.message}</p>`);
      } else {
        location.href = response.data.callback;
      }
    },
    error: function (jqXHR, textStatus, errorThrown) {},
  });
}
