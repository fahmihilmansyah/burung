const penilaianStack = [];

const penilaianOptions = {
  rules: {
    'tipe_penilaian_id[0]': {
      required: true,
    },
  },
  messages: {
    'tipe_penilaian_id[0]': {
      required: 'isi tipe penilaian kamu',
    },
  },
};

function addPenilaian(action, prompt = '--- Pilih ---') {
  var html = `<div class="AppFormGroup">
                <label>Tipe Penilaian<span class="delete" onclick="deletePenilaian(this)"><i class="uil uil-trash-alt"></i></span></label>`;
  $.ajax({
    url: action,
    type: 'get',
    success: function (response) {
      const count = $('.tipepenilaiandropdown').length;
      const name = `tipe_penilaian_id[${count}]`;
      const id = `penilaianSelect${count}`;

      html += `<select name="${name}" class="form-control tipepenilaiandropdown" id="${id}">`;
      html += `<option value="">${prompt}</option>`;
      response.data.map((item) => {
        html += `<option value="${item.id}">${item.nama_type}</option>`;
      });
      html += `</select>`;
      html += `
                <div class="error-wrapper"></div>
                </div>
                `;

      $('#target-penilaian').prepend(html);
      $('#formAddPenilaian').validate(penilaianOptions);
      addRequiredRules(id, 'tipe penilaian');
    },
    error: function (jqXHR, textStatus, errorThrown) {},
  });
}

function deletePenilaian(self) {
  const parentClassName = self.parentNode.parentNode.className;
  const parentCount = $(`.${parentClassName}`).length;

  if (parentCount > 1) {
    self.parentNode.parentNode.remove();
  }
}

function doPenilaian(action) {
  const form = `#formAddPenilaian`;

  const getJenis = $('.tipepenilaiandropdown');
  var values = [];
  for (i = 0; i < getJenis.length; i++) {
    values.push(getJenis[i].value);
  }
  var counts = {};
  values.forEach(function (x) {
    counts[x] = (counts[x] || 0) + 1;
  });
  var errorSameValue = 0;
  for (var key in counts) {
    if (counts.hasOwnProperty(key)) {
      if (counts[key] > 1) {
        errorSameValue++;
        break;
      }
    }
  }
  if (errorSameValue > 0) {
    Snackbar.show({
      text: 'Oops.. Terjadi Kesalahan! Ada duplikasi data.',
      actionText: 'DISMISS',
      pos: 'bottom-center',
      actionTextColor: '#dc3545',
    });
    return false;
  }

  $(form).validate(penilaianOptions);

  if ($(form).valid()) {
    loadingType = 'fullScreenLoading';
    $.ajax({
      url: action,
      type: 'post',
      data: $(form).serialize(),
      success: function (response) {
        location.href = response.data.callback;
      },
      error: function (jqXHR, textStatus, errorThrown) {},
    });
  }
}

function addRequiredRules(id, label) {
  $(`#${id}`).rules('add', {
    required: true,
    messages: {
      required: `Isi ${label} kamu`,
    },
  });
}

function removeRules(id) {
  $(`#${id}`).rules('remove');
  $(`#${id}`).next().empty();
}
