function logout(action) {
  swal(
    {
      title: 'Anda yakin?',
      text: 'anda akan dilogout dari aplikasi ini.',
      type: 'warning',
      showCancelButton: true,
      confirmButtonClass: 'btn-danger',
      confirmButtonText: 'Ya, keluarkan saya!',
      cancelButtonText: 'Tidak, batalkan!',
      closeOnConfirm: false,
      closeOnCancel: true,
    },
    function (isConfirm) {
      if (isConfirm) {
        location.href = action;
      }
    }
  );
}
