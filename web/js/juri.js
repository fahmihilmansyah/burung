function carouselInit() {
  const column = $('#gantungan').attr('column');
  $('.owl-carousel').owlCarousel({
    items: column,
  });
}

carouselInit();

function Penilaian() {
  var gantungan = null;
  var event = null;
  var tipePenilaian = null;

  this.setGntId = function (value) {
    gantungan = value;
  };
  this.setEvId = function (value) {
    event = value;
  };
  this.setTpId = function (value) {
    tipePenilaian = value;
  };
  this.data = function () {
    return {
      gantungan,
      event,
      tipePenilaian,
    };
  };
  this.reset = function () {
    event = null;
    gantungan = null;
    tipePenilaian = null;
  };
}

const v = new Penilaian();

$('.assignValue').each(function () {
  $(this).on('click', function () {
    console.log($(this).attr('evId'));
    v.setEvId($(this).attr('evId'));
    v.setGntId($(this).attr('gntId'));

    $('.modalGantungan').show();
  });
});

$('.modalGantunganClose').on('click', function () {
  $('.modalGantungan').hide();
});

function sendPenilaian(self, action) {
  const tpid = self.children[0].value ?? null;
  v.setTpId(tpid);
  $.ajax({
    url: action,
    type: 'post',
    data: v.data(),
    success: function (response) {
      v.reset();
      $('.modalGantungan').hide();

      const totalpoint = response.data.totalpoint ?? 0;
      const gantunganid = response.data.gantungan_id ?? 'error';

      $(`#point-${gantunganid}`).html(totalpoint);
    },
    error: function (jqXHR, textStatus, errorThrown) {},
  });
}

function saveNilai() {
  console.log(v.data());
  const form = `#formNilai`;
  $(form).validate(jenisOptions);

  const data = $(form).serializeArray();

  data.push({ name: 'gantungan', value: v.data().gantungan });
  data.push({ name: 'event', value: v.data().event });

  if ($(form).valid()) {
    loadingType = 'fullScreenLoading';
    $.ajax({
      url: $(form).attr('action'),
      type: 'post',
      data: data,
      success: function (response) {
        v.reset();
        $('.modalGantungan').hide();

        const totalpoint = response.data.totalpoint ?? 0;
        const gantunganid = response.data.gantungan_id ?? 'error';

        $(`#point-${gantunganid}`).html(totalpoint);
        $(form).trigger('reset');
      },
      error: function (jqXHR, textStatus, errorThrown) {},
    });
  }
}
