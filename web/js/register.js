const options = {
  rules: {
    nama_lengkap: 'required',
    username: 'required',
    password: 'required',
    notelp: 'required',
    jenkel: 'required',
    usahaquestion: 'required',
    jenisusaha: 'required',
  },
  messages: {
    nama_lengkap: 'Isi nama lengkap kamu',
    username: 'Isi email kamu',
    password: 'Isi password kamu',
    notelp: 'Isi nomor telpon kamu',
    jenkel: 'Isi jenis kelamin kamu',
    usahaquestion: 'Isi pertanyaan usaha kamu',
    jenisusaha: 'Isi jenis usaha kamu',
  },
};

function doRegister(action) {
  const form = `#formRegister`;
  $(form).validate(options);

  if ($(form).valid()) {
    loadingType = 'fullScreenLoading';
    $.ajax({
      url: action,
      type: 'post',
      data: $(form).serialize(),
      success: function (response) {
        location.href = response.data.callback;
      },
      error: function (jqXHR, textStatus, errorThrown) {},
    });
  }
}

function handleRadioChange(radio) {
  $('#formRegister').validate(options);
  if (radio.value === '1') {
    $('#jenisUsahaWrapper').show();
    addRequiredRules('jenisusaha', 'jenis usaha');
    addRequiredRules('nama_toko', 'nama toko');
    addRequiredRules('alamat', 'alamat');
  } else {
    $('#jenisUsahaWrapper').hide();
    removeRules('jenisusaha');
    removeRules('nama_toko');
    removeRules('alamat');
  }
}

function addRequiredRules(id, label) {
  $(`#${id}`).rules('add', {
    required: true,
    messages: {
      required: `Isi ${label} kamu`,
    },
  });
}

function removeRules(id) {
  $(`#${id}`).rules('remove');
  $(`#${id}`).next().empty();
}
