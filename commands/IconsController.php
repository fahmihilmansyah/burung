<?php
namespace app\commands;
use Yii;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\db\Command;
use app\helpers\MyLibs;
use thamtech\uuid\helpers\UuidHelper;
use app\modules\bo\models\Icons;

class IconsController extends Controller
{
    public function actionSeeds()
    {
        $myIcons = MyLibs::listFeatherIcon();

        foreach ($myIcons as $key => $value) {
            $icon = new Icons;
            $icon->icon = $value[0];
            $icon->save();
        }

        return ExitCode::OK;
    }
}