<?php
namespace app\commands;
use Yii;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\db\Command;
use thamtech\uuid\helpers\UuidHelper;

class UserController extends Controller
{
    public function actionSeeds()
    {
        /**
        * Menu
        */
        $dashboardId = 1;
        $settingAppId = 2;
        $manajemenMenuId = 3;
        $manajemenRolesId =  4;
        $manajemenUsersId = 5;

        Yii::$app->db->createCommand()->batchInsert('menus',
            [ 
                'id',
                'sort_number',
                'name',
                'path',
                'icon',
                'parent',   
                'is_active',                     
                'created_at',
                'updated_at'
            ],
            [
                [
                    $dashboardId,
                    '1',
                    'Dashboard',
                    '#',
                    'icon-bar-chart', 
                    '0',
                    '1',
                    new \yii\db\Expression('NOW()'),
                    new \yii\db\Expression('NOW()')
                ],
                [
                    $settingAppId,
                    '2',
                    'Setting Application',
                    '#',
                    'icon-settings', 
                    '0',
                    '1',
                    new \yii\db\Expression('NOW()'),
                    new \yii\db\Expression('NOW()')
                ],
                [
                    $manajemenMenuId,
                    '3',
                    'Manajemen Menus',
                    'settings/menu',
                    '', 
                    $settingAppId,
                    '1',
                    new \yii\db\Expression('NOW()'),
                    new \yii\db\Expression('NOW()')
                ],
                [
                    $manajemenRolesId,
                    '4',
                    'Manajemen Roles',
                    'settings/role',
                    '', 
                    $settingAppId,
                    '1',
                    new \yii\db\Expression('NOW()'),
                    new \yii\db\Expression('NOW()')
                ],
                [
                    $manajemenUsersId,
                    '5',
                    'Manajemen Users',
                    'settings/user',
                    '', 
                    $settingAppId,
                    '1',
                    new \yii\db\Expression('NOW()'),
                    new \yii\db\Expression('NOW()')
                ],
                
            ]
        )->execute();

        /**
        * Roles
        */

        $rolesAdminId = UuidHelper::uuid();
        Yii::$app->db->createCommand()->batchInsert('roles',
            [ 
                'id',
                'name',
                'description',
                'created_at',
                'updated_at'
            ],
            [
                [$rolesAdminId,'Super Administrator','Role untuk admin tertinggi', new \yii\db\Expression('NOW()'),new \yii\db\Expression('NOW()')],
            ]
        )->execute();


        /**
        * User
        */
        $hash = Yii::$app->getSecurity();
        $password = $hash->generatePasswordHash('gacor123456');
        $adminId = UuidHelper::uuid();

        Yii::$app->db->createCommand()->batchInsert('admins',
            [ 
                'id',
                'name',
                'username',
                'email',
                'password',        
                'role_id',    
                'created_at' ,
                'updated_at'
            ],
            [
                [$adminId,'Admin Gacor','admin@gacorapp.id','admin@gacorapp.id', $password, $rolesAdminId, new \yii\db\Expression('NOW()'),new \yii\db\Expression('NOW()')],
            ]
        )->execute();


        $superAdminId = UuidHelper::uuid();
        Yii::$app->db->createCommand()->batchInsert('roles_access',
            [ 
                'id',
                'menu_id',
                'role_id',
            ],
            [
                [
                    UuidHelper::uuid(),
                    $dashboardId,
                    $rolesAdminId,
                ],
                [
                    UuidHelper::uuid(),
                    $settingAppId,
                    $rolesAdminId,
                ],
                [
                    UuidHelper::uuid(),
                    $manajemenMenuId,
                    $rolesAdminId,
                ],
                [
                    UuidHelper::uuid(),
                    $manajemenRolesId,
                    $rolesAdminId,
                ],
                [
                    UuidHelper::uuid(),
                    $manajemenUsersId,
                    $rolesAdminId,
                ],
            ]
        )->execute();

        return ExitCode::OK;
    }
}